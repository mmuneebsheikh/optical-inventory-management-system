﻿namespace VisualProjectOpInventory
{
    partial class Bill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bill));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btwBackBill = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBoxPrescription = new System.Windows.Forms.GroupBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btwAddPrescription = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.PrescriptionPanel = new System.Windows.Forms.Panel();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.btwDeletePresciption = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPhoneNomaskedBill = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDiscountBill = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBalanceBill = new System.Windows.Forms.TextBox();
            this.txtAdvanceBill = new System.Windows.Forms.TextBox();
            this.txtNetTotalBill = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeliveryDateBill = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSerialNoBill = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAddressBill = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNameAddNewCustomer = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel4.SuspendLayout();
            this.groupBoxPrescription.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.PrescriptionPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(11, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1348, 792);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.dataGridView1);
            this.panel5.Controls.Add(this.btwBackBill);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Location = new System.Drawing.Point(748, 10);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(598, 666);
            this.panel5.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label26.Location = new System.Drawing.Point(128, 101);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(381, 40);
            this.label26.TabIndex = 21;
            this.label26.Text = "yahan pe single Item k detail textBox se lenge \r\naur gridview me add krwaenge";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 253);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(592, 236);
            this.dataGridView1.TabIndex = 14;
            // 
            // btwBackBill
            // 
            this.btwBackBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBackBill.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btwBackBill.Location = new System.Drawing.Point(487, 7);
            this.btwBackBill.Margin = new System.Windows.Forms.Padding(2);
            this.btwBackBill.Name = "btwBackBill";
            this.btwBackBill.Size = new System.Drawing.Size(108, 33);
            this.btwBackBill.TabIndex = 1;
            this.btwBackBill.Text = "Back";
            this.btwBackBill.UseVisualStyleBackColor = true;
            this.btwBackBill.Click += new System.EventHandler(this.btwBackBill_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label13.Location = new System.Drawing.Point(98, 7);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(155, 31);
            this.label13.TabIndex = 13;
            this.label13.Text = "Item Detail";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBoxPrescription);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Location = new System.Drawing.Point(7, 312);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 364);
            this.panel4.TabIndex = 1;
            // 
            // groupBoxPrescription
            // 
            this.groupBoxPrescription.Controls.Add(this.btnSave);
            this.groupBoxPrescription.Controls.Add(this.btwAddPrescription);
            this.groupBoxPrescription.Controls.Add(this.flowLayoutPanel1);
            this.groupBoxPrescription.Location = new System.Drawing.Point(14, 63);
            this.groupBoxPrescription.Name = "groupBoxPrescription";
            this.groupBoxPrescription.Size = new System.Drawing.Size(705, 286);
            this.groupBoxPrescription.TabIndex = 1;
            this.groupBoxPrescription.TabStop = false;
            this.groupBoxPrescription.Text = "Prescription";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Location = new System.Drawing.Point(181, 257);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 32;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btwAddPrescription
            // 
            this.btwAddPrescription.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btwAddPrescription.Location = new System.Drawing.Point(12, 246);
            this.btwAddPrescription.Name = "btwAddPrescription";
            this.btwAddPrescription.Size = new System.Drawing.Size(112, 34);
            this.btwAddPrescription.TabIndex = 31;
            this.btwAddPrescription.Text = "Add Prescription";
            this.btwAddPrescription.UseVisualStyleBackColor = true;
            this.btwAddPrescription.Click += new System.EventHandler(this.btwAddPrescription_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flowLayoutPanel1.Controls.Add(this.PrescriptionPanel);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 19);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(648, 205);
            this.flowLayoutPanel1.TabIndex = 30;
            // 
            // PrescriptionPanel
            // 
            this.PrescriptionPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PrescriptionPanel.BackColor = System.Drawing.Color.Silver;
            this.PrescriptionPanel.Controls.Add(this.textBox13);
            this.PrescriptionPanel.Controls.Add(this.btwDeletePresciption);
            this.PrescriptionPanel.Controls.Add(this.label15);
            this.PrescriptionPanel.Controls.Add(this.textBox7);
            this.PrescriptionPanel.Controls.Add(this.textBox8);
            this.PrescriptionPanel.Controls.Add(this.textBox9);
            this.PrescriptionPanel.Controls.Add(this.textBox10);
            this.PrescriptionPanel.Controls.Add(this.textBox11);
            this.PrescriptionPanel.Controls.Add(this.textBox12);
            this.PrescriptionPanel.Controls.Add(this.label16);
            this.PrescriptionPanel.Controls.Add(this.label17);
            this.PrescriptionPanel.Controls.Add(this.label18);
            this.PrescriptionPanel.Controls.Add(this.textBox4);
            this.PrescriptionPanel.Controls.Add(this.textBox5);
            this.PrescriptionPanel.Controls.Add(this.textBox6);
            this.PrescriptionPanel.Controls.Add(this.textBox3);
            this.PrescriptionPanel.Controls.Add(this.textBox2);
            this.PrescriptionPanel.Controls.Add(this.textBox1);
            this.PrescriptionPanel.Controls.Add(this.label19);
            this.PrescriptionPanel.Controls.Add(this.label20);
            this.PrescriptionPanel.Controls.Add(this.label21);
            this.PrescriptionPanel.Controls.Add(this.label22);
            this.PrescriptionPanel.Controls.Add(this.label23);
            this.PrescriptionPanel.Controls.Add(this.label24);
            this.PrescriptionPanel.Controls.Add(this.label25);
            this.PrescriptionPanel.Location = new System.Drawing.Point(3, 3);
            this.PrescriptionPanel.Name = "PrescriptionPanel";
            this.PrescriptionPanel.Size = new System.Drawing.Size(636, 196);
            this.PrescriptionPanel.TabIndex = 0;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(82, 10);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 23;
            // 
            // btwDeletePresciption
            // 
            this.btwDeletePresciption.Location = new System.Drawing.Point(546, 7);
            this.btwDeletePresciption.Name = "btwDeletePresciption";
            this.btwDeletePresciption.Size = new System.Drawing.Size(75, 23);
            this.btwDeletePresciption.TabIndex = 1;
            this.btwDeletePresciption.Text = "X";
            this.btwDeletePresciption.UseVisualStyleBackColor = true;
            this.btwDeletePresciption.Click += new System.EventHandler(this.btwDeletePresciption_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(31, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Name";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(63, 121);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(78, 20);
            this.textBox7.TabIndex = 12;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(147, 121);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(78, 20);
            this.textBox8.TabIndex = 11;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(231, 121);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(78, 20);
            this.textBox9.TabIndex = 10;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(63, 157);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(78, 20);
            this.textBox10.TabIndex = 9;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(147, 157);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(78, 20);
            this.textBox11.TabIndex = 8;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(231, 157);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(78, 20);
            this.textBox12.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(525, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "AXIS";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(441, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "CYL";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(357, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "SPH";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(360, 121);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(78, 20);
            this.textBox4.TabIndex = 18;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(444, 121);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(78, 20);
            this.textBox5.TabIndex = 17;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(528, 121);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(78, 20);
            this.textBox6.TabIndex = 16;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(360, 157);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(78, 20);
            this.textBox3.TabIndex = 19;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(444, 157);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(78, 20);
            this.textBox2.TabIndex = 20;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(528, 157);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(78, 20);
            this.textBox1.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 160);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "N";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 124);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "D";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(228, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "AXIS";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(144, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "CYL";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(60, 89);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "SPH";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(435, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "LEFT";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(110, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "RIGHT";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label12.Location = new System.Drawing.Point(294, 22);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 37);
            this.label12.TabIndex = 0;
            this.label12.Text = "Order Details";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.txtPhoneNomaskedBill);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.txtDeliveryDateBill);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtSerialNoBill);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtAddressBill);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtNameAddNewCustomer);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(9, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(734, 297);
            this.panel2.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel6.Controls.Add(this.label14);
            this.panel6.Location = new System.Drawing.Point(320, 66);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(70, 210);
            this.panel6.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label14.Location = new System.Drawing.Point(26, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 182);
            this.label14.TabIndex = 23;
            this.label14.Text = "O\r\nP\r\nT\r\nI\r\nC\r\nA\r\nN\r\n";
            // 
            // txtPhoneNomaskedBill
            // 
            this.txtPhoneNomaskedBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNomaskedBill.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtPhoneNomaskedBill.Location = new System.Drawing.Point(95, 134);
            this.txtPhoneNomaskedBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNomaskedBill.Mask = "0000-0000000";
            this.txtPhoneNomaskedBill.Name = "txtPhoneNomaskedBill";
            this.txtPhoneNomaskedBill.Size = new System.Drawing.Size(210, 24);
            this.txtPhoneNomaskedBill.TabIndex = 21;
            this.txtPhoneNomaskedBill.Text = "0000000";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDiscountBill);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtBalanceBill);
            this.groupBox1.Controls.Add(this.txtAdvanceBill);
            this.groupBox1.Controls.Add(this.txtNetTotalBill);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(406, 76);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(314, 173);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Amount:";
            // 
            // txtDiscountBill
            // 
            this.txtDiscountBill.Location = new System.Drawing.Point(81, 68);
            this.txtDiscountBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiscountBill.Name = "txtDiscountBill";
            this.txtDiscountBill.Size = new System.Drawing.Size(230, 26);
            this.txtDiscountBill.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(4, 74);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 20);
            this.label11.TabIndex = 19;
            this.label11.Text = "Discount:";
            // 
            // txtBalanceBill
            // 
            this.txtBalanceBill.Location = new System.Drawing.Point(81, 133);
            this.txtBalanceBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtBalanceBill.Name = "txtBalanceBill";
            this.txtBalanceBill.Size = new System.Drawing.Size(230, 26);
            this.txtBalanceBill.TabIndex = 18;
            // 
            // txtAdvanceBill
            // 
            this.txtAdvanceBill.Location = new System.Drawing.Point(81, 102);
            this.txtAdvanceBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdvanceBill.Name = "txtAdvanceBill";
            this.txtAdvanceBill.Size = new System.Drawing.Size(230, 26);
            this.txtAdvanceBill.TabIndex = 17;
            // 
            // txtNetTotalBill
            // 
            this.txtNetTotalBill.Location = new System.Drawing.Point(81, 32);
            this.txtNetTotalBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtNetTotalBill.Name = "txtNetTotalBill";
            this.txtNetTotalBill.Size = new System.Drawing.Size(230, 26);
            this.txtNetTotalBill.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(4, 139);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Balance:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(4, 106);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Advance:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(8, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Net Total:";
            // 
            // txtDeliveryDateBill
            // 
            this.txtDeliveryDateBill.Location = new System.Drawing.Point(131, 253);
            this.txtDeliveryDateBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtDeliveryDateBill.Name = "txtDeliveryDateBill";
            this.txtDeliveryDateBill.Size = new System.Drawing.Size(174, 26);
            this.txtDeliveryDateBill.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(9, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Serial No:";
            // 
            // txtSerialNoBill
            // 
            this.txtSerialNoBill.Location = new System.Drawing.Point(95, 66);
            this.txtSerialNoBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtSerialNoBill.Name = "txtSerialNoBill";
            this.txtSerialNoBill.Size = new System.Drawing.Size(210, 26);
            this.txtSerialNoBill.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(21, 253);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Delivery Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(9, 168);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Address:";
            // 
            // txtAddressBill
            // 
            this.txtAddressBill.Location = new System.Drawing.Point(75, 166);
            this.txtAddressBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddressBill.Multiline = true;
            this.txtAddressBill.Name = "txtAddressBill";
            this.txtAddressBill.Size = new System.Drawing.Size(230, 74);
            this.txtAddressBill.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(734, 50);
            this.panel3.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(482, 21);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(334, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bill";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(9, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Contact No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(18, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name:";
            // 
            // txtNameAddNewCustomer
            // 
            this.txtNameAddNewCustomer.Location = new System.Drawing.Point(95, 101);
            this.txtNameAddNewCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.txtNameAddNewCustomer.Name = "txtNameAddNewCustomer";
            this.txtNameAddNewCustomer.Size = new System.Drawing.Size(210, 26);
            this.txtNameAddNewCustomer.TabIndex = 1;
            // 
            // Bill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 701);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Bill";
            this.Text = "                                                                                 " +
    "                                                                             Bil" +
    "l";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBoxPrescription.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.PrescriptionPanel.ResumeLayout(false);
            this.PrescriptionPanel.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNameAddNewCustomer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDiscountBill;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBalanceBill;
        private System.Windows.Forms.TextBox txtAdvanceBill;
        private System.Windows.Forms.TextBox txtNetTotalBill;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker txtDeliveryDateBill;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSerialNoBill;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAddressBill;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.MaskedTextBox txtPhoneNomaskedBill;
        private System.Windows.Forms.Button btwBackBill;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBoxPrescription;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel PrescriptionPanel;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Button btwDeletePresciption;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btwAddPrescription;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label26;
    }
}