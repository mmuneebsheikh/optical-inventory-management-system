﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Bill : Form
    {
        Panel[] p = new Panel[10];
//        List<Panel> panels = new List<Panel>();

        int pInd = 0;




        public Bill()
        {
            InitializeComponent();
        }


        void dynamicText() // // // retriving text from new textboxes to a label
        {
            for (int i = 0; i < pInd; i++)
            {
                foreach (Control c in p[i].Controls)
                {
                    if (c.GetType() == typeof(TextBox))
                    {
                        ///
                        // save the data anywhere from here
                        ///
                        //label12.Text += c.Name + ":" + c.Text + Environment.NewLine;
                    }
                }
            }
        }

        void DeletePrescription(object sender, EventArgs e)
        {
            //Panel p = ((Panel)sender);
            //p.Controls.Clear();
            
            string btnname = ((Button)sender).Name;
            string ind = btnname.Substring(btnname.Length - 1);
            int i = Convert.ToInt32(ind);
            flowLayoutPanel1.Container.Remove(p[i]);
            p[i].Dispose();
        }

        int pNo = 0;
        private void createPresciption()
        {
            if (pInd < 10)
            {
                p[pInd] = new Panel();
                p[pInd].Height = PrescriptionPanel.Height;
                p[pInd].Width = PrescriptionPanel.Width;
                p[pInd].BackColor = PrescriptionPanel.BackColor;
                p[pInd].Name = "newpanel" + pInd.ToString();
                flowLayoutPanel1.Controls.Add(p[pInd]);

                //Panel panel = new Panel();
                //panel.Height = PrescriptionPanel.Height;
                //panel.Width = PrescriptionPanel.Width;
                //panel.BackColor = PrescriptionPanel.BackColor;
                //panel.Name = "newpanel" + pNo.ToString();
                //panels.Add(panel);
                //pNo++;
                //flowLayoutPanel1.Controls.Add(panel);
                int txtInd = 0;
                foreach (Control c in PrescriptionPanel.Controls) // // // adding controls to new panel
                {
                    Control c2 = new Control();
                    if (c.GetType() == typeof(Label))
                    {
                        c2 = new Label();
                    }
                    if (c.GetType() == typeof(TextBox))
                    {
                        c2 = new TextBox();
                        c2.Name = "txt" + txtInd;
                        txtInd++;
                    }
                    if (c.GetType() == typeof(Button))
                    {
                        c2 = new Button();
                        if (c.Text == "X")
                        {
                            c2.Click += new EventHandler(DeletePrescription); // // // binding delete prescription event
                        }
                        c2.Name = "btwX" + pInd.ToString();
                    }
                    c2.Location = c.Location;
                    c2.Size = c.Size;
                    if (c.GetType() != typeof(TextBox)) // // // to empty dynamic textboxes
                        c2.Text = c.Text;
                    p[pInd].Controls.Add(c2);
                    //panel.Controls.Add(c2);
                }
                pInd++;

            }
            else
            {
                MessageBox.Show("Cannot Add Further Prescription");
            }
        }



        private void btwBackBill_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerList CL = new CustomerList();
            this.Dispose();
            CL.Show();

        }

        private void btwAddPrescription_Click(object sender, EventArgs e)
        {
                createPresciption();
        }

        private void btwDeletePresciption_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Prescription cannot be deleted!");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }
    }
}
