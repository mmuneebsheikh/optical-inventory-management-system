﻿namespace VisualProjectOpInventory
{
    partial class Inventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Inventory));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btweditprod = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btwGoInventory = new System.Windows.Forms.Button();
            this.txtSearchInventory = new System.Windows.Forms.TextBox();
            this.DGInventory = new System.Windows.Forms.DataGridView();
            this.btwBackInventory = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGInventory)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, -31);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(898, 568);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btweditprod);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.btwBackInventory);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Location = new System.Drawing.Point(9, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(879, 547);
            this.panel2.TabIndex = 0;
            // 
            // btweditprod
            // 
            this.btweditprod.Font = new System.Drawing.Font("Mistral", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btweditprod.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btweditprod.Location = new System.Drawing.Point(670, 139);
            this.btweditprod.Margin = new System.Windows.Forms.Padding(2);
            this.btweditprod.Name = "btweditprod";
            this.btweditprod.Size = new System.Drawing.Size(161, 31);
            this.btweditprod.TabIndex = 34;
            this.btweditprod.Text = "Update/Delete Products";
            this.btweditprod.UseVisualStyleBackColor = true;
            this.btweditprod.Click += new System.EventHandler(this.btweditprod_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btwGoInventory);
            this.panel6.Controls.Add(this.txtSearchInventory);
            this.panel6.Controls.Add(this.DGInventory);
            this.panel6.Location = new System.Drawing.Point(47, 174);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(784, 357);
            this.panel6.TabIndex = 33;
            // 
            // btwGoInventory
            // 
            this.btwGoInventory.Font = new System.Drawing.Font("Lucida Sans Unicode", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwGoInventory.Location = new System.Drawing.Point(713, 2);
            this.btwGoInventory.Margin = new System.Windows.Forms.Padding(2);
            this.btwGoInventory.Name = "btwGoInventory";
            this.btwGoInventory.Size = new System.Drawing.Size(67, 20);
            this.btwGoInventory.TabIndex = 7;
            this.btwGoInventory.Text = "Search";
            this.btwGoInventory.UseVisualStyleBackColor = true;
            this.btwGoInventory.Click += new System.EventHandler(this.btwGoInventory_Click);
            // 
            // txtSearchInventory
            // 
            this.txtSearchInventory.Location = new System.Drawing.Point(0, 2);
            this.txtSearchInventory.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchInventory.Name = "txtSearchInventory";
            this.txtSearchInventory.Size = new System.Drawing.Size(709, 20);
            this.txtSearchInventory.TabIndex = 6;
            // 
            // DGInventory
            // 
            this.DGInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGInventory.Location = new System.Drawing.Point(0, 26);
            this.DGInventory.Margin = new System.Windows.Forms.Padding(2);
            this.DGInventory.Name = "DGInventory";
            this.DGInventory.RowTemplate.Height = 24;
            this.DGInventory.Size = new System.Drawing.Size(780, 318);
            this.DGInventory.TabIndex = 0;
            // 
            // btwBackInventory
            // 
            this.btwBackInventory.Font = new System.Drawing.Font("Mistral", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBackInventory.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btwBackInventory.Location = new System.Drawing.Point(769, 12);
            this.btwBackInventory.Margin = new System.Windows.Forms.Padding(2);
            this.btwBackInventory.Name = "btwBackInventory";
            this.btwBackInventory.Size = new System.Drawing.Size(102, 31);
            this.btwBackInventory.TabIndex = 32;
            this.btwBackInventory.Text = "Back";
            this.btwBackInventory.UseVisualStyleBackColor = true;
            this.btwBackInventory.Click += new System.EventHandler(this.btwBackInventory_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(307, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(268, 49);
            this.panel3.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Mistral", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(89, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 33);
            this.label1.TabIndex = 1;
            this.label1.Text = "Inventory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Enter Model No";
            // 
            // Inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 546);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Inventory";
            this.Text = "Inventory";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Inventory_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGInventory)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btwBackInventory;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btwGoInventory;
        private System.Windows.Forms.TextBox txtSearchInventory;
        private System.Windows.Forms.DataGridView DGInventory;
        private System.Windows.Forms.Button btweditprod;
        private System.Windows.Forms.Label label2;
    }
}