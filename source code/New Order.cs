﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class New_Order : Form
    {
        public New_Order()
        {
            InitializeComponent();
        }

        private void New_Order_Load(object sender, EventArgs e)
        {
            txtAddressNewOrder.Enabled = false;
            txtAddressNewOrder.Visible = false;
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            txtAddressNewOrder.Enabled = true;
            txtAddressNewOrder.Visible = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainmenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DGNewOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btwBackNewOrder_Click(object sender, EventArgs e)
        {
            MainPage mainpageobj = new MainPage();
            //this.Close();
            this.Hide();
            mainpageobj.Show();
        }
    }
}
