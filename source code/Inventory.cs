﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Inventory : Form
    {
        DataAccessClass DA = new DataAccessClass();
        public Inventory()
        {
            InitializeComponent();
        }

        private void btwBackInventory_Click(object sender, EventArgs e)
        {
            OpeningTheme obj = new OpeningTheme();
            this.Close();
            this.Hide();
            obj.Show();
        }

        private void btwGoInventory_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearchInventory.Text))
            {
                string prd = txtSearchInventory.Text;
                DGInventory.DataSource = DA.GetProductStockByModelno(prd).Tables["product_stock"];

            }
            else
            {
                DGInventory.DataSource = DA.AllProduct_display().Tables["product_stock"];
            }
        }

        private void Inventory_Load(object sender, EventArgs e)
        {
            DGInventory.DataSource = DA.AllProduct_display().Tables["product_stock"];
        }

        private void btweditprod_Click(object sender, EventArgs e)
        {
            
            if (txtSearchInventory.Text != "")
            {
                string modelNo = txtSearchInventory.Text;
                    
            }

            this.Hide();
            UpdateDelete_inventory udi = new UpdateDelete_inventory();
            udi.Show();
        }
    }
}
