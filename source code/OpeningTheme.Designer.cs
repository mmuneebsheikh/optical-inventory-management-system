﻿namespace VisualProjectOpInventory
{
    partial class OpeningTheme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btwExitOpeningTheme = new System.Windows.Forms.Button();
            this.btwViewOpeningTheme = new System.Windows.Forms.Button();
            this.btwBillOpeningTheme = new System.Windows.Forms.Button();
            this.btwSalesOpeningTheme = new System.Windows.Forms.Button();
            this.btwVendorsOpeningTheme = new System.Windows.Forms.Button();
            this.btwCustomerOpeningTheme = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btwExitOpeningTheme);
            this.panel1.Controls.Add(this.btwViewOpeningTheme);
            this.panel1.Controls.Add(this.btwBillOpeningTheme);
            this.panel1.Controls.Add(this.btwSalesOpeningTheme);
            this.panel1.Controls.Add(this.btwVendorsOpeningTheme);
            this.panel1.Controls.Add(this.btwCustomerOpeningTheme);
            this.panel1.Location = new System.Drawing.Point(111, 26);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(832, 364);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::VisualProjectOpInventory.Properties.Resources.kghostview;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(322, 17);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(188, 139);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btwExitOpeningTheme
            // 
            this.btwExitOpeningTheme.AutoSize = true;
            this.btwExitOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwExitOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwExitOpeningTheme.Location = new System.Drawing.Point(722, 168);
            this.btwExitOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwExitOpeningTheme.Name = "btwExitOpeningTheme";
            this.btwExitOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwExitOpeningTheme.TabIndex = 5;
            this.btwExitOpeningTheme.Text = "Exit";
            this.btwExitOpeningTheme.UseVisualStyleBackColor = true;
            this.btwExitOpeningTheme.Click += new System.EventHandler(this.btwExitOpeningTheme_Click);
            // 
            // btwViewOpeningTheme
            // 
            this.btwViewOpeningTheme.AutoSize = true;
            this.btwViewOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwViewOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwViewOpeningTheme.Location = new System.Drawing.Point(618, 140);
            this.btwViewOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwViewOpeningTheme.Name = "btwViewOpeningTheme";
            this.btwViewOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwViewOpeningTheme.TabIndex = 4;
            this.btwViewOpeningTheme.Text = "Inventory";
            this.btwViewOpeningTheme.UseVisualStyleBackColor = true;
            this.btwViewOpeningTheme.Click += new System.EventHandler(this.btwViewOpeningTheme_Click);
            // 
            // btwBillOpeningTheme
            // 
            this.btwBillOpeningTheme.AutoSize = true;
            this.btwBillOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBillOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBillOpeningTheme.Location = new System.Drawing.Point(514, 101);
            this.btwBillOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwBillOpeningTheme.Name = "btwBillOpeningTheme";
            this.btwBillOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwBillOpeningTheme.TabIndex = 3;
            this.btwBillOpeningTheme.Text = "Admin";
            this.btwBillOpeningTheme.UseVisualStyleBackColor = true;
            this.btwBillOpeningTheme.Click += new System.EventHandler(this.btwBillOpeningTheme_Click);
            // 
            // btwSalesOpeningTheme
            // 
            this.btwSalesOpeningTheme.AutoSize = true;
            this.btwSalesOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSalesOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwSalesOpeningTheme.Location = new System.Drawing.Point(217, 101);
            this.btwSalesOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwSalesOpeningTheme.Name = "btwSalesOpeningTheme";
            this.btwSalesOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwSalesOpeningTheme.TabIndex = 2;
            this.btwSalesOpeningTheme.Text = "Sales";
            this.btwSalesOpeningTheme.UseVisualStyleBackColor = true;
            this.btwSalesOpeningTheme.Click += new System.EventHandler(this.btwSalesOpeningTheme_Click);
            // 
            // btwVendorsOpeningTheme
            // 
            this.btwVendorsOpeningTheme.AutoSize = true;
            this.btwVendorsOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwVendorsOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwVendorsOpeningTheme.Location = new System.Drawing.Point(113, 140);
            this.btwVendorsOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwVendorsOpeningTheme.Name = "btwVendorsOpeningTheme";
            this.btwVendorsOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwVendorsOpeningTheme.TabIndex = 1;
            this.btwVendorsOpeningTheme.Text = " Vendors";
            this.btwVendorsOpeningTheme.UseVisualStyleBackColor = true;
            this.btwVendorsOpeningTheme.Click += new System.EventHandler(this.btwVendorsOpeningTheme_Click);
            // 
            // btwCustomerOpeningTheme
            // 
            this.btwCustomerOpeningTheme.AutoSize = true;
            this.btwCustomerOpeningTheme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwCustomerOpeningTheme.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwCustomerOpeningTheme.Location = new System.Drawing.Point(9, 168);
            this.btwCustomerOpeningTheme.Margin = new System.Windows.Forms.Padding(2);
            this.btwCustomerOpeningTheme.Name = "btwCustomerOpeningTheme";
            this.btwCustomerOpeningTheme.Size = new System.Drawing.Size(100, 82);
            this.btwCustomerOpeningTheme.TabIndex = 0;
            this.btwCustomerOpeningTheme.Text = "Customer";
            this.btwCustomerOpeningTheme.UseVisualStyleBackColor = true;
            this.btwCustomerOpeningTheme.Click += new System.EventHandler(this.btwCustomerOpeningTheme_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // OpeningTheme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1054, 416);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OpeningTheme";
            this.Text = "OpeningTheme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OpeningTheme_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btwExitOpeningTheme;
        private System.Windows.Forms.Button btwViewOpeningTheme;
        private System.Windows.Forms.Button btwBillOpeningTheme;
        private System.Windows.Forms.Button btwSalesOpeningTheme;
        private System.Windows.Forms.Button btwVendorsOpeningTheme;
        private System.Windows.Forms.Button btwCustomerOpeningTheme;
        private System.Windows.Forms.Timer timer1;
    }
}