﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{

    
    public partial class VendorsOrder : Form
    {
        DataAccessClass DA = new DataAccessClass();
        public VendorsOrder()
        {
            InitializeComponent();
        }

        private void btwAddNewOrders_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vendor = new Vendor();
            vendor.Show();
        }

        private void VendorsOrder_Load(object sender, EventArgs e)
        {
            ////DataSet dispVendors = 
            DGVendorsOrders.Rows.Clear();
            DGVendorsOrders.DataSource = null;
            DGVendorsOrders.Refresh();
            DGVendorsOrders.DataSource = DA.vendor_detail_display().Tables["vendor_detail"];
            DGVendorsOrders.Refresh();        
        }

        private void btwSearchOrders_Click(object sender, EventArgs e)
        {
            string VdName = txtVendorSearch.Text;
            if (!string.IsNullOrWhiteSpace(VdName))
            {
                DGVendorsOrders.DataSource = DA.vendor_detail_displayDS(VdName).Tables["vendor_detail"];
                List<int> VendBills = DA.vendorNameToget_vbillno(VdName);
                cmbBills.Items.Clear();
                cmbBills.SelectedIndex = -1;
                cmbBills.Text = "";
                foreach (var item in VendBills)
                {
                    cmbBills.Items.Add(item);
                }
            }

        }

        private void btwShowVendor_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtVendorSearch.Text))
            {
                string name = txtVendorSearch.Text;
                this.Hide();
               
                Vendor v = new Vendor(name);
                v.Show();
            }
            else
            {
                MessageBox.Show("Please Enter Vendor Name");
            }
        }

        private void btwShowBill_Click(object sender, EventArgs e)
        {
            if (cmbBills.SelectedIndex != -1)
            {
                int billNo = Convert.ToInt32(cmbBills.SelectedItem);
                VendorPrintBill PrintBill = new VendorPrintBill(billNo);
                // this.Hide();
                PrintBill.Show();
            }
        }

        private void MenuStripVendorOrders_clicked(object sender, EventArgs e)
        {
            this.Hide();
            VendorsOrder VO = new VendorsOrder();
            VO.Show();

        }

        private void MenuStripNewVendor_clicked(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vendor = new Vendor();
            vendor.Show();
        }

        private void MenuStripVendorList(object sender, EventArgs e)
        {
            this.Hide();
            VendorList VL = new VendorList();
            VL.Show();

        }
    }
}
