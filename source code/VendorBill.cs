﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class VendorBill : Form
    {

        int total;

        double totalProdPrice = 0;
        string UserName;
        VendorClass vd = new VendorClass();
        DataAccessClass DA = new DataAccessClass();
        public VendorBill()
        {
            InitializeComponent();
        }
        public VendorBill(string vendorName)
        {
            InitializeComponent();
            this.vd.Name = vendorName;
        }
        public VendorBill(VendorClass vendor,string UserName)
        {
            InitializeComponent();
            this.vd = vendor;
            this.UserName = UserName;
        }


        private void btwAddVendor_Click(object sender, EventArgs e)
        {
            try
            {
                string prodName = txtIProductNameVendor.Text;
                string modelNo = txtModelNoVendor.Text;
                string color = txtColorVendor.Text;
                int  qty = Convert.ToInt32(txtQuantityVendor.Text);
                //string size = cboxSizeVendor.GetItemText(this.cboxSizeVendor.SelectedItem);
                string size = cboxSizeVendor.SelectedItem.ToString(); 
                //cate = cboxCategoryVendor.GetItemText(this.cboxCategoryVendor.SelectedItem);
                 string cate = cboxCategoryVendor.SelectedItem.ToString();
                string gender = "";
                if (radioButtonMaleVendor.Checked)
	            {
		            gender = "Male";
	            }
                else {gender = "Female";}
                double retPrice = Convert.ToDouble(txtRetailPriceVendor.Text);
                double wholePrice = Convert.ToDouble(txtWholeSalePriceVendor.Text);
                totalProdPrice = wholePrice * qty;
                DGProductsVendor.Rows.Add(prodName,modelNo,color,qty,size,cate,gender,retPrice,wholePrice,totalProdPrice);
                
       


                ClearTextBoxes();
                cboxCategoryVendor.SelectedIndex = -1;
                cboxSizeVendor.SelectedIndex = -1;
                btwSaveVendor.Enabled = true;
                vd.bill.amount.totalBill += totalProdPrice;
                txtTotalBill.Text = vd.bill.amount.totalBill.ToString();
                txtCredit.Text = "";
                txtCredit.Text = "0";
            }
            catch (Exception ex)
            {
                
                MessageBox.Show(ex.Message);
            }
        
        }
        private void ClearTextBoxes()
        {
            Action<Control.ControlCollection> func = null;

            func = (controls) =>
            {
                foreach (Control control in controls)
                    if (control is TextBox){
                        if (control.Name != "txtTotalBill" && control.Name != "txtCredit" && control.Name != "txtBalance" && control.Name != "txtVendorNameVendorBill")
                        {
                            (control as TextBox).Clear();   
                        }
                    }
                    else
                    {
                        func(control.Controls);
                    }
                        
            };

            func(Controls);
        }

        private void VendorBill_Load(object sender, EventArgs e)
        {
            txtVendorNameVendorBill.Text = this.vd.Name;
        }

        private void btwSaveVendor_Click(object sender, EventArgs e)
        {
            try
            {
                //Set Status
                if (txtBalance.Text == "0")
                {
                    lblStatusVendorBill.Text = "Paid";
                }
                else if (txtBalance.Text == txtTotalBill.Text)
                {
                    lblStatusVendorBill.Text = "UnPaid";
                }
                else lblStatusVendorBill.Text = "Partially Paid";


                vd.bill = new VendorClass.Bill
                {
                    OrderDate = Convert.ToDateTime(txtOrderDate.Text),
                    DueDate = Convert.ToDateTime(txtDueDate.Text),
                    status = lblStatusVendorBill.Text
                };

                for (int i = 0; i < DGProductsVendor.Rows.Count - 1; i++)
                {

                    vd.prod = new VendorClass.Product();
                    vd.prod.prodName = DGProductsVendor.Rows[i].Cells[0].Value.ToString();
                    vd.prod.modelNo = DGProductsVendor.Rows[i].Cells[1].Value.ToString();
                    vd.prod.color = DGProductsVendor.Rows[i].Cells[2].Value.ToString();
                    vd.prod.qty = Convert.ToInt32(DGProductsVendor.Rows[i].Cells[3].Value);
                    vd.prod.size = Convert.ToInt32(DGProductsVendor.Rows[i].Cells[4].Value);
                    vd.prod.category = DGProductsVendor.Rows[i].Cells[5].Value.ToString();
                    vd.prod.gender = DGProductsVendor.Rows[i].Cells[6].Value.ToString();
                    vd.prod.retailPrice = Convert.ToDouble(DGProductsVendor.Rows[i].Cells[7].Value);
                    vd.prod.wholesalePrice = Convert.ToDouble(DGProductsVendor.Rows[i].Cells[8].Value);
                    vd.bill.products.Add(vd.prod);

                    //MessageBox.Show(vd.bill.products[i].prodName);
                }
          
                vd.bill.amount.paid = Convert.ToDouble(txtCredit.Text);
                vd.bill.amount.balance = Convert.ToDouble(txtBalance.Text);//txtBalance.Text
                vd.bill.amount.totalBill = Convert.ToDouble(txtTotalBill.Text);//txtTotalBill.Text

                

                //Insert in vendor Bill
                DA.vendor_bill_insert(UserName, vd.Name, vd.bill.OrderDate, vd.bill.DueDate, vd.bill.status, vd.bill.amount.totalBill, vd.bill.amount.paid, vd.bill.amount.balance);
                
                
                //Insert in Shipment // Insert in Products
                for (int i = 0; i < vd.bill.products.Count; i++)
                {
                    DA.product_stock_insert(vd.bill.products[i].prodName, vd.bill.products[i].modelNo, vd.bill.products[i].color, vd.bill.products[i].qty, vd.bill.products[i].size, vd.bill.products[i].retailPrice, vd.bill.products[i].category, vd.bill.products[i].gender, vd.bill.products[i].wholesalePrice);
                    //DA.shipment_insert(vd.bill.products[i].prodName);    
                }
                FormReset();
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            
        }

        void FormReset()
        {
            ClearTextBoxes();
            DGProductsVendor.Rows.Clear();
            txtBalance.Text = "";
            txtTotalBill.Text = "";
            txtCredit.Text = "";
            btwSaveVendor.Enabled = false;
        }


        private void Qty_WholeSale_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtQuantityVendor.Text != "" && txtWholeSalePriceVendor.Text != "")
                {
                    int qty = Convert.ToInt32(txtQuantityVendor.Text);
                    double wholePrice = Convert.ToDouble(txtWholeSalePriceVendor.Text);
                    totalProdPrice = wholePrice * qty;
                    txtTotProdPrice.Text = totalProdPrice.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txtWholeSalePriceVendor.Text = "";
                txtQuantityVendor.Text = "";
            }

        }

        private void txtCancel_Click(object sender, EventArgs e)
        {
            Vendor V = new Vendor(vd.Name);
            this.Hide();
            V.Show();
        }

        private void txtTotProdPrice_TextChanged(object sender, EventArgs e)
        {
           // total = Convert.ToInt32(txtTotProdPrice.Text);
        }

        private void txtCredit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double cred = 0;
                if (txtCredit.Text != "" )
                {
                   cred = Convert.ToDouble(txtCredit.Text);
                }
                
                double bal = vd.bill.amount.totalBill - cred;
                if (bal < 0)
                {
                    MessageBox.Show("Advance Payment is full");
                    txtCredit.Text = txtTotalBill.Text;
                    txtBalance.Text = "0";
                }
                else
                {
                    txtBalance.Text = bal.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btwEdit_Click(object sender, EventArgs e)
        {
            if (!txtTotalBill.Enabled)
            {
                txtTotalBill.ReadOnly = true;
                txtBalance.ReadOnly = true;
            }
            else
            {
                txtTotalBill.ReadOnly = false;
                txtBalance.ReadOnly = false;
            }
        }
    }
}
