﻿namespace VisualProjectOpInventory
{
    partial class SalesReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btwBack = new System.Windows.Forms.Button();
            this.btwShowReports = new System.Windows.Forms.Button();
            this.btwSearch = new System.Windows.Forms.Button();
            this.btwGenerateReport = new System.Windows.Forms.Button();
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DGMonthlyBills = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGMonthlyBills)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btwBack);
            this.panel1.Controls.Add(this.btwShowReports);
            this.panel1.Controls.Add(this.btwSearch);
            this.panel1.Controls.Add(this.btwGenerateReport);
            this.panel1.Controls.Add(this.cmbMonth);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.DGMonthlyBills);
            this.panel1.Location = new System.Drawing.Point(9, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(682, 397);
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(223, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(272, 37);
            this.label2.TabIndex = 1;
            this.label2.Text = "SALES REPORT";
            // 
            // btwBack
            // 
            this.btwBack.Location = new System.Drawing.Point(558, 10);
            this.btwBack.Name = "btwBack";
            this.btwBack.Size = new System.Drawing.Size(121, 23);
            this.btwBack.TabIndex = 6;
            this.btwBack.Text = "Back";
            this.btwBack.UseVisualStyleBackColor = true;
            this.btwBack.Click += new System.EventHandler(this.btwBack_Click);
            // 
            // btwShowReports
            // 
            this.btwShowReports.Location = new System.Drawing.Point(530, 155);
            this.btwShowReports.Name = "btwShowReports";
            this.btwShowReports.Size = new System.Drawing.Size(108, 43);
            this.btwShowReports.TabIndex = 5;
            this.btwShowReports.Text = "Show Reports";
            this.btwShowReports.UseVisualStyleBackColor = true;
            this.btwShowReports.Click += new System.EventHandler(this.btwShowReports_Click);
            // 
            // btwSearch
            // 
            this.btwSearch.Location = new System.Drawing.Point(243, 116);
            this.btwSearch.Name = "btwSearch";
            this.btwSearch.Size = new System.Drawing.Size(121, 23);
            this.btwSearch.TabIndex = 4;
            this.btwSearch.Text = "Search";
            this.btwSearch.UseVisualStyleBackColor = true;
            this.btwSearch.Click += new System.EventHandler(this.btwSearch_Click);
            // 
            // btwGenerateReport
            // 
            this.btwGenerateReport.Location = new System.Drawing.Point(530, 106);
            this.btwGenerateReport.Name = "btwGenerateReport";
            this.btwGenerateReport.Size = new System.Drawing.Size(108, 43);
            this.btwGenerateReport.TabIndex = 3;
            this.btwGenerateReport.Text = "Generate Report";
            this.btwGenerateReport.UseVisualStyleBackColor = true;
            this.btwGenerateReport.Click += new System.EventHandler(this.btwGenerateReport_Click);
            // 
            // cmbMonth
            // 
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Items.AddRange(new object[] {
            "January",
            "Feburary",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "Octuber",
            "November",
            "December"});
            this.cmbMonth.Location = new System.Drawing.Point(105, 118);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(121, 21);
            this.cmbMonth.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Month";
            // 
            // DGMonthlyBills
            // 
            this.DGMonthlyBills.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGMonthlyBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGMonthlyBills.Location = new System.Drawing.Point(32, 199);
            this.DGMonthlyBills.Name = "DGMonthlyBills";
            this.DGMonthlyBills.Size = new System.Drawing.Size(619, 177);
            this.DGMonthlyBills.TabIndex = 0;
            // 
            // SalesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 518);
            this.Controls.Add(this.panel1);
            this.Name = "SalesReport";
            this.Text = "SalesReport";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGMonthlyBills)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btwBack;
        private System.Windows.Forms.Button btwShowReports;
        private System.Windows.Forms.Button btwSearch;
        private System.Windows.Forms.Button btwGenerateReport;
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DGMonthlyBills;
    }
}