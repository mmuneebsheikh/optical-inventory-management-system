﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class VendorPrintBill : Form
    {
        DataAccessClass DA = new DataAccessClass();
        //VendorClass vend = new VendorClass();
        int billNo;
        public VendorPrintBill()
        {
            InitializeComponent();
        }
        public VendorPrintBill(int billNo)
        {
            InitializeComponent();
            ShowBill(billNo);
            this.billNo = billNo;

        }

        void ShowBill(int billNo)
        {
            lblbillno.Text = billNo.ToString();
            lbltotal.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[0].ToString();
            //lblpaid.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[1].ToString();
            txtPaid.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[1].ToString();
            lblblnc.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[2].ToString();
            lblDueDate.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[3].ToString();
            lblOrdDate.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[4].ToString();
            lblStatus.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[5].ToString();
            lblvendorName.Text = DA.vendor_billPrint(billNo).Tables["vendor_billPrint"].Rows[0].ItemArray[6].ToString();

            int rows = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows.Count;
            //lblProducts.Text = "" + rows.ToString();
            //lblProducts.Text = "BrandName\tModelNo\tColor\tQuantity\tSize\tCategory\tGender\tRetail\tWholePrice" + Environment.NewLine;
            for (int i = 0; i < rows; i++)
            {
                string brnd = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[0].ToString();
                string model = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[1].ToString();
                string color = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[2].ToString();
                string qty = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[3].ToString();
                string size = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[4].ToString();
                string cate = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[5].ToString();
                string gender = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[6].ToString();
                string retail = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[7].ToString();
                string whole = DA.vendor_billPrint(billNo).Tables["vendor_billProdsPrint"].Rows[i].ItemArray[8].ToString();



                //lblProducts.Text += brnd + "\t" + model;
                lblBrnd.Text += brnd + Environment.NewLine;
                lblModel.Text += model + Environment.NewLine;
                lblColor.Text += color + Environment.NewLine;
                lblQty.Text += qty + Environment.NewLine;
                lblSize.Text += size + Environment.NewLine;
                lblCate.Text += cate + Environment.NewLine;
                lblGender.Text += gender + Environment.NewLine;
                lblRetail.Text += retail + Environment.NewLine;
                lblWholeSale.Text += whole + Environment.NewLine;

            }
        }

        private void btwUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                double paid = Convert.ToDouble(txtPaid.Text);
                double total = Convert.ToDouble(lbltotal.Text);
                double balance = total - paid;
                string status = lblStatus.Text;
                //Set Status
                if (balance == 0)
                {
                    status = "Paid";
                }
                else if (balance == total)
                {
                    status = "UnPaid";
                }
                else status = "Partially Paid";

                MessageBox.Show(billNo.ToString());
                DA.UpdateBill(paid, status, balance, billNo);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
    
        
            
    
        }
    }
}
