﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualProjectOpInventory
{
    class StockClass
    {
        public string Name { get; set; }
        public string modelNo { get; set; }
        public string color { get; set; }
        public string category { get; set; }
        public string gender { get; set; }
        public int size { get; set; }
        public int qty { get; set; }
        public double retailPrice { get; set; }
        public double wholesalePrice { get; set; }
        //public Image disp { get; set; }
    }
}
