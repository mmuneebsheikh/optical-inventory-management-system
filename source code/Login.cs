﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Login : Form
    {
        DataAccessClass DA = new DataAccessClass();
        object obj;
        string toPage;
        
        public Login()
        {
            InitializeComponent();
            
        }
        public Login(string toPage,object vd)
        {
            InitializeComponent();
            this.toPage = toPage;
            obj = vd;
        }
        private void btwBackLogin_Click(object sender, EventArgs e)
        {
            //MainPage obj = new MainPage();
            //this.Close();
            this.Hide();
            //obj.Show();
            if (toPage == "vendorBill")
            {
                Vendor V = new Vendor(((VendorClass)obj).Name);
                V.Show();
            }
        }

        private void btwEnterLogin_Click(object sender, EventArgs e)
        {
            string userName = txtUsernameLogin.Text;
            string pass = txtPasswordLogin.Text;
         
            bool IsUser = DA.VarifyUser(userName , pass);
            if (IsUser)
            {
                //chk toPage for different form load
                string un = DA.GetUserName(userName, pass);
                if (toPage == "vendorBill")
                {
                    this.Close();
                    VendorBill VB = new VendorBill((VendorClass)obj,un);
                    VB.Show();
                }
                else if (toPage == "CustomerBill")
                {
                    this.Close();
                    CustomerNewBill CB = new CustomerNewBill((CustomerClass)obj,un);
                    CB.Show();
                }
                else if (toPage == "Admin")
                {
                    this.Close();
                    AdminPanel AP = new AdminPanel();
                    AP.Show();
                }
                else if (toPage == "Sales")
                {
                    this.Close();
                    SalesReport SR = new SalesReport();
                    SR.Show();
                }
                else
                {
                    this.Close();
                    OpeningTheme OT = new OpeningTheme();
                    OT.Show();
                }
                
            }
            else
            {
                MessageBox.Show("Invalid userName or Password!");
                txtPasswordLogin.Text = "";
                txtUsernameLogin.Text = "";
                txtUsernameLogin.Focus();
            }
        }

       

    }
}
