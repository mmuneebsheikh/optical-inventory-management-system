﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class VendorList : Form
    {
        DataAccessClass DA = new DataAccessClass();

        public VendorList()
        {
            InitializeComponent();
        }

        private void btwBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vend = new Vendor();
            vend.Show();
                
        }

        private void VendorList_Load(object sender, EventArgs e)
        {
            var source = new AutoCompleteStringCollection();
            DGVendorList.DataSource = DA.AllVendors().Tables["vendor_detail"];
            List<string> src = new List<string>();
            int i = 0;
            foreach (DataGridViewRow row in DGVendorList.Rows)
            {
                //src.Add(row.Cells[0].ToString());
                source.Add(row.Cells[0].ToString());
                //i++;
            }
            txtVendorSearch.AutoCompleteCustomSource =  source;
        }

        private void btwSearchVendorList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtVendorSearch.Text))
            {
                string vdName = txtVendorSearch.Text;
                DGVendorList.DataSource = DA.GetVendorDetailsByName(vdName).Tables["vendor_detail"];
               
            }
            else
            {
                DGVendorList.DataSource = DA.AllVendors().Tables["vendor_detail"];
            }
        }

        private void MenuStripVendorOrders_clicked(object sender, EventArgs e)
        {
            this.Hide();
            VendorsOrder VO = new VendorsOrder();
            VO.Show();
        }

        private void MenuStripNewVendor_clicked(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vendor = new Vendor();
            vendor.Show();
        }

        private void MenuStripVendorList(object sender, EventArgs e)
        {
            this.Hide();
            VendorList VL = new VendorList();
            VL.Show();
        }

        private void btwShowVendor_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtVendorSearch.Text))
            {
                string name = txtVendorSearch.Text;
                this.Hide();

                Vendor v = new Vendor(name);
                v.Show();
            }
            else
            {
                MessageBox.Show("Please Enter Vendor Name");
            }
        }
    }
}
