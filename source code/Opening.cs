﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Opening : Form
    {
        public Opening()
        {
            InitializeComponent();
        }

        private void openprojectbtw_Click(object sender, EventArgs e)
        {
            OpeningTheme obj = new OpeningTheme();
            //this.Close();
            this.Hide();
            obj.Show();
        }

        private void Opening_Load(object sender, EventArgs e)
        {

        }
    }
}
