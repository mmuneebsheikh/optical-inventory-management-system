﻿namespace VisualProjectOpInventory
{
    partial class CustomerMainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicalDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mAINMENUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btwBack = new System.Windows.Forms.Button();
            this.btwCustomerList = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btwDeleteCustomer = new System.Windows.Forms.Button();
            this.btwUpdateCustomer = new System.Windows.Forms.Button();
            this.btwAddCustomer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DGBills = new System.Windows.Forms.DataGridView();
            this.btwCustomerBillCustomer = new System.Windows.Forms.Button();
            this.txtPhoneNomaskedVendor = new System.Windows.Forms.MaskedTextBox();
            this.txtNameVendor = new System.Windows.Forms.TextBox();
            this.txtAddressVendor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.MainmenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGBills)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.mAINMENUToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.MainmenuStrip1.Size = new System.Drawing.Size(1311, 25);
            this.MainmenuStrip1.TabIndex = 5;
            this.MainmenuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            this.vendorsNameToolStripMenuItem.Click += new System.EventHandler(this.vendorsNameToolStripMenuItem_Click);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ordersToolStripMenuItem.Text = "New Vendor";
            this.ordersToolStripMenuItem.Click += new System.EventHandler(this.ordersToolStripMenuItem_Click);
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem1.Text = "Vendor List";
            this.vendorsNameToolStripMenuItem1.Click += new System.EventHandler(this.vendorsNameToolStripMenuItem1_Click);
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordersToolStripMenuItem1,
            this.customerAddToolStripMenuItem,
            this.customerListToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(88, 21);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(175, 22);
            this.ordersToolStripMenuItem1.Text = "Orders";
            this.ordersToolStripMenuItem1.Click += new System.EventHandler(this.ordersToolStripMenuItem1_Click);
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.customerAddToolStripMenuItem.Text = "CustomerAdd";
            this.customerAddToolStripMenuItem.Click += new System.EventHandler(this.customerAddToolStripMenuItem_Click);
            // 
            // customerListToolStripMenuItem
            // 
            this.customerListToolStripMenuItem.Name = "customerListToolStripMenuItem";
            this.customerListToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.customerListToolStripMenuItem.Text = "Customer List";
            this.customerListToolStripMenuItem.Click += new System.EventHandler(this.customerListToolStripMenuItem_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monthlySalesToolStripMenuItem,
            this.yearlySalesToolStripMenuItem,
            this.profitsToolStripMenuItem,
            this.lossToolStripMenuItem,
            this.graphicalDataToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // monthlySalesToolStripMenuItem
            // 
            this.monthlySalesToolStripMenuItem.Name = "monthlySalesToolStripMenuItem";
            this.monthlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.monthlySalesToolStripMenuItem.Text = "MonthlySales";
            // 
            // yearlySalesToolStripMenuItem
            // 
            this.yearlySalesToolStripMenuItem.Name = "yearlySalesToolStripMenuItem";
            this.yearlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yearlySalesToolStripMenuItem.Text = "YearlySales";
            // 
            // profitsToolStripMenuItem
            // 
            this.profitsToolStripMenuItem.Name = "profitsToolStripMenuItem";
            this.profitsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.profitsToolStripMenuItem.Text = "Profits";
            // 
            // lossToolStripMenuItem
            // 
            this.lossToolStripMenuItem.Name = "lossToolStripMenuItem";
            this.lossToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lossToolStripMenuItem.Text = "Loss";
            // 
            // graphicalDataToolStripMenuItem
            // 
            this.graphicalDataToolStripMenuItem.Name = "graphicalDataToolStripMenuItem";
            this.graphicalDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.graphicalDataToolStripMenuItem.Text = "GraphicalData";
            // 
            // mAINMENUToolStripMenuItem
            // 
            this.mAINMENUToolStripMenuItem.Name = "mAINMENUToolStripMenuItem";
            this.mAINMENUToolStripMenuItem.Size = new System.Drawing.Size(106, 21);
            this.mAINMENUToolStripMenuItem.Text = "MAIN MENU";
            this.mAINMENUToolStripMenuItem.Click += new System.EventHandler(this.mAINMENUToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.btwBack);
            this.panel1.Controls.Add(this.btwCustomerList);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(241, 58);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 669);
            this.panel1.TabIndex = 4;
            // 
            // btwBack
            // 
            this.btwBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBack.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBack.Location = new System.Drawing.Point(4, 8);
            this.btwBack.Margin = new System.Windows.Forms.Padding(2);
            this.btwBack.Name = "btwBack";
            this.btwBack.Size = new System.Drawing.Size(79, 28);
            this.btwBack.TabIndex = 57;
            this.btwBack.Text = "<--";
            this.btwBack.UseVisualStyleBackColor = true;
            this.btwBack.Click += new System.EventHandler(this.btwBack_Click);
            // 
            // btwCustomerList
            // 
            this.btwCustomerList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwCustomerList.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwCustomerList.Location = new System.Drawing.Point(706, 10);
            this.btwCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.btwCustomerList.Name = "btwCustomerList";
            this.btwCustomerList.Size = new System.Drawing.Size(137, 28);
            this.btwCustomerList.TabIndex = 56;
            this.btwCustomerList.Text = "Customer List";
            this.btwCustomerList.UseVisualStyleBackColor = true;
            this.btwCustomerList.Click += new System.EventHandler(this.btwCustomerList_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btwDeleteCustomer);
            this.panel3.Controls.Add(this.btwUpdateCustomer);
            this.panel3.Controls.Add(this.btwAddCustomer);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.btwCustomerBillCustomer);
            this.panel3.Controls.Add(this.txtPhoneNomaskedVendor);
            this.panel3.Controls.Add(this.txtNameVendor);
            this.panel3.Controls.Add(this.txtAddressVendor);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.Location = new System.Drawing.Point(4, 43);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(856, 709);
            this.panel3.TabIndex = 1;
            // 
            // btwDeleteCustomer
            // 
            this.btwDeleteCustomer.Location = new System.Drawing.Point(724, 31);
            this.btwDeleteCustomer.Name = "btwDeleteCustomer";
            this.btwDeleteCustomer.Size = new System.Drawing.Size(115, 44);
            this.btwDeleteCustomer.TabIndex = 63;
            this.btwDeleteCustomer.Text = "Delete";
            this.btwDeleteCustomer.UseVisualStyleBackColor = true;
            this.btwDeleteCustomer.Click += new System.EventHandler(this.btwDeleteCustomer_Click);
            // 
            // btwUpdateCustomer
            // 
            this.btwUpdateCustomer.Location = new System.Drawing.Point(724, 81);
            this.btwUpdateCustomer.Name = "btwUpdateCustomer";
            this.btwUpdateCustomer.Size = new System.Drawing.Size(115, 45);
            this.btwUpdateCustomer.TabIndex = 62;
            this.btwUpdateCustomer.Text = "Update";
            this.btwUpdateCustomer.UseVisualStyleBackColor = true;
            this.btwUpdateCustomer.Click += new System.EventHandler(this.btwDeleteCustomer_Click);
            // 
            // btwAddCustomer
            // 
            this.btwAddCustomer.Enabled = false;
            this.btwAddCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwAddCustomer.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwAddCustomer.Location = new System.Drawing.Point(724, 184);
            this.btwAddCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.btwAddCustomer.Name = "btwAddCustomer";
            this.btwAddCustomer.Size = new System.Drawing.Size(115, 50);
            this.btwAddCustomer.TabIndex = 61;
            this.btwAddCustomer.Text = "AddCustomer";
            this.btwAddCustomer.UseVisualStyleBackColor = true;
            this.btwAddCustomer.Click += new System.EventHandler(this.btwAddCustomer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DGBills);
            this.groupBox1.Location = new System.Drawing.Point(20, 206);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(836, 415);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bills";
            // 
            // DGBills
            // 
            this.DGBills.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGBills.Location = new System.Drawing.Point(4, 36);
            this.DGBills.Margin = new System.Windows.Forms.Padding(2);
            this.DGBills.Name = "DGBills";
            this.DGBills.RowTemplate.Height = 24;
            this.DGBills.Size = new System.Drawing.Size(828, 417);
            this.DGBills.TabIndex = 0;
            // 
            // btwCustomerBillCustomer
            // 
            this.btwCustomerBillCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwCustomerBillCustomer.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwCustomerBillCustomer.Location = new System.Drawing.Point(724, 130);
            this.btwCustomerBillCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.btwCustomerBillCustomer.Name = "btwCustomerBillCustomer";
            this.btwCustomerBillCustomer.Size = new System.Drawing.Size(115, 50);
            this.btwCustomerBillCustomer.TabIndex = 59;
            this.btwCustomerBillCustomer.Text = "New Bill";
            this.btwCustomerBillCustomer.UseVisualStyleBackColor = true;
            this.btwCustomerBillCustomer.Click += new System.EventHandler(this.btwCustomerBillCustomer_Click);
            // 
            // txtPhoneNomaskedVendor
            // 
            this.txtPhoneNomaskedVendor.Location = new System.Drawing.Point(165, 65);
            this.txtPhoneNomaskedVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNomaskedVendor.Name = "txtPhoneNomaskedVendor";
            this.txtPhoneNomaskedVendor.Size = new System.Drawing.Size(240, 23);
            this.txtPhoneNomaskedVendor.TabIndex = 6;
            // 
            // txtNameVendor
            // 
            this.txtNameVendor.Location = new System.Drawing.Point(165, 31);
            this.txtNameVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtNameVendor.Name = "txtNameVendor";
            this.txtNameVendor.Size = new System.Drawing.Size(243, 23);
            this.txtNameVendor.TabIndex = 9;
            this.txtNameVendor.TextChanged += new System.EventHandler(this.txtNameVendor_TextChanged);
            // 
            // txtAddressVendor
            // 
            this.txtAddressVendor.Location = new System.Drawing.Point(165, 103);
            this.txtAddressVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddressVendor.Multiline = true;
            this.txtAddressVendor.Name = "txtAddressVendor";
            this.txtAddressVendor.Size = new System.Drawing.Size(243, 73);
            this.txtAddressVendor.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(20, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 29);
            this.label3.TabIndex = 8;
            this.label3.Text = "Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(20, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Phone No:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(20, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Name:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(351, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(141, 36);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(13, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CustomerMainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1311, 749);
            this.Controls.Add(this.MainmenuStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "CustomerMainPage";
            this.Text = "CustomerMainPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGBills)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicalDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mAINMENUToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btwBack;
        private System.Windows.Forms.Button btwCustomerList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btwAddCustomer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DGBills;
        private System.Windows.Forms.Button btwCustomerBillCustomer;
        private System.Windows.Forms.MaskedTextBox txtPhoneNomaskedVendor;
        private System.Windows.Forms.TextBox txtNameVendor;
        private System.Windows.Forms.TextBox txtAddressVendor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btwDeleteCustomer;
        private System.Windows.Forms.Button btwUpdateCustomer;
    }
}