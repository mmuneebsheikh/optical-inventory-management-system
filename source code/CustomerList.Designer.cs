﻿namespace VisualProjectOpInventory
{
    partial class CustomerList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerList));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btwOrders = new System.Windows.Forms.Button();
            this.btwAddCustomerList = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btwShowCustomer = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtSearchCustomerList = new System.Windows.Forms.TextBox();
            this.btwSearchCustomerList = new System.Windows.Forms.Button();
            this.DGCustomerList = new System.Windows.Forms.DataGridView();
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicalDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mAINMENUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGCustomerList)).BeginInit();
            this.MainmenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btwShowCustomer);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.DGCustomerList);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(934, 470);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btwOrders);
            this.panel2.Controls.Add(this.btwAddCustomerList);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(0, 28);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(932, 81);
            this.panel2.TabIndex = 6;
            // 
            // btwOrders
            // 
            this.btwOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwOrders.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwOrders.Location = new System.Drawing.Point(691, 2);
            this.btwOrders.Margin = new System.Windows.Forms.Padding(2);
            this.btwOrders.Name = "btwOrders";
            this.btwOrders.Size = new System.Drawing.Size(117, 41);
            this.btwOrders.TabIndex = 3;
            this.btwOrders.Text = "ORDERS";
            this.btwOrders.UseVisualStyleBackColor = true;
            this.btwOrders.Click += new System.EventHandler(this.btwOrders_Click);
            // 
            // btwAddCustomerList
            // 
            this.btwAddCustomerList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwAddCustomerList.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwAddCustomerList.Location = new System.Drawing.Point(813, 2);
            this.btwAddCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.btwAddCustomerList.Name = "btwAddCustomerList";
            this.btwAddCustomerList.Size = new System.Drawing.Size(117, 41);
            this.btwAddCustomerList.TabIndex = 2;
            this.btwAddCustomerList.Text = "AddCustomer";
            this.btwAddCustomerList.UseVisualStyleBackColor = true;
            this.btwAddCustomerList.Click += new System.EventHandler(this.btwBackCustomerList_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(414, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Customer List";
            // 
            // btwShowCustomer
            // 
            this.btwShowCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwShowCustomer.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwShowCustomer.Location = new System.Drawing.Point(813, 113);
            this.btwShowCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.btwShowCustomer.Name = "btwShowCustomer";
            this.btwShowCustomer.Size = new System.Drawing.Size(117, 64);
            this.btwShowCustomer.TabIndex = 7;
            this.btwShowCustomer.Text = "Show Customer";
            this.btwShowCustomer.UseVisualStyleBackColor = true;
            this.btwShowCustomer.Click += new System.EventHandler(this.btwAddNewBill_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtSearchCustomerList);
            this.panel4.Controls.Add(this.btwSearchCustomerList);
            this.panel4.Location = new System.Drawing.Point(2, 114);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(806, 63);
            this.panel4.TabIndex = 4;
            // 
            // txtSearchCustomerList
            // 
            this.txtSearchCustomerList.Location = new System.Drawing.Point(0, 0);
            this.txtSearchCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchCustomerList.Name = "txtSearchCustomerList";
            this.txtSearchCustomerList.Size = new System.Drawing.Size(753, 20);
            this.txtSearchCustomerList.TabIndex = 4;
            // 
            // btwSearchCustomerList
            // 
            this.btwSearchCustomerList.Font = new System.Drawing.Font("Lucida Sans Unicode", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSearchCustomerList.Location = new System.Drawing.Point(751, 0);
            this.btwSearchCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.btwSearchCustomerList.Name = "btwSearchCustomerList";
            this.btwSearchCustomerList.Size = new System.Drawing.Size(56, 22);
            this.btwSearchCustomerList.TabIndex = 3;
            this.btwSearchCustomerList.Text = "Go";
            this.btwSearchCustomerList.UseVisualStyleBackColor = true;
            this.btwSearchCustomerList.Click += new System.EventHandler(this.btwSearchCustomerList_Click);
            // 
            // DGCustomerList
            // 
            this.DGCustomerList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGCustomerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGCustomerList.Location = new System.Drawing.Point(9, 182);
            this.DGCustomerList.Margin = new System.Windows.Forms.Padding(2);
            this.DGCustomerList.Name = "DGCustomerList";
            this.DGCustomerList.RowTemplate.Height = 24;
            this.DGCustomerList.Size = new System.Drawing.Size(916, 270);
            this.DGCustomerList.TabIndex = 9;
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.mAINMENUToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.MainmenuStrip1.Size = new System.Drawing.Size(934, 25);
            this.MainmenuStrip1.TabIndex = 4;
            this.MainmenuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ordersToolStripMenuItem.Text = "New Vendor";
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem1.Text = "Vendor List";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem2,
            this.ordersToolStripMenuItem1,
            this.customerAddToolStripMenuItem,
            this.customerNameToolStripMenuItem,
            this.customerRemoveToolStripMenuItem,
            this.customerUpdateToolStripMenuItem,
            this.customerStatusToolStripMenuItem,
            this.customerPaymentStatusToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(88, 21);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // customerToolStripMenuItem2
            // 
            this.customerToolStripMenuItem2.Name = "customerToolStripMenuItem2";
            this.customerToolStripMenuItem2.Size = new System.Drawing.Size(252, 22);
            this.customerToolStripMenuItem2.Text = "CustomerPage";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.ordersToolStripMenuItem1.Text = "Orders";
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerAddToolStripMenuItem.Text = "CustomerAdd";
            // 
            // customerNameToolStripMenuItem
            // 
            this.customerNameToolStripMenuItem.Name = "customerNameToolStripMenuItem";
            this.customerNameToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerNameToolStripMenuItem.Text = "CustomerName";
            // 
            // customerRemoveToolStripMenuItem
            // 
            this.customerRemoveToolStripMenuItem.Name = "customerRemoveToolStripMenuItem";
            this.customerRemoveToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerRemoveToolStripMenuItem.Text = "CustomerRemove";
            // 
            // customerUpdateToolStripMenuItem
            // 
            this.customerUpdateToolStripMenuItem.Name = "customerUpdateToolStripMenuItem";
            this.customerUpdateToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerUpdateToolStripMenuItem.Text = "CustomerUpdate";
            // 
            // customerStatusToolStripMenuItem
            // 
            this.customerStatusToolStripMenuItem.Name = "customerStatusToolStripMenuItem";
            this.customerStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerStatusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // customerPaymentStatusToolStripMenuItem
            // 
            this.customerPaymentStatusToolStripMenuItem.Name = "customerPaymentStatusToolStripMenuItem";
            this.customerPaymentStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerPaymentStatusToolStripMenuItem.Text = "CustomerPaymentStatus";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monthlySalesToolStripMenuItem,
            this.yearlySalesToolStripMenuItem,
            this.profitsToolStripMenuItem,
            this.lossToolStripMenuItem,
            this.graphicalDataToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // monthlySalesToolStripMenuItem
            // 
            this.monthlySalesToolStripMenuItem.Name = "monthlySalesToolStripMenuItem";
            this.monthlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.monthlySalesToolStripMenuItem.Text = "MonthlySales";
            // 
            // yearlySalesToolStripMenuItem
            // 
            this.yearlySalesToolStripMenuItem.Name = "yearlySalesToolStripMenuItem";
            this.yearlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yearlySalesToolStripMenuItem.Text = "YearlySales";
            // 
            // profitsToolStripMenuItem
            // 
            this.profitsToolStripMenuItem.Name = "profitsToolStripMenuItem";
            this.profitsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.profitsToolStripMenuItem.Text = "Profits";
            // 
            // lossToolStripMenuItem
            // 
            this.lossToolStripMenuItem.Name = "lossToolStripMenuItem";
            this.lossToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lossToolStripMenuItem.Text = "Loss";
            // 
            // graphicalDataToolStripMenuItem
            // 
            this.graphicalDataToolStripMenuItem.Name = "graphicalDataToolStripMenuItem";
            this.graphicalDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.graphicalDataToolStripMenuItem.Text = "GraphicalData";
            // 
            // mAINMENUToolStripMenuItem
            // 
            this.mAINMENUToolStripMenuItem.Name = "mAINMENUToolStripMenuItem";
            this.mAINMENUToolStripMenuItem.Size = new System.Drawing.Size(106, 21);
            this.mAINMENUToolStripMenuItem.Text = "MAIN MENU";
            this.mAINMENUToolStripMenuItem.Click += new System.EventHandler(this.mAINMENUToolStripMenuItem_Click);
            // 
            // CustomerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(934, 567);
            this.Controls.Add(this.MainmenuStrip1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CustomerList";
            this.Text = "CustomerList";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CustomerList_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGCustomerList)).EndInit();
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btwAddCustomerList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btwShowCustomer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtSearchCustomerList;
        private System.Windows.Forms.Button btwSearchCustomerList;
        private System.Windows.Forms.DataGridView DGCustomerList;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicalDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mAINMENUToolStripMenuItem;
        private System.Windows.Forms.Button btwOrders;
    }
}