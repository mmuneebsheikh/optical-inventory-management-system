﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void btwExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btwCustomer_Click(object sender, EventArgs e)
        {
            CustomerList customerlistobject = new CustomerList();
            this.Hide();
            customerlistobject.Show();
        }

        private void btwVendors_Click(object sender, EventArgs e)
        {
            VendorsOrder obj1 = new VendorsOrder();
            this.Hide();
            this.Dispose();
            obj1.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btwSalesMain_Click(object sender, EventArgs e)
        {
            Login loginpageobject = new Login();
            this.Hide();
            loginpageobject.Show();
        }

        private void btwBillMain_Click(object sender, EventArgs e)
        {
            Login loginpageobjectbill = new Login();
            this.Hide();
            loginpageobjectbill.Show();

        }
    }
}
