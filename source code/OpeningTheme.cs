﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class OpeningTheme : Form
    {
        public OpeningTheme()
        {
            InitializeComponent();
        }
        bool flag;
        int count = 0;
        private void OpeningTheme_Load(object sender, EventArgs e)
        {
            pictureBox1.Visible = false;
            btwCustomerOpeningTheme.Hide();
            btwVendorsOpeningTheme.Hide(); 
            btwSalesOpeningTheme.Hide();
            btwBillOpeningTheme.Hide(); 
            btwViewOpeningTheme.Hide();
            btwExitOpeningTheme.Hide();
            flag = true;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (flag)
            {
                btwCustomerOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape1 = new System.Drawing.Drawing2D.GraphicsPath();
                shape1.AddEllipse(1, 5, 100, 70);
                btwCustomerOpeningTheme.Region = new System.Drawing.Region(shape1);
                flag = false;
            }
            count++;
            if (count == 5)
            {
                btwVendorsOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape2 = new System.Drawing.Drawing2D.GraphicsPath();
                shape2.AddEllipse(1, 5, 100, 70);
                btwVendorsOpeningTheme.Region = new System.Drawing.Region(shape2);
            }
            if (count == 10)
            {
                btwSalesOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape3 = new System.Drawing.Drawing2D.GraphicsPath();
                shape3.AddEllipse(1, 5, 100, 70);
                btwSalesOpeningTheme.Region = new System.Drawing.Region(shape3);
            }
            if (count == 15)
            {
                btwBillOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape4 = new System.Drawing.Drawing2D.GraphicsPath();
                shape4.AddEllipse(1, 5, 100, 70);
                btwBillOpeningTheme.Region = new System.Drawing.Region(shape4);
            }
            if (count == 20)
            {
                btwViewOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape5 = new System.Drawing.Drawing2D.GraphicsPath();
                shape5.AddEllipse(1, 5, 100, 70);
                btwViewOpeningTheme.Region = new System.Drawing.Region(shape5);
               
            }
            if (count==25)
            {
                btwExitOpeningTheme.Show();
                System.Drawing.Drawing2D.GraphicsPath shape6 = new System.Drawing.Drawing2D.GraphicsPath();
                shape6.AddEllipse(1, 5, 100, 70);
                btwExitOpeningTheme.Region = new System.Drawing.Region(shape6);
                
            }
            if (count==30)
            {
                //pictureBox1.Enabled = true;
                pictureBox1.Visible = true;
                timer1.Stop();
            }
        }

        private void btwExitOpeningTheme_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btwCustomerOpeningTheme_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerMainPage cust = new CustomerMainPage();
            cust.Show();
            //customerlistobject.Show();
        }

        private void btwVendorsOpeningTheme_Click(object sender, EventArgs e)
        {
            Vendor vendorsearchobject = new Vendor();
            //this.Close();
            this.Hide();
            vendorsearchobject.Show();
        }

        private void btwSalesOpeningTheme_Click(object sender, EventArgs e)
        {
            Login loginobj = new Login("Sales",null);
            this.Close();
            this.Hide();
            loginobj.Show();
        }

        private void btwBillOpeningTheme_Click(object sender, EventArgs e)
        {
            Login loginobjbill = new Login("Admin",null);
            //this.Close();
            this.Hide();
            loginobjbill.Show();
        }

        private void btwViewOpeningTheme_Click(object sender, EventArgs e)
        {
            Inventory inventory = new Inventory();
            //this.Close();
            this.Hide();
            inventory.Show();
        }
    }
}
