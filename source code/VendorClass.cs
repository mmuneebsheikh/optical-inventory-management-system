﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualProjectOpInventory
{
    public class VendorClass
    {
        
        public string Name { get; set; }
        public string contactNo { get; set; }
        public string Address { get; set; }
        public double balance { get; set; }
        public double credit { get; set; }

        public class Product
        {
            public string prodName { get; set; }
            public string modelNo { get; set; }
            public string color { get; set; }
            public string category { get; set; }
            public int size { get; set; }
            public int qty { get; set; }
            public string gender { get; set; }
            public double wholesalePrice { get; set; }
            public double retailPrice { get; set; }
            // DateTime date { get; set; }
        }
        public Product prod = new Product();

        public class Bill
        {
            public Bill()
            {
                products = new List<Product>();
            }
            public int BillNo { get; set; }
            public DateTime OrderDate { get; set; }
            public DateTime DueDate { get; set; }
            public List <Product> products { get; set; }
            public string status { get; set; }
            public class Amount
            {
                public double totalBill { get; set; }
                public double paid { get; set; }
                public double balance { get; set; }
            }
            public Amount amount = new Amount();
        }
        public Bill bill = new Bill();
    }
}
