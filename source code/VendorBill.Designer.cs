﻿namespace VisualProjectOpInventory
{
    partial class VendorBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtCancel = new System.Windows.Forms.Button();
            this.btwEdit = new System.Windows.Forms.Button();
            this.txtTotalBill = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTotProdPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDueDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOrderDate = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.btwSaveVendor = new System.Windows.Forms.Button();
            this.lblStatusVendorBill = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCredit = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVendorNameVendorBill = new System.Windows.Forms.TextBox();
            this.DGProductsVendor = new System.Windows.Forms.DataGridView();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Color = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RetailPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WholeSalePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totProdPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Image = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btwAddVendor = new System.Windows.Forms.Button();
            this.txtWholeSalePriceVendor = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButtonFemaleVendor = new System.Windows.Forms.RadioButton();
            this.radioButtonMaleVendor = new System.Windows.Forms.RadioButton();
            this.txtRetailPriceVendor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtQuantityVendor = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboxCategoryVendor = new System.Windows.Forms.ComboBox();
            this.txtColorVendor = new System.Windows.Forms.TextBox();
            this.cboxSizeVendor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtModelNoVendor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIProductNameVendor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGProductsVendor)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.txtCancel);
            this.panel1.Controls.Add(this.btwEdit);
            this.panel1.Controls.Add(this.txtTotalBill);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtTotProdPrice);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtDueDate);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtOrderDate);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.btwSaveVendor);
            this.panel1.Controls.Add(this.lblStatusVendorBill);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtBalance);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txtCredit);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtVendorNameVendorBill);
            this.panel1.Controls.Add(this.DGProductsVendor);
            this.panel1.Controls.Add(this.btwAddVendor);
            this.panel1.Controls.Add(this.txtWholeSalePriceVendor);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.radioButtonFemaleVendor);
            this.panel1.Controls.Add(this.radioButtonMaleVendor);
            this.panel1.Controls.Add(this.txtRetailPriceVendor);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtQuantityVendor);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.cboxCategoryVendor);
            this.panel1.Controls.Add(this.txtColorVendor);
            this.panel1.Controls.Add(this.cboxSizeVendor);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtModelNoVendor);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtIProductNameVendor);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(5, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(868, 697);
            this.panel1.TabIndex = 0;
            // 
            // txtCancel
            // 
            this.txtCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCancel.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtCancel.Location = new System.Drawing.Point(781, 10);
            this.txtCancel.Margin = new System.Windows.Forms.Padding(2);
            this.txtCancel.Name = "txtCancel";
            this.txtCancel.Size = new System.Drawing.Size(73, 29);
            this.txtCancel.TabIndex = 98;
            this.txtCancel.Text = "Back";
            this.txtCancel.UseVisualStyleBackColor = true;
            this.txtCancel.Click += new System.EventHandler(this.txtCancel_Click);
            // 
            // btwEdit
            // 
            this.btwEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwEdit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwEdit.Location = new System.Drawing.Point(299, 612);
            this.btwEdit.Margin = new System.Windows.Forms.Padding(2);
            this.btwEdit.Name = "btwEdit";
            this.btwEdit.Size = new System.Drawing.Size(58, 29);
            this.btwEdit.TabIndex = 97;
            this.btwEdit.Text = "Edit";
            this.btwEdit.UseVisualStyleBackColor = true;
            this.btwEdit.Click += new System.EventHandler(this.btwEdit_Click);
            // 
            // txtTotalBill
            // 
            this.txtTotalBill.Location = new System.Drawing.Point(299, 651);
            this.txtTotalBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalBill.Name = "txtTotalBill";
            this.txtTotalBill.ReadOnly = true;
            this.txtTotalBill.Size = new System.Drawing.Size(216, 20);
            this.txtTotalBill.TabIndex = 96;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label18.Location = new System.Drawing.Point(178, 643);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(125, 29);
            this.label18.TabIndex = 95;
            this.label18.Text = "Total Bill:";
            // 
            // txtTotProdPrice
            // 
            this.txtTotProdPrice.Location = new System.Drawing.Point(199, 310);
            this.txtTotProdPrice.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotProdPrice.Name = "txtTotProdPrice";
            this.txtTotProdPrice.ReadOnly = true;
            this.txtTotProdPrice.Size = new System.Drawing.Size(216, 20);
            this.txtTotProdPrice.TabIndex = 94;
            this.txtTotProdPrice.TextChanged += new System.EventHandler(this.txtTotProdPrice_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(20, 301);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 29);
            this.label4.TabIndex = 93;
            this.label4.Text = "Total Price:";
            // 
            // txtDueDate
            // 
            this.txtDueDate.Location = new System.Drawing.Point(199, 58);
            this.txtDueDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(216, 20);
            this.txtDueDate.TabIndex = 92;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(16, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 29);
            this.label3.TabIndex = 91;
            this.label3.Text = "Due Date:";
            // 
            // txtOrderDate
            // 
            this.txtOrderDate.Location = new System.Drawing.Point(199, 19);
            this.txtOrderDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrderDate.Name = "txtOrderDate";
            this.txtOrderDate.Size = new System.Drawing.Size(216, 20);
            this.txtOrderDate.TabIndex = 90;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(16, 13);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 29);
            this.label11.TabIndex = 89;
            this.label11.Text = "OrderDate";
            // 
            // btwSaveVendor
            // 
            this.btwSaveVendor.Enabled = false;
            this.btwSaveVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSaveVendor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwSaveVendor.Location = new System.Drawing.Point(14, 610);
            this.btwSaveVendor.Margin = new System.Windows.Forms.Padding(2);
            this.btwSaveVendor.Name = "btwSaveVendor";
            this.btwSaveVendor.Size = new System.Drawing.Size(134, 64);
            this.btwSaveVendor.TabIndex = 88;
            this.btwSaveVendor.Text = "Save";
            this.btwSaveVendor.UseVisualStyleBackColor = true;
            this.btwSaveVendor.Click += new System.EventHandler(this.btwSaveVendor_Click);
            // 
            // lblStatusVendorBill
            // 
            this.lblStatusVendorBill.AutoSize = true;
            this.lblStatusVendorBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatusVendorBill.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblStatusVendorBill.Location = new System.Drawing.Point(97, 352);
            this.lblStatusVendorBill.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatusVendorBill.Name = "lblStatusVendorBill";
            this.lblStatusVendorBill.Size = new System.Drawing.Size(76, 24);
            this.lblStatusVendorBill.TabIndex = 87;
            this.lblStatusVendorBill.Text = "Unpaid";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(21, 352);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 24);
            this.label2.TabIndex = 86;
            this.label2.Text = "Status:";
            // 
            // txtBalance
            // 
            this.txtBalance.Location = new System.Drawing.Point(638, 653);
            this.txtBalance.Margin = new System.Windows.Forms.Padding(2);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(216, 20);
            this.txtBalance.TabIndex = 85;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label16.Location = new System.Drawing.Point(524, 646);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 29);
            this.label16.TabIndex = 84;
            this.label16.Text = "Balance:";
            // 
            // txtCredit
            // 
            this.txtCredit.Location = new System.Drawing.Point(638, 617);
            this.txtCredit.Margin = new System.Windows.Forms.Padding(2);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(216, 20);
            this.txtCredit.TabIndex = 83;
            this.txtCredit.TextChanged += new System.EventHandler(this.txtCredit_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label17.Location = new System.Drawing.Point(524, 617);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 29);
            this.label17.TabIndex = 82;
            this.label17.Text = "Credit:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(442, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 29);
            this.label1.TabIndex = 80;
            this.label1.Text = "VendorName:";
            // 
            // txtVendorNameVendorBill
            // 
            this.txtVendorNameVendorBill.Location = new System.Drawing.Point(638, 48);
            this.txtVendorNameVendorBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtVendorNameVendorBill.Name = "txtVendorNameVendorBill";
            this.txtVendorNameVendorBill.ReadOnly = true;
            this.txtVendorNameVendorBill.Size = new System.Drawing.Size(216, 20);
            this.txtVendorNameVendorBill.TabIndex = 81;
            // 
            // DGProductsVendor
            // 
            this.DGProductsVendor.AllowUserToDeleteRows = false;
            this.DGProductsVendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGProductsVendor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductName,
            this.ModelNo,
            this.Color,
            this.Quantity,
            this.ProdSize,
            this.Category,
            this.Gender,
            this.RetailPrice,
            this.WholeSalePrice,
            this.totProdPrice,
            this.Image});
            this.DGProductsVendor.Location = new System.Drawing.Point(21, 378);
            this.DGProductsVendor.Margin = new System.Windows.Forms.Padding(2);
            this.DGProductsVendor.Name = "DGProductsVendor";
            this.DGProductsVendor.ReadOnly = true;
            this.DGProductsVendor.RowTemplate.Height = 24;
            this.DGProductsVendor.Size = new System.Drawing.Size(840, 217);
            this.DGProductsVendor.TabIndex = 79;
            // 
            // ProductName
            // 
            this.ProductName.HeaderText = "ProductName";
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            // 
            // ModelNo
            // 
            this.ModelNo.HeaderText = "ModelNo";
            this.ModelNo.Name = "ModelNo";
            this.ModelNo.ReadOnly = true;
            // 
            // Color
            // 
            this.Color.HeaderText = "Color";
            this.Color.Name = "Color";
            this.Color.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Quantity";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // ProdSize
            // 
            this.ProdSize.HeaderText = "Size";
            this.ProdSize.Name = "ProdSize";
            this.ProdSize.ReadOnly = true;
            // 
            // Category
            // 
            this.Category.HeaderText = "Category";
            this.Category.Name = "Category";
            this.Category.ReadOnly = true;
            // 
            // Gender
            // 
            this.Gender.HeaderText = "Gender";
            this.Gender.Name = "Gender";
            this.Gender.ReadOnly = true;
            // 
            // RetailPrice
            // 
            this.RetailPrice.HeaderText = "RetailPrice";
            this.RetailPrice.Name = "RetailPrice";
            this.RetailPrice.ReadOnly = true;
            // 
            // WholeSalePrice
            // 
            this.WholeSalePrice.HeaderText = "WholeSalePrice";
            this.WholeSalePrice.Name = "WholeSalePrice";
            this.WholeSalePrice.ReadOnly = true;
            // 
            // totProdPrice
            // 
            this.totProdPrice.HeaderText = "Total Product Ammount";
            this.totProdPrice.Name = "totProdPrice";
            this.totProdPrice.ReadOnly = true;
            // 
            // Image
            // 
            this.Image.HeaderText = "Image";
            this.Image.Name = "Image";
            this.Image.ReadOnly = true;
            // 
            // btwAddVendor
            // 
            this.btwAddVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwAddVendor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwAddVendor.Location = new System.Drawing.Point(624, 190);
            this.btwAddVendor.Margin = new System.Windows.Forms.Padding(2);
            this.btwAddVendor.Name = "btwAddVendor";
            this.btwAddVendor.Size = new System.Drawing.Size(214, 45);
            this.btwAddVendor.TabIndex = 78;
            this.btwAddVendor.Text = "Add ";
            this.btwAddVendor.UseVisualStyleBackColor = true;
            this.btwAddVendor.Click += new System.EventHandler(this.btwAddVendor_Click);
            // 
            // txtWholeSalePriceVendor
            // 
            this.txtWholeSalePriceVendor.Location = new System.Drawing.Point(622, 151);
            this.txtWholeSalePriceVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtWholeSalePriceVendor.Name = "txtWholeSalePriceVendor";
            this.txtWholeSalePriceVendor.Size = new System.Drawing.Size(216, 20);
            this.txtWholeSalePriceVendor.TabIndex = 77;
            this.txtWholeSalePriceVendor.Leave += new System.EventHandler(this.Qty_WholeSale_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(451, 151);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(150, 20);
            this.label10.TabIndex = 76;
            this.label10.Text = "Whole Sale Price:";
            // 
            // radioButtonFemaleVendor
            // 
            this.radioButtonFemaleVendor.AutoSize = true;
            this.radioButtonFemaleVendor.Location = new System.Drawing.Point(753, 92);
            this.radioButtonFemaleVendor.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonFemaleVendor.Name = "radioButtonFemaleVendor";
            this.radioButtonFemaleVendor.Size = new System.Drawing.Size(59, 17);
            this.radioButtonFemaleVendor.TabIndex = 75;
            this.radioButtonFemaleVendor.TabStop = true;
            this.radioButtonFemaleVendor.Text = "Female";
            this.radioButtonFemaleVendor.UseVisualStyleBackColor = true;
            // 
            // radioButtonMaleVendor
            // 
            this.radioButtonMaleVendor.AutoSize = true;
            this.radioButtonMaleVendor.Location = new System.Drawing.Point(622, 92);
            this.radioButtonMaleVendor.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonMaleVendor.Name = "radioButtonMaleVendor";
            this.radioButtonMaleVendor.Size = new System.Drawing.Size(48, 17);
            this.radioButtonMaleVendor.TabIndex = 74;
            this.radioButtonMaleVendor.TabStop = true;
            this.radioButtonMaleVendor.Text = "Male";
            this.radioButtonMaleVendor.UseVisualStyleBackColor = true;
            // 
            // txtRetailPriceVendor
            // 
            this.txtRetailPriceVendor.Location = new System.Drawing.Point(622, 115);
            this.txtRetailPriceVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtRetailPriceVendor.Name = "txtRetailPriceVendor";
            this.txtRetailPriceVendor.Size = new System.Drawing.Size(216, 20);
            this.txtRetailPriceVendor.TabIndex = 71;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label14.Location = new System.Drawing.Point(495, 115);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 20);
            this.label14.TabIndex = 70;
            this.label14.Text = "Retail Price:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label15.Location = new System.Drawing.Point(494, 82);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 29);
            this.label15.TabIndex = 69;
            this.label15.Text = "Gender:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(20, 224);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 29);
            this.label6.TabIndex = 59;
            this.label6.Text = "Size:";
            // 
            // txtQuantityVendor
            // 
            this.txtQuantityVendor.Location = new System.Drawing.Point(199, 199);
            this.txtQuantityVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtQuantityVendor.Name = "txtQuantityVendor";
            this.txtQuantityVendor.Size = new System.Drawing.Size(216, 20);
            this.txtQuantityVendor.TabIndex = 67;
            this.txtQuantityVendor.Leave += new System.EventHandler(this.Qty_WholeSale_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label13.Location = new System.Drawing.Point(20, 190);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 29);
            this.label13.TabIndex = 66;
            this.label13.Text = "Quantity";
            // 
            // cboxCategoryVendor
            // 
            this.cboxCategoryVendor.FormattingEnabled = true;
            this.cboxCategoryVendor.Items.AddRange(new object[] {
            "Frames",
            "Sunglass"});
            this.cboxCategoryVendor.Location = new System.Drawing.Point(199, 274);
            this.cboxCategoryVendor.Margin = new System.Windows.Forms.Padding(2);
            this.cboxCategoryVendor.Name = "cboxCategoryVendor";
            this.cboxCategoryVendor.Size = new System.Drawing.Size(216, 21);
            this.cboxCategoryVendor.TabIndex = 65;
            this.cboxCategoryVendor.Text = "Select";
            // 
            // txtColorVendor
            // 
            this.txtColorVendor.Location = new System.Drawing.Point(199, 168);
            this.txtColorVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtColorVendor.Name = "txtColorVendor";
            this.txtColorVendor.Size = new System.Drawing.Size(216, 20);
            this.txtColorVendor.TabIndex = 64;
            // 
            // cboxSizeVendor
            // 
            this.cboxSizeVendor.FormattingEnabled = true;
            this.cboxSizeVendor.Items.AddRange(new object[] {
            "48",
            "50",
            "52",
            "53",
            "54",
            "58"});
            this.cboxSizeVendor.Location = new System.Drawing.Point(199, 234);
            this.cboxSizeVendor.Margin = new System.Windows.Forms.Padding(2);
            this.cboxSizeVendor.Name = "cboxSizeVendor";
            this.cboxSizeVendor.Size = new System.Drawing.Size(216, 21);
            this.cboxSizeVendor.TabIndex = 63;
            this.cboxSizeVendor.Text = "Select";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(9, 97);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(186, 29);
            this.label9.TabIndex = 56;
            this.label9.Text = "Product Name:";
            // 
            // txtModelNoVendor
            // 
            this.txtModelNoVendor.Location = new System.Drawing.Point(199, 140);
            this.txtModelNoVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtModelNoVendor.Name = "txtModelNoVendor";
            this.txtModelNoVendor.Size = new System.Drawing.Size(216, 20);
            this.txtModelNoVendor.TabIndex = 62;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(20, 131);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 29);
            this.label8.TabIndex = 57;
            this.label8.Text = "Model No:";
            // 
            // txtIProductNameVendor
            // 
            this.txtIProductNameVendor.Location = new System.Drawing.Point(199, 106);
            this.txtIProductNameVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtIProductNameVendor.Name = "txtIProductNameVendor";
            this.txtIProductNameVendor.Size = new System.Drawing.Size(216, 20);
            this.txtIProductNameVendor.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(20, 159);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 29);
            this.label5.TabIndex = 58;
            this.label5.Text = "Color:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(20, 265);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 29);
            this.label7.TabIndex = 60;
            this.label7.Text = "Category:";
            // 
            // VendorBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(884, 719);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "VendorBill";
            this.Text = "VendorBill";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VendorBill_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGProductsVendor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView DGProductsVendor;
        private System.Windows.Forms.Button btwAddVendor;
        private System.Windows.Forms.TextBox txtWholeSalePriceVendor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton radioButtonFemaleVendor;
        private System.Windows.Forms.RadioButton radioButtonMaleVendor;
        private System.Windows.Forms.TextBox txtRetailPriceVendor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtQuantityVendor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboxCategoryVendor;
        private System.Windows.Forms.TextBox txtColorVendor;
        private System.Windows.Forms.ComboBox cboxSizeVendor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtModelNoVendor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIProductNameVendor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVendorNameVendorBill;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCredit;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblStatusVendorBill;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btwSaveVendor;
        private System.Windows.Forms.DateTimePicker txtDueDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker txtOrderDate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTotalBill;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTotProdPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Color;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn Category;
        private System.Windows.Forms.DataGridViewTextBoxColumn Gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn RetailPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn WholeSalePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn totProdPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Image;
        private System.Windows.Forms.Button btwEdit;
        private System.Windows.Forms.Button txtCancel;
    }
}