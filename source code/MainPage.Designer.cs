﻿namespace VisualProjectOpInventory
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckVendorsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clickStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsStatusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.goToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btwExitMain = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btwBillMain = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btwSalesMain = new System.Windows.Forms.Button();
            this.btwCustomerMain = new System.Windows.Forms.Button();
            this.btwVendorsMain = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.MainmenuStrip1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.MainmenuStrip1);
            this.panel1.Location = new System.Drawing.Point(1, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(902, 109);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 22.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(371, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 46);
            this.label1.TabIndex = 8;
            this.label1.Text = "OpticalEye";
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Segoe WP", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.helpToolStripMenuItem,
            this.goToToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Size = new System.Drawing.Size(902, 31);
            this.MainmenuStrip1.TabIndex = 0;
            this.MainmenuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsTypeToolStripMenuItem,
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1,
            this.vendorsStatusToolStripMenuItem,
            this.paymentsStatusToolStripMenuItem});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(86, 27);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsTypeToolStripMenuItem
            // 
            this.vendorsTypeToolStripMenuItem.Name = "vendorsTypeToolStripMenuItem";
            this.vendorsTypeToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsTypeToolStripMenuItem.Text = "VendorsPage";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.ordersToolStripMenuItem.Text = "VendorsType";
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(277, 28);
            this.vendorsNameToolStripMenuItem1.Text = "VendorsName";
            // 
            // vendorsStatusToolStripMenuItem
            // 
            this.vendorsStatusToolStripMenuItem.Name = "vendorsStatusToolStripMenuItem";
            this.vendorsStatusToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsStatusToolStripMenuItem.Text = "VendorsStatus";
            // 
            // paymentsStatusToolStripMenuItem
            // 
            this.paymentsStatusToolStripMenuItem.Name = "paymentsStatusToolStripMenuItem";
            this.paymentsStatusToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.paymentsStatusToolStripMenuItem.Text = "VendorsPaymentsStatus";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem2,
            this.ordersToolStripMenuItem1,
            this.customerNameToolStripMenuItem,
            this.customerAddToolStripMenuItem,
            this.customerRemoveToolStripMenuItem,
            this.customerUpdateToolStripMenuItem,
            this.customerStatusToolStripMenuItem,
            this.customerPaymentStatusToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(99, 27);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // customerToolStripMenuItem2
            // 
            this.customerToolStripMenuItem2.Name = "customerToolStripMenuItem2";
            this.customerToolStripMenuItem2.Size = new System.Drawing.Size(283, 28);
            this.customerToolStripMenuItem2.Text = "CustomerPage";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(283, 28);
            this.ordersToolStripMenuItem1.Text = "Orders";
            // 
            // customerNameToolStripMenuItem
            // 
            this.customerNameToolStripMenuItem.Name = "customerNameToolStripMenuItem";
            this.customerNameToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerNameToolStripMenuItem.Text = "CustomerName";
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerAddToolStripMenuItem.Text = "CustomerAdd";
            // 
            // customerRemoveToolStripMenuItem
            // 
            this.customerRemoveToolStripMenuItem.Name = "customerRemoveToolStripMenuItem";
            this.customerRemoveToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerRemoveToolStripMenuItem.Text = "CustomerRemove";
            // 
            // customerUpdateToolStripMenuItem
            // 
            this.customerUpdateToolStripMenuItem.Name = "customerUpdateToolStripMenuItem";
            this.customerUpdateToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerUpdateToolStripMenuItem.Text = "CustomerUpdate";
            // 
            // customerStatusToolStripMenuItem
            // 
            this.customerStatusToolStripMenuItem.Name = "customerStatusToolStripMenuItem";
            this.customerStatusToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerStatusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // customerPaymentStatusToolStripMenuItem
            // 
            this.customerPaymentStatusToolStripMenuItem.Name = "customerPaymentStatusToolStripMenuItem";
            this.customerPaymentStatusToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerPaymentStatusToolStripMenuItem.Text = "CustomerPaymentStatus";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toAddCustomerToolStripMenuItem,
            this.toCheckCustomerStatusToolStripMenuItem,
            this.toAddVendorsToolStripMenuItem,
            this.toCheckVendorsStatusToolStripMenuItem,
            this.paymentsToolStripMenuItem,
            this.ordersToolStripMenuItem2});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(60, 27);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // toAddCustomerToolStripMenuItem
            // 
            this.toAddCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem3});
            this.toAddCustomerToolStripMenuItem.Name = "toAddCustomerToolStripMenuItem";
            this.toAddCustomerToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toAddCustomerToolStripMenuItem.Text = "ToAddCustomer";
            // 
            // customerToolStripMenuItem3
            // 
            this.customerToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
            this.customerToolStripMenuItem3.Name = "customerToolStripMenuItem3";
            this.customerToolStripMenuItem3.Size = new System.Drawing.Size(202, 28);
            this.customerToolStripMenuItem3.Text = "ClickCustomer";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(236, 28);
            this.addToolStripMenuItem.Text = "ClickCustomerAdd";
            // 
            // toCheckCustomerStatusToolStripMenuItem
            // 
            this.toCheckCustomerStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerToolStripMenuItem});
            this.toCheckCustomerStatusToolStripMenuItem.Name = "toCheckCustomerStatusToolStripMenuItem";
            this.toCheckCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toCheckCustomerStatusToolStripMenuItem.Text = "ToCheckCustomerStatus";
            // 
            // clickCustomerToolStripMenuItem
            // 
            this.clickCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerStatusToolStripMenuItem});
            this.clickCustomerToolStripMenuItem.Name = "clickCustomerToolStripMenuItem";
            this.clickCustomerToolStripMenuItem.Size = new System.Drawing.Size(202, 28);
            this.clickCustomerToolStripMenuItem.Text = "ClickCustomer";
            // 
            // clickCustomerStatusToolStripMenuItem
            // 
            this.clickCustomerStatusToolStripMenuItem.Name = "clickCustomerStatusToolStripMenuItem";
            this.clickCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 28);
            this.clickCustomerStatusToolStripMenuItem.Text = "ClickCustomerStatus";
            // 
            // toAddVendorsToolStripMenuItem
            // 
            this.toAddVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem});
            this.toAddVendorsToolStripMenuItem.Name = "toAddVendorsToolStripMenuItem";
            this.toAddVendorsToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toAddVendorsToolStripMenuItem.Text = "ToAddVendors";
            // 
            // clickVendorsToolStripMenuItem
            // 
            this.clickVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorAddToolStripMenuItem});
            this.clickVendorsToolStripMenuItem.Name = "clickVendorsToolStripMenuItem";
            this.clickVendorsToolStripMenuItem.Size = new System.Drawing.Size(189, 28);
            this.clickVendorsToolStripMenuItem.Text = "ClickVendors";
            // 
            // clickVendorAddToolStripMenuItem
            // 
            this.clickVendorAddToolStripMenuItem.Name = "clickVendorAddToolStripMenuItem";
            this.clickVendorAddToolStripMenuItem.Size = new System.Drawing.Size(216, 28);
            this.clickVendorAddToolStripMenuItem.Text = "ClickVendorAdd";
            // 
            // toCheckVendorsStatusToolStripMenuItem
            // 
            this.toCheckVendorsStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem1});
            this.toCheckVendorsStatusToolStripMenuItem.Name = "toCheckVendorsStatusToolStripMenuItem";
            this.toCheckVendorsStatusToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toCheckVendorsStatusToolStripMenuItem.Text = "ToCheckVendorsStatus";
            // 
            // clickVendorsToolStripMenuItem1
            // 
            this.clickVendorsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickStatusToolStripMenuItem});
            this.clickVendorsToolStripMenuItem1.Name = "clickVendorsToolStripMenuItem1";
            this.clickVendorsToolStripMenuItem1.Size = new System.Drawing.Size(189, 28);
            this.clickVendorsToolStripMenuItem1.Text = "ClickVendors";
            // 
            // clickStatusToolStripMenuItem
            // 
            this.clickStatusToolStripMenuItem.Name = "clickStatusToolStripMenuItem";
            this.clickStatusToolStripMenuItem.Size = new System.Drawing.Size(175, 28);
            this.clickStatusToolStripMenuItem.Text = "ClickStatus";
            // 
            // paymentsToolStripMenuItem
            // 
            this.paymentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem4,
            this.vendorsToolStripMenuItem});
            this.paymentsToolStripMenuItem.Name = "paymentsToolStripMenuItem";
            this.paymentsToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.paymentsToolStripMenuItem.Text = "Payments";
            // 
            // customerToolStripMenuItem4
            // 
            this.customerToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripMenuItem,
            this.vendorsStatusToolStripMenuItem1});
            this.customerToolStripMenuItem4.Name = "customerToolStripMenuItem4";
            this.customerToolStripMenuItem4.Size = new System.Drawing.Size(163, 28);
            this.customerToolStripMenuItem4.Text = "Customer";
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(213, 28);
            this.statusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // vendorsStatusToolStripMenuItem1
            // 
            this.vendorsStatusToolStripMenuItem1.Name = "vendorsStatusToolStripMenuItem1";
            this.vendorsStatusToolStripMenuItem1.Size = new System.Drawing.Size(213, 28);
            this.vendorsStatusToolStripMenuItem1.Text = "VendorsStatus";
            // 
            // vendorsToolStripMenuItem
            // 
            this.vendorsToolStripMenuItem.Name = "vendorsToolStripMenuItem";
            this.vendorsToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.vendorsToolStripMenuItem.Text = "Vendors";
            // 
            // ordersToolStripMenuItem2
            // 
            this.ordersToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem5,
            this.vendorsToolStripMenuItem1});
            this.ordersToolStripMenuItem2.Name = "ordersToolStripMenuItem2";
            this.ordersToolStripMenuItem2.Size = new System.Drawing.Size(279, 28);
            this.ordersToolStripMenuItem2.Text = "Orders";
            // 
            // customerToolStripMenuItem5
            // 
            this.customerToolStripMenuItem5.Name = "customerToolStripMenuItem5";
            this.customerToolStripMenuItem5.Size = new System.Drawing.Size(163, 28);
            this.customerToolStripMenuItem5.Text = "Customer";
            // 
            // vendorsToolStripMenuItem1
            // 
            this.vendorsToolStripMenuItem1.Name = "vendorsToolStripMenuItem1";
            this.vendorsToolStripMenuItem1.Size = new System.Drawing.Size(163, 28);
            this.vendorsToolStripMenuItem1.Text = "Vendors";
            // 
            // goToToolStripMenuItem
            // 
            this.goToToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem6,
            this.vendorsToolStripMenuItem2,
            this.salesToolStripMenuItem1,
            this.ordersToolStripMenuItem3,
            this.paymentsToolStripMenuItem1,
            this.statusToolStripMenuItem1,
            this.loginToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.goToToolStripMenuItem.Name = "goToToolStripMenuItem";
            this.goToToolStripMenuItem.Size = new System.Drawing.Size(62, 27);
            this.goToToolStripMenuItem.Text = "GoTo";
            // 
            // customerToolStripMenuItem6
            // 
            this.customerToolStripMenuItem6.Name = "customerToolStripMenuItem6";
            this.customerToolStripMenuItem6.Size = new System.Drawing.Size(181, 28);
            this.customerToolStripMenuItem6.Text = "Customer";
            // 
            // vendorsToolStripMenuItem2
            // 
            this.vendorsToolStripMenuItem2.Name = "vendorsToolStripMenuItem2";
            this.vendorsToolStripMenuItem2.Size = new System.Drawing.Size(181, 28);
            this.vendorsToolStripMenuItem2.Text = "Vendors";
            // 
            // salesToolStripMenuItem1
            // 
            this.salesToolStripMenuItem1.Name = "salesToolStripMenuItem1";
            this.salesToolStripMenuItem1.Size = new System.Drawing.Size(181, 28);
            this.salesToolStripMenuItem1.Text = "Sales";
            // 
            // ordersToolStripMenuItem3
            // 
            this.ordersToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerOrdersToolStripMenuItem,
            this.vendorsOrdersToolStripMenuItem});
            this.ordersToolStripMenuItem3.Name = "ordersToolStripMenuItem3";
            this.ordersToolStripMenuItem3.Size = new System.Drawing.Size(181, 28);
            this.ordersToolStripMenuItem3.Text = "Orders";
            // 
            // customerOrdersToolStripMenuItem
            // 
            this.customerOrdersToolStripMenuItem.Name = "customerOrdersToolStripMenuItem";
            this.customerOrdersToolStripMenuItem.Size = new System.Drawing.Size(217, 28);
            this.customerOrdersToolStripMenuItem.Text = "CustomerOrders";
            // 
            // vendorsOrdersToolStripMenuItem
            // 
            this.vendorsOrdersToolStripMenuItem.Name = "vendorsOrdersToolStripMenuItem";
            this.vendorsOrdersToolStripMenuItem.Size = new System.Drawing.Size(217, 28);
            this.vendorsOrdersToolStripMenuItem.Text = "VendorsOrders";
            // 
            // paymentsToolStripMenuItem1
            // 
            this.paymentsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem7,
            this.vendorsToolStripMenuItem3});
            this.paymentsToolStripMenuItem1.Name = "paymentsToolStripMenuItem1";
            this.paymentsToolStripMenuItem1.Size = new System.Drawing.Size(181, 28);
            this.paymentsToolStripMenuItem1.Text = "Payments";
            // 
            // customerToolStripMenuItem7
            // 
            this.customerToolStripMenuItem7.Name = "customerToolStripMenuItem7";
            this.customerToolStripMenuItem7.Size = new System.Drawing.Size(170, 28);
            this.customerToolStripMenuItem7.Text = "Customers";
            // 
            // vendorsToolStripMenuItem3
            // 
            this.vendorsToolStripMenuItem3.Name = "vendorsToolStripMenuItem3";
            this.vendorsToolStripMenuItem3.Size = new System.Drawing.Size(170, 28);
            this.vendorsToolStripMenuItem3.Text = "Vendors";
            // 
            // statusToolStripMenuItem1
            // 
            this.statusToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem8,
            this.vendorsToolStripMenuItem4});
            this.statusToolStripMenuItem1.Name = "statusToolStripMenuItem1";
            this.statusToolStripMenuItem1.Size = new System.Drawing.Size(181, 28);
            this.statusToolStripMenuItem1.Text = "Status";
            // 
            // customerToolStripMenuItem8
            // 
            this.customerToolStripMenuItem8.Name = "customerToolStripMenuItem8";
            this.customerToolStripMenuItem8.Size = new System.Drawing.Size(170, 28);
            this.customerToolStripMenuItem8.Text = "Customers";
            // 
            // vendorsToolStripMenuItem4
            // 
            this.vendorsToolStripMenuItem4.Name = "vendorsToolStripMenuItem4";
            this.vendorsToolStripMenuItem4.Size = new System.Drawing.Size(170, 28);
            this.vendorsToolStripMenuItem4.Text = "Vendors";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(181, 28);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(181, 28);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel3.Location = new System.Drawing.Point(12, 376);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(695, 100);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel4.AutoSize = true;
            this.panel4.Controls.Add(this.btwExitMain);
            this.panel4.Location = new System.Drawing.Point(713, 376);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(180, 100);
            this.panel4.TabIndex = 1;
            // 
            // btwExitMain
            // 
            this.btwExitMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwExitMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwExitMain.Location = new System.Drawing.Point(53, 32);
            this.btwExitMain.Name = "btwExitMain";
            this.btwExitMain.Size = new System.Drawing.Size(75, 40);
            this.btwExitMain.TabIndex = 2;
            this.btwExitMain.Text = "Exit";
            this.btwExitMain.UseVisualStyleBackColor = true;
            this.btwExitMain.Click += new System.EventHandler(this.btwExit_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel5.Controls.Add(this.btwBillMain);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.btwSalesMain);
            this.panel5.Controls.Add(this.btwCustomerMain);
            this.panel5.Controls.Add(this.btwVendorsMain);
            this.panel5.Location = new System.Drawing.Point(12, 118);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(881, 252);
            this.panel5.TabIndex = 0;
            // 
            // btwBillMain
            // 
            this.btwBillMain.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBillMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBillMain.Location = new System.Drawing.Point(681, 132);
            this.btwBillMain.Name = "btwBillMain";
            this.btwBillMain.Size = new System.Drawing.Size(175, 102);
            this.btwBillMain.TabIndex = 7;
            this.btwBillMain.Text = "Bill";
            this.btwBillMain.UseVisualStyleBackColor = true;
            this.btwBillMain.Click += new System.EventHandler(this.btwBillMain_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.LightGray;
            this.panel6.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel6.Location = new System.Drawing.Point(646, 132);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(29, 100);
            this.panel6.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightGray;
            this.panel2.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel2.Location = new System.Drawing.Point(430, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(29, 100);
            this.panel2.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.LightGray;
            this.panel7.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel7.Location = new System.Drawing.Point(214, 130);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(29, 100);
            this.panel7.TabIndex = 4;
            // 
            // btwSalesMain
            // 
            this.btwSalesMain.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSalesMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwSalesMain.Location = new System.Drawing.Point(465, 130);
            this.btwSalesMain.Name = "btwSalesMain";
            this.btwSalesMain.Size = new System.Drawing.Size(175, 102);
            this.btwSalesMain.TabIndex = 2;
            this.btwSalesMain.Text = "Sales";
            this.btwSalesMain.UseVisualStyleBackColor = true;
            this.btwSalesMain.Click += new System.EventHandler(this.btwSalesMain_Click);
            // 
            // btwCustomerMain
            // 
            this.btwCustomerMain.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwCustomerMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwCustomerMain.Location = new System.Drawing.Point(249, 130);
            this.btwCustomerMain.Name = "btwCustomerMain";
            this.btwCustomerMain.Size = new System.Drawing.Size(175, 102);
            this.btwCustomerMain.TabIndex = 1;
            this.btwCustomerMain.Text = "Customers";
            this.btwCustomerMain.UseVisualStyleBackColor = true;
            this.btwCustomerMain.Click += new System.EventHandler(this.btwCustomer_Click);
            // 
            // btwVendorsMain
            // 
            this.btwVendorsMain.Font = new System.Drawing.Font("Mistral", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwVendorsMain.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwVendorsMain.Location = new System.Drawing.Point(33, 128);
            this.btwVendorsMain.Name = "btwVendorsMain";
            this.btwVendorsMain.Size = new System.Drawing.Size(175, 102);
            this.btwVendorsMain.TabIndex = 0;
            this.btwVendorsMain.Text = "Vendors";
            this.btwVendorsMain.UseVisualStyleBackColor = true;
            this.btwVendorsMain.Click += new System.EventHandler(this.btwVendors_Click);
            // 
            // panel8
            // 
            this.panel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel8.BackgroundImage = global::VisualProjectOpInventory.Properties.Resources.kghostview;
            this.panel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Location = new System.Drawing.Point(348, 118);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(210, 112);
            this.panel8.TabIndex = 8;
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(905, 488);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainmenuStrip1;
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btwSalesMain;
        private System.Windows.Forms.Button btwCustomerMain;
        private System.Windows.Forms.Button btwVendorsMain;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem goToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckVendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clickStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem customerOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btwExitMain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btwBillMain;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
    }
}

