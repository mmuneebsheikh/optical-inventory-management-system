﻿namespace VisualProjectOpInventory
{
    partial class CustomerNewBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btwRemoveItem = new System.Windows.Forms.Button();
            this.txtItemDesc = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btwGenerateBill = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.cmbPres = new System.Windows.Forms.ComboBox();
            this.btwAddItem = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbItems = new System.Windows.Forms.ComboBox();
            this.DGCustProds = new System.Windows.Forms.DataGridView();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PresName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btwBackBill = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btwDonePres = new System.Windows.Forms.Button();
            this.btwAddPrescription = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlPrescription = new System.Windows.Forms.Panel();
            this.txtPresName = new System.Windows.Forms.TextBox();
            this.btwClosePres = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtL_D_SPH = new System.Windows.Forms.TextBox();
            this.txtL_D_CYL = new System.Windows.Forms.TextBox();
            this.txtL_D_AXIS = new System.Windows.Forms.TextBox();
            this.txtL_N_SPH = new System.Windows.Forms.TextBox();
            this.txtL_N_CYL = new System.Windows.Forms.TextBox();
            this.txtL_N_AXIS = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtR_N_SPH = new System.Windows.Forms.TextBox();
            this.txtR_N_CYL = new System.Windows.Forms.TextBox();
            this.txtR_N_AXIS = new System.Windows.Forms.TextBox();
            this.txtR_D_AXIS = new System.Windows.Forms.TextBox();
            this.txtR_D_CYL = new System.Windows.Forms.TextBox();
            this.txtR_D_SPH = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPhoneNo = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDiscountBill = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBalanceBill = new System.Windows.Forms.TextBox();
            this.txtAdvanceBill = new System.Windows.Forms.TextBox();
            this.txtNetTotalBill = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeliveryDateBill = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSerialNoBill = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAddressBill = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNameAddNewCustomer = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGCustProds)).BeginInit();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlPrescription.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(11, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1279, 692);
            this.panel1.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.listBox1);
            this.panel5.Controls.Add(this.btwRemoveItem);
            this.panel5.Controls.Add(this.txtItemDesc);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.btwGenerateBill);
            this.panel5.Controls.Add(this.label27);
            this.panel5.Controls.Add(this.cmbPres);
            this.panel5.Controls.Add(this.btwAddItem);
            this.panel5.Controls.Add(this.label26);
            this.panel5.Controls.Add(this.cmbItems);
            this.panel5.Controls.Add(this.DGCustProds);
            this.panel5.Controls.Add(this.btwBackBill);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Location = new System.Drawing.Point(796, 10);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(470, 666);
            this.panel5.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(22, 193);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 13);
            this.label29.TabIndex = 30;
            this.label29.Text = "label29";
            this.label29.Visible = false;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(183, 265);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 29;
            this.listBox1.Visible = false;
            // 
            // btwRemoveItem
            // 
            this.btwRemoveItem.Location = new System.Drawing.Point(392, 325);
            this.btwRemoveItem.Name = "btwRemoveItem";
            this.btwRemoveItem.Size = new System.Drawing.Size(75, 23);
            this.btwRemoveItem.TabIndex = 28;
            this.btwRemoveItem.Text = "Remove";
            this.btwRemoveItem.UseVisualStyleBackColor = true;
            this.btwRemoveItem.Click += new System.EventHandler(this.btwRemoveItem_Click);
            // 
            // txtItemDesc
            // 
            this.txtItemDesc.Location = new System.Drawing.Point(183, 157);
            this.txtItemDesc.Multiline = true;
            this.txtItemDesc.Name = "txtItemDesc";
            this.txtItemDesc.Size = new System.Drawing.Size(180, 93);
            this.txtItemDesc.TabIndex = 27;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label28.Location = new System.Drawing.Point(18, 155);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 20);
            this.label28.TabIndex = 26;
            this.label28.Text = "Description";
            // 
            // btwGenerateBill
            // 
            this.btwGenerateBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwGenerateBill.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btwGenerateBill.Location = new System.Drawing.Point(346, 611);
            this.btwGenerateBill.Margin = new System.Windows.Forms.Padding(2);
            this.btwGenerateBill.Name = "btwGenerateBill";
            this.btwGenerateBill.Size = new System.Drawing.Size(108, 53);
            this.btwGenerateBill.TabIndex = 25;
            this.btwGenerateBill.Text = "Generate Bill";
            this.btwGenerateBill.UseVisualStyleBackColor = true;
            this.btwGenerateBill.Click += new System.EventHandler(this.btwGenerateBill_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label27.Location = new System.Drawing.Point(18, 109);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(160, 20);
            this.label27.TabIndex = 24;
            this.label27.Text = "Select Prescription";
            // 
            // cmbPres
            // 
            this.cmbPres.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbPres.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbPres.FormattingEnabled = true;
            this.cmbPres.Location = new System.Drawing.Point(183, 111);
            this.cmbPres.Name = "cmbPres";
            this.cmbPres.Size = new System.Drawing.Size(180, 21);
            this.cmbPres.TabIndex = 23;
            // 
            // btwAddItem
            // 
            this.btwAddItem.Location = new System.Drawing.Point(392, 193);
            this.btwAddItem.Name = "btwAddItem";
            this.btwAddItem.Size = new System.Drawing.Size(75, 23);
            this.btwAddItem.TabIndex = 22;
            this.btwAddItem.Text = "Add";
            this.btwAddItem.UseVisualStyleBackColor = true;
            this.btwAddItem.Click += new System.EventHandler(this.btwAddItem_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label26.Location = new System.Drawing.Point(18, 85);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(101, 20);
            this.label26.TabIndex = 21;
            this.label26.Text = "Select Item";
            // 
            // cmbItems
            // 
            this.cmbItems.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbItems.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbItems.FormattingEnabled = true;
            this.cmbItems.Items.AddRange(new object[] {
            "hello",
            "hell",
            "prod",
            "Ferrari",
            "porshe"});
            this.cmbItems.Location = new System.Drawing.Point(183, 84);
            this.cmbItems.Name = "cmbItems";
            this.cmbItems.Size = new System.Drawing.Size(231, 21);
            this.cmbItems.TabIndex = 15;
            // 
            // DGCustProds
            // 
            this.DGCustProds.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGCustProds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGCustProds.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemName,
            this.ModelNo,
            this.PresName,
            this.ItemDesc});
            this.DGCustProds.Location = new System.Drawing.Point(3, 372);
            this.DGCustProds.Name = "DGCustProds";
            this.DGCustProds.ReadOnly = true;
            this.DGCustProds.Size = new System.Drawing.Size(464, 217);
            this.DGCustProds.TabIndex = 14;
            // 
            // ItemName
            // 
            this.ItemName.HeaderText = "ItemName";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ModelNo
            // 
            this.ModelNo.HeaderText = "Model No";
            this.ModelNo.Name = "ModelNo";
            this.ModelNo.ReadOnly = true;
            // 
            // PresName
            // 
            this.PresName.HeaderText = "Prescription Name";
            this.PresName.Name = "PresName";
            this.PresName.ReadOnly = true;
            // 
            // ItemDesc
            // 
            this.ItemDesc.HeaderText = "Description";
            this.ItemDesc.Name = "ItemDesc";
            this.ItemDesc.ReadOnly = true;
            // 
            // btwBackBill
            // 
            this.btwBackBill.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBackBill.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btwBackBill.Location = new System.Drawing.Point(234, 611);
            this.btwBackBill.Margin = new System.Windows.Forms.Padding(2);
            this.btwBackBill.Name = "btwBackBill";
            this.btwBackBill.Size = new System.Drawing.Size(108, 53);
            this.btwBackBill.TabIndex = 1;
            this.btwBackBill.Text = "Back";
            this.btwBackBill.UseVisualStyleBackColor = true;
            this.btwBackBill.Click += new System.EventHandler(this.btwBackBill_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label13.Location = new System.Drawing.Point(98, 7);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(155, 31);
            this.label13.TabIndex = 13;
            this.label13.Text = "Item Detail";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btwDonePres);
            this.panel4.Controls.Add(this.btwAddPrescription);
            this.panel4.Controls.Add(this.flowLayoutPanel1);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Location = new System.Drawing.Point(7, 313);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 364);
            this.panel4.TabIndex = 1;
            // 
            // btwDonePres
            // 
            this.btwDonePres.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btwDonePres.Location = new System.Drawing.Point(584, 308);
            this.btwDonePres.Name = "btwDonePres";
            this.btwDonePres.Size = new System.Drawing.Size(117, 41);
            this.btwDonePres.TabIndex = 31;
            this.btwDonePres.Text = "Done";
            this.btwDonePres.UseVisualStyleBackColor = true;
            this.btwDonePres.Click += new System.EventHandler(this.btwDonePres_Click);
            // 
            // btwAddPrescription
            // 
            this.btwAddPrescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btwAddPrescription.Location = new System.Drawing.Point(584, 22);
            this.btwAddPrescription.Name = "btwAddPrescription";
            this.btwAddPrescription.Size = new System.Drawing.Size(117, 41);
            this.btwAddPrescription.TabIndex = 30;
            this.btwAddPrescription.Text = "Add Prescription";
            this.btwAddPrescription.UseVisualStyleBackColor = true;
            this.btwAddPrescription.Click += new System.EventHandler(this.btwAddPrescription_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flowLayoutPanel1.Controls.Add(this.pnlPrescription);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(35, 69);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(643, 203);
            this.flowLayoutPanel1.TabIndex = 29;
            // 
            // pnlPrescription
            // 
            this.pnlPrescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlPrescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.pnlPrescription.Controls.Add(this.txtPresName);
            this.pnlPrescription.Controls.Add(this.btwClosePres);
            this.pnlPrescription.Controls.Add(this.label15);
            this.pnlPrescription.Controls.Add(this.txtL_D_SPH);
            this.pnlPrescription.Controls.Add(this.txtL_D_CYL);
            this.pnlPrescription.Controls.Add(this.txtL_D_AXIS);
            this.pnlPrescription.Controls.Add(this.txtL_N_SPH);
            this.pnlPrescription.Controls.Add(this.txtL_N_CYL);
            this.pnlPrescription.Controls.Add(this.txtL_N_AXIS);
            this.pnlPrescription.Controls.Add(this.label16);
            this.pnlPrescription.Controls.Add(this.label17);
            this.pnlPrescription.Controls.Add(this.label18);
            this.pnlPrescription.Controls.Add(this.txtR_N_SPH);
            this.pnlPrescription.Controls.Add(this.txtR_N_CYL);
            this.pnlPrescription.Controls.Add(this.txtR_N_AXIS);
            this.pnlPrescription.Controls.Add(this.txtR_D_AXIS);
            this.pnlPrescription.Controls.Add(this.txtR_D_CYL);
            this.pnlPrescription.Controls.Add(this.txtR_D_SPH);
            this.pnlPrescription.Controls.Add(this.label19);
            this.pnlPrescription.Controls.Add(this.label20);
            this.pnlPrescription.Controls.Add(this.label21);
            this.pnlPrescription.Controls.Add(this.label22);
            this.pnlPrescription.Controls.Add(this.label23);
            this.pnlPrescription.Controls.Add(this.label24);
            this.pnlPrescription.Controls.Add(this.label25);
            this.pnlPrescription.Location = new System.Drawing.Point(3, 3);
            this.pnlPrescription.Name = "pnlPrescription";
            this.pnlPrescription.Size = new System.Drawing.Size(636, 196);
            this.pnlPrescription.TabIndex = 0;
            // 
            // txtPresName
            // 
            this.txtPresName.Location = new System.Drawing.Point(82, 10);
            this.txtPresName.Name = "txtPresName";
            this.txtPresName.Size = new System.Drawing.Size(100, 20);
            this.txtPresName.TabIndex = 23;
            // 
            // btwClosePres
            // 
            this.btwClosePres.Location = new System.Drawing.Point(546, 7);
            this.btwClosePres.Name = "btwClosePres";
            this.btwClosePres.Size = new System.Drawing.Size(75, 23);
            this.btwClosePres.TabIndex = 1;
            this.btwClosePres.Text = "X";
            this.btwClosePres.UseVisualStyleBackColor = true;
            this.btwClosePres.Click += new System.EventHandler(this.btwClosePres_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(31, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Name";
            // 
            // txtL_D_SPH
            // 
            this.txtL_D_SPH.Location = new System.Drawing.Point(360, 121);
            this.txtL_D_SPH.Name = "txtL_D_SPH";
            this.txtL_D_SPH.Size = new System.Drawing.Size(78, 20);
            this.txtL_D_SPH.TabIndex = 12;
            // 
            // txtL_D_CYL
            // 
            this.txtL_D_CYL.Location = new System.Drawing.Point(444, 121);
            this.txtL_D_CYL.Name = "txtL_D_CYL";
            this.txtL_D_CYL.Size = new System.Drawing.Size(78, 20);
            this.txtL_D_CYL.TabIndex = 11;
            // 
            // txtL_D_AXIS
            // 
            this.txtL_D_AXIS.Location = new System.Drawing.Point(528, 121);
            this.txtL_D_AXIS.Name = "txtL_D_AXIS";
            this.txtL_D_AXIS.Size = new System.Drawing.Size(78, 20);
            this.txtL_D_AXIS.TabIndex = 10;
            // 
            // txtL_N_SPH
            // 
            this.txtL_N_SPH.Location = new System.Drawing.Point(360, 157);
            this.txtL_N_SPH.Name = "txtL_N_SPH";
            this.txtL_N_SPH.Size = new System.Drawing.Size(78, 20);
            this.txtL_N_SPH.TabIndex = 9;
            // 
            // txtL_N_CYL
            // 
            this.txtL_N_CYL.Location = new System.Drawing.Point(444, 157);
            this.txtL_N_CYL.Name = "txtL_N_CYL";
            this.txtL_N_CYL.Size = new System.Drawing.Size(78, 20);
            this.txtL_N_CYL.TabIndex = 8;
            // 
            // txtL_N_AXIS
            // 
            this.txtL_N_AXIS.Location = new System.Drawing.Point(528, 157);
            this.txtL_N_AXIS.Name = "txtL_N_AXIS";
            this.txtL_N_AXIS.Size = new System.Drawing.Size(78, 20);
            this.txtL_N_AXIS.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(525, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "AXIS";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(441, 89);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "CYL";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(357, 89);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "SPH";
            // 
            // txtR_N_SPH
            // 
            this.txtR_N_SPH.Location = new System.Drawing.Point(63, 157);
            this.txtR_N_SPH.Name = "txtR_N_SPH";
            this.txtR_N_SPH.Size = new System.Drawing.Size(78, 20);
            this.txtR_N_SPH.TabIndex = 18;
            // 
            // txtR_N_CYL
            // 
            this.txtR_N_CYL.Location = new System.Drawing.Point(147, 157);
            this.txtR_N_CYL.Name = "txtR_N_CYL";
            this.txtR_N_CYL.Size = new System.Drawing.Size(78, 20);
            this.txtR_N_CYL.TabIndex = 17;
            // 
            // txtR_N_AXIS
            // 
            this.txtR_N_AXIS.Location = new System.Drawing.Point(231, 157);
            this.txtR_N_AXIS.Name = "txtR_N_AXIS";
            this.txtR_N_AXIS.Size = new System.Drawing.Size(78, 20);
            this.txtR_N_AXIS.TabIndex = 16;
            // 
            // txtR_D_AXIS
            // 
            this.txtR_D_AXIS.Location = new System.Drawing.Point(231, 121);
            this.txtR_D_AXIS.Name = "txtR_D_AXIS";
            this.txtR_D_AXIS.Size = new System.Drawing.Size(78, 20);
            this.txtR_D_AXIS.TabIndex = 19;
            // 
            // txtR_D_CYL
            // 
            this.txtR_D_CYL.Location = new System.Drawing.Point(147, 121);
            this.txtR_D_CYL.Name = "txtR_D_CYL";
            this.txtR_D_CYL.Size = new System.Drawing.Size(78, 20);
            this.txtR_D_CYL.TabIndex = 20;
            // 
            // txtR_D_SPH
            // 
            this.txtR_D_SPH.Location = new System.Drawing.Point(63, 121);
            this.txtR_D_SPH.Name = "txtR_D_SPH";
            this.txtR_D_SPH.Size = new System.Drawing.Size(78, 20);
            this.txtR_D_SPH.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 160);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "N";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 124);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "D";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(228, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "AXIS";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(144, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(27, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "CYL";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(60, 89);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(29, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "SPH";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(435, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(33, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "LEFT";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(110, 65);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "RIGHT";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label12.Location = new System.Drawing.Point(294, 22);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 37);
            this.label12.TabIndex = 0;
            this.label12.Text = "Order Details";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblStatus);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.txtPhoneNo);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.txtDeliveryDateBill);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtSerialNoBill);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtAddressBill);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtNameAddNewCustomer);
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(9, 10);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(783, 297);
            this.panel2.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblStatus.Location = new System.Drawing.Point(531, 253);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 20);
            this.lblStatus.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label30.Location = new System.Drawing.Point(435, 253);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(62, 20);
            this.label30.TabIndex = 23;
            this.label30.Text = "Status";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel6.Controls.Add(this.label14);
            this.panel6.Location = new System.Drawing.Point(352, 67);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(70, 210);
            this.panel6.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label14.Location = new System.Drawing.Point(20, 11);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 182);
            this.label14.TabIndex = 23;
            this.label14.Text = "O\r\nP\r\nT\r\nI\r\nC\r\nA\r\nN\r\n";
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.txtPhoneNo.Location = new System.Drawing.Point(117, 136);
            this.txtPhoneNo.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.ReadOnly = true;
            this.txtPhoneNo.Size = new System.Drawing.Size(210, 24);
            this.txtPhoneNo.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDiscountBill);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtBalanceBill);
            this.groupBox1.Controls.Add(this.txtAdvanceBill);
            this.groupBox1.Controls.Add(this.txtNetTotalBill);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(427, 67);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(354, 173);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Amount:";
            // 
            // txtDiscountBill
            // 
            this.txtDiscountBill.Location = new System.Drawing.Point(99, 70);
            this.txtDiscountBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiscountBill.Name = "txtDiscountBill";
            this.txtDiscountBill.Size = new System.Drawing.Size(230, 26);
            this.txtDiscountBill.TabIndex = 20;
            this.txtDiscountBill.TextChanged += new System.EventHandler(this.txtDiscountBill_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(4, 74);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 20);
            this.label11.TabIndex = 19;
            this.label11.Text = "Discount:";
            // 
            // txtBalanceBill
            // 
            this.txtBalanceBill.Location = new System.Drawing.Point(99, 135);
            this.txtBalanceBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtBalanceBill.Name = "txtBalanceBill";
            this.txtBalanceBill.ReadOnly = true;
            this.txtBalanceBill.Size = new System.Drawing.Size(230, 26);
            this.txtBalanceBill.TabIndex = 18;
            this.txtBalanceBill.TextChanged += new System.EventHandler(this.txtBalanceBill_TextChanged);
            // 
            // txtAdvanceBill
            // 
            this.txtAdvanceBill.Location = new System.Drawing.Point(99, 104);
            this.txtAdvanceBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdvanceBill.Name = "txtAdvanceBill";
            this.txtAdvanceBill.Size = new System.Drawing.Size(230, 26);
            this.txtAdvanceBill.TabIndex = 17;
            this.txtAdvanceBill.TextChanged += new System.EventHandler(this.txtAdvanceBill_TextChanged);
            // 
            // txtNetTotalBill
            // 
            this.txtNetTotalBill.Location = new System.Drawing.Point(99, 34);
            this.txtNetTotalBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtNetTotalBill.Name = "txtNetTotalBill";
            this.txtNetTotalBill.ReadOnly = true;
            this.txtNetTotalBill.Size = new System.Drawing.Size(230, 26);
            this.txtNetTotalBill.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(4, 139);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Balance:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(4, 106);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Advance:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(8, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Net Total:";
            // 
            // txtDeliveryDateBill
            // 
            this.txtDeliveryDateBill.Location = new System.Drawing.Point(146, 253);
            this.txtDeliveryDateBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtDeliveryDateBill.Name = "txtDeliveryDateBill";
            this.txtDeliveryDateBill.Size = new System.Drawing.Size(174, 26);
            this.txtDeliveryDateBill.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(9, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Serial No:";
            this.label6.Visible = false;
            // 
            // txtSerialNoBill
            // 
            this.txtSerialNoBill.Location = new System.Drawing.Point(117, 67);
            this.txtSerialNoBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtSerialNoBill.Name = "txtSerialNoBill";
            this.txtSerialNoBill.ReadOnly = true;
            this.txtSerialNoBill.Size = new System.Drawing.Size(210, 26);
            this.txtSerialNoBill.TabIndex = 10;
            this.txtSerialNoBill.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(21, 253);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Delivery Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(9, 168);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Address:";
            // 
            // txtAddressBill
            // 
            this.txtAddressBill.Location = new System.Drawing.Point(90, 165);
            this.txtAddressBill.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddressBill.Multiline = true;
            this.txtAddressBill.Name = "txtAddressBill";
            this.txtAddressBill.ReadOnly = true;
            this.txtAddressBill.Size = new System.Drawing.Size(230, 74);
            this.txtAddressBill.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(783, 50);
            this.panel3.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(482, 21);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 20);
            this.label7.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label3.Location = new System.Drawing.Point(334, 7);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 31);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bill";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(9, 136);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Contact No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(18, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Name:";
            // 
            // txtNameAddNewCustomer
            // 
            this.txtNameAddNewCustomer.Location = new System.Drawing.Point(117, 103);
            this.txtNameAddNewCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.txtNameAddNewCustomer.Name = "txtNameAddNewCustomer";
            this.txtNameAddNewCustomer.ReadOnly = true;
            this.txtNameAddNewCustomer.Size = new System.Drawing.Size(210, 26);
            this.txtNameAddNewCustomer.TabIndex = 1;
            // 
            // CustomerNewBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1301, 704);
            this.Controls.Add(this.panel1);
            this.Name = "CustomerNewBill";
            this.Text = "CustomerNewBill";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CustomerNewBill_Load);
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGCustProds)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlPrescription.ResumeLayout(false);
            this.pnlPrescription.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btwBackBill;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btwAddPrescription;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlPrescription;
        private System.Windows.Forms.TextBox txtPresName;
        private System.Windows.Forms.Button btwClosePres;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtL_D_SPH;
        private System.Windows.Forms.TextBox txtL_D_CYL;
        private System.Windows.Forms.TextBox txtL_D_AXIS;
        private System.Windows.Forms.TextBox txtL_N_SPH;
        private System.Windows.Forms.TextBox txtL_N_CYL;
        private System.Windows.Forms.TextBox txtL_N_AXIS;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtR_N_SPH;
        private System.Windows.Forms.TextBox txtR_N_CYL;
        private System.Windows.Forms.TextBox txtR_N_AXIS;
        private System.Windows.Forms.TextBox txtR_D_AXIS;
        private System.Windows.Forms.TextBox txtR_D_CYL;
        private System.Windows.Forms.TextBox txtR_D_SPH;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtPhoneNo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDiscountBill;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBalanceBill;
        private System.Windows.Forms.TextBox txtAdvanceBill;
        private System.Windows.Forms.TextBox txtNetTotalBill;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker txtDeliveryDateBill;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSerialNoBill;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAddressBill;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNameAddNewCustomer;
        private System.Windows.Forms.DataGridView DGCustProds;
        private System.Windows.Forms.Button btwAddItem;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbItems;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox cmbPres;
        private System.Windows.Forms.Button btwGenerateBill;
        private System.Windows.Forms.TextBox txtItemDesc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PresName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemDesc;
        private System.Windows.Forms.Button btwDonePres;
        private System.Windows.Forms.Button btwRemoveItem;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label30;
    }
}