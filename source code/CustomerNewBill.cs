﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class CustomerNewBill : Form
    {

        Panel[] p = new Panel[10];
        int pInd = 0;
        CustomerClass cus = new CustomerClass();
        DataAccessClass DA = new DataAccessClass();
        List<StockClass> StockList = new List<StockClass>();
            
        string userName;
        double totalAmount = 0;
        double disAmount = 0;
        public CustomerNewBill()
        {
            InitializeComponent();
        }
        public CustomerNewBill(CustomerClass cc , string userName)
        {
            InitializeComponent();
            this.cus = cc;
            this.userName = userName;
        }

        private void createPresciption()
        {
            p[pInd] = new Panel();
            p[pInd].Height = pnlPrescription.Height;
            p[pInd].Width = pnlPrescription.Width;
            p[pInd].BackColor = pnlPrescription.BackColor;
            p[pInd].Name = "newpanel" + pInd.ToString();
            flowLayoutPanel1.Controls.Add(p[pInd]);
            int txtInd = 0;
            foreach (Control c in pnlPrescription.Controls) // // // adding controls to new panel
            {
                Control c2 = new Control();
                if (c.GetType() == typeof(Label))
                {
                    c2 = new Label();
                }
                if (c.GetType() == typeof(TextBox))
                {
                    c2 = new TextBox();
                    c2.Name = "txt" + txtInd;
                    txtInd++;
                }
                if (c.GetType() == typeof(Button))
                {
                    c2 = new Button();
                    if (c.Text == "X")
                    {
                        c2.Click += new EventHandler(DeletePrescription); // // // binding delete prescription event
                    }
                    c2.Name = "btnX" + pInd.ToString();
                }
                c2.Location = c.Location;
                c2.Size = c.Size;
                if (c.GetType() != typeof(TextBox)) // // // to empty dynamic textboxes
                    c2.Text = c.Text;
                p[pInd].Controls.Add(c2);

            }
            pInd++;

        }

        void SetCmbPres()
        {
            cmbPres.Items.Clear();
            for (int i = 0; i < cus.prescriptions.Count; i++)
            {
                cmbPres.Items.Add(cus.prescriptions[i]);
            }

            cmbPres.DisplayMember = "NamePresc";
            cmbPres.ValueMember = "NamePresc";
        }


        void dynamicText() // // // retriving text from new textboxes to a label
        {

            if (!string.IsNullOrWhiteSpace(txtPresName.Text)) // Main Presc
            {
                CustomerClass.Prescription pres = new CustomerClass.Prescription();
                pres.NamePresc = txtPresName.Text;
                pres.rightEyeNo.distance.sph = Convert.ToDouble(txtR_D_SPH.Text);
                pres.rightEyeNo.distance.cyl = Convert.ToDouble(txtR_D_CYL.Text);
                pres.rightEyeNo.distance.axis = Convert.ToDouble(txtR_D_AXIS.Text);

                pres.leftEyeNo.distance.sph = Convert.ToDouble(txtL_D_SPH.Text);
                pres.rightEyeNo.distance.cyl = Convert.ToDouble(txtL_D_CYL.Text);
                pres.rightEyeNo.distance.axis = Convert.ToDouble(txtL_D_AXIS.Text);


                pres.rightEyeNo.near.sph = Convert.ToDouble(txtR_N_SPH.Text);
                pres.rightEyeNo.near.cyl = Convert.ToDouble(txtR_N_CYL.Text);
                pres.rightEyeNo.near.axis = Convert.ToDouble(txtR_N_AXIS.Text);

                pres.leftEyeNo.distance.sph = Convert.ToDouble(txtL_D_SPH.Text);
                pres.rightEyeNo.distance.cyl = Convert.ToDouble(txtL_D_CYL.Text);
                pres.rightEyeNo.distance.axis = Convert.ToDouble(txtL_D_AXIS.Text);

                pres.leftEyeNo.near.sph = Convert.ToDouble(txtL_D_SPH.Text);
                pres.rightEyeNo.near.cyl = Convert.ToDouble(txtL_D_CYL.Text);
                pres.rightEyeNo.near.axis = Convert.ToDouble(txtL_D_AXIS.Text);

                cus.prescriptions.Add(pres);
        
            }

            for (int i = 0; i < pInd; i++)
            {
                CustomerClass.Prescription pres = new CustomerClass.Prescription();
                foreach (Control c in p[i].Controls)
                {
                    if (c.GetType() == typeof(TextBox))
                    {
                        ///
                        // save the data anywhere from here
                          ///
                        if (c.Name == "txt0" )
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.NamePresc = c.Text;
                            else
                            {
                                MessageBox.Show("Please give Prescription Name");
                                c.Focus();
                                return;
                            }
                        }
                        
                        if (c.Name == "txt1")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.leftEyeNo.distance.sph = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt2")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.leftEyeNo.distance.cyl = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt3")
                        {   
                            if(!string.IsNullOrWhiteSpace(c.Text))
                                pres.leftEyeNo.distance.axis = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt4")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.leftEyeNo.near.sph = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt5")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.leftEyeNo.near.cyl = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt6")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text)) 
                                pres.leftEyeNo.near.axis = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt7")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text)) 
                                pres.rightEyeNo.near.sph = Convert.ToDouble(c.Text);                            
                        }
                        if (c.Name == "txt8")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.rightEyeNo.near.cyl = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt9")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.rightEyeNo.near.axis = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt10")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.rightEyeNo.distance.axis = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt11")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.rightEyeNo.distance.cyl = Convert.ToDouble(c.Text);
                        }
                        if (c.Name == "txt12")
                        {
                            if (!string.IsNullOrWhiteSpace(c.Text))
                                pres.rightEyeNo.distance.sph = Convert.ToDouble(c.Text);
                        }
                            //label29.Text += c.Name + ":" + c.Text + Environment.NewLine;
                    }
                }
                cus.prescriptions.Add(pres);
            }
            SetCmbPres();

        }

        void DeletePrescription(object sender, EventArgs e)
        {
            //Panel p = ((Panel)sender);
            //p.Controls.Clear();
            string btnname = ((Button)sender).Name;
            string ind = btnname.Substring(btnname.Length - 1);
            int i = Convert.ToInt32(ind);
            flowLayoutPanel1.Controls.Remove(p[i]);
            p[i].Dispose();
        }



        private void btwAddPrescription_Click(object sender, EventArgs e)
        {
            createPresciption();
        }

        private void btwBackBill_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerMainPage CM = new CustomerMainPage();
            CM.Show();
        }

        private void CustomerNewBill_Load(object sender, EventArgs e)
        {
            //string billNo = (Convert.ToInt32(DA.GetLastCustumerBillID().Rows[0].ItemArray[0]) + 1).ToString();
           // txtSerialNoBill.Text = billNo;
            Onload();
            
        }

        void Onload()
        {
            btwGenerateBill.Enabled = false;
            txtNameAddNewCustomer.Text = cus.Name;
            txtPhoneNo.Text = cus.ContactNo;
            txtAddressBill.Text = cus.Address;
            int rows = DA.AllProduct_display().Tables["product_stock"].Rows.Count;
            cmbItems.Items.Clear();
            cmbItems.Text = "Brand      ModelNo     Rs[Price]";
            StockClass SC = new StockClass();
            for (int i = 0; i < rows; i++)
            {
                SC = new StockClass();
                SC.Name = DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[0].ToString();
                SC.modelNo = DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[1].ToString();
                SC.color = DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[2].ToString();
                SC.qty = Convert.ToInt32(DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[3]);
                SC.size = Convert.ToInt32(DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[4]);
                SC.retailPrice = Convert.ToDouble(DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[5]);
                SC.category = DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[6].ToString();
                SC.gender = DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[7].ToString();
                SC.wholesalePrice = Convert.ToDouble(DA.AllProduct_display().Tables["product_stock"].Rows[i].ItemArray[8]);
                //string brand,model,color,price;
                StockList.Add(SC);

                if(StockList[i].qty > 0)
                    cmbItems.Items.Add(StockList[i].Name + "     " + StockList[i].modelNo + "     RS " + StockList[i].retailPrice+ "     QTY " + StockList[i].qty);
                //cmbItems.Items.Add();
            }


        }

        void UpdateCmbItems()
        {
            cmbItems.Items.Clear();
            for (int i = 0; i < StockList.Count; i++)
            {
                if (StockList[i].qty > 0)
                {
                    cmbItems.Items.Add(StockList[i].Name + "     " + StockList[i].modelNo + "     RS " + StockList[i].retailPrice + "     QTY " + StockList[i].qty);
                }
            }
        }

        void updateListBox()// ForChk
        {
            listBox1.Items.Clear();
            for (int i = 0; i < cus.products.Count; i++)
            {
                listBox1.Items.Add(cus.products[i]);
            }
            listBox1.DisplayMember = "ProdName";
        }
        private void btwAddItem_Click(object sender, EventArgs e)
        {
            if (cmbItems.SelectedIndex != -1)
            {
                CustomerClass.Products cProds = new CustomerClass.Products();
                int ind = cmbItems.SelectedIndex;
                if (cmbPres.SelectedIndex != -1)
                {
                    int pind = cmbPres.SelectedIndex;

                    CustomerClass.Prescription pres = new CustomerClass.Prescription();
                    pres = (CustomerClass.Prescription)cmbPres.SelectedItem;

                    cProds.prescriptions.NamePresc = pres.NamePresc;
                    
                }
                else
                {
                    cProds.prescriptions.NamePresc = "";
                }
                
                
                string brand = StockList[ind].Name;
                string model = StockList[ind].modelNo;
                string Desc = txtItemDesc.Text;
                totalAmount += StockList[ind].retailPrice;        
                
                
                cProds.ProdName = brand;
                cProds.modelNo = model;
                cProds.Desc = Desc;
                
                //cProds.prescriptions.NamePresc = 
                cus.products.Add(cProds);

                //updateListBox(); // ForChk

                DGCustProds.Rows.Add(brand,model,cProds.prescriptions.NamePresc,Desc);
                
                cmbItems.SelectedIndex = -1;
                cmbItems.Text = "Brand      ModelNo     Rs[Price]";


                txtNetTotalBill.Text = totalAmount.ToString();
                //if (!Double.IsNaN(Convert.ToDouble(txtAdvanceBill.Text)))
                txtBalanceBill.Text = txtNetTotalBill.Text;
                txtAdvanceBill.Text = "0";
                //else txtBalanceBill.Text = txtNetTotalBill.Text;
                StockList[ind].qty -= 1;
                UpdateCmbItems();
               // cmbPres.SelectedIndex = -1;
                btwGenerateBill.Enabled = true;
            }
            else
            {
                MessageBox.Show("Choose Any Frame");
            }
        }

        private void btwClosePres_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This Prescription Cannot be closed!!");
        }

        private void btwDonePres_Click(object sender, EventArgs e)
        {
            dynamicText(); // -->>CHECK THIS FIRST
            btwAddPrescription.Enabled = false;
            btwDonePres.Enabled = false;
        }

        private void btwGenerateBill_Click(object sender, EventArgs e)
        {
            
            
            // bill 
            double adv;
            DateTime today = DateTime.Now;
            string ord = today.ToString();
            DateTime delverydate = Convert.ToDateTime(txtDeliveryDateBill.Text);
            //string del = txtDeliveryDateBill.Text;
            double bal = Convert.ToDouble(txtBalanceBill.Text);
            if (txtAdvanceBill.Text=="")
            {
                adv = 0;
            }
            adv = Convert.ToDouble(txtAdvanceBill.Text);
            string status = lblStatus.Text;
            
            DA.customer_bill_insert(today,delverydate,bal,adv,status,cus.Name);


            for (int i = 0; i < cus.products.Count; i++)
            {
                string n = cus.products[i].prescriptions.NamePresc;
                double Lsph = cus.products[i].prescriptions.leftEyeNo.distance.sph;
                double Lcyl = cus.products[i].prescriptions.leftEyeNo.distance.cyl;
                double Laxis = cus.products[i].prescriptions.leftEyeNo.distance.axis;

            //    DA.SetDistanceLeftEye(n,sph,cyl,axis);

               double Rsph = cus.products[i].prescriptions.rightEyeNo.distance.sph;
               double Rcyl = cus.products[i].prescriptions.rightEyeNo.distance.cyl;
               double Raxis = cus.products[i].prescriptions.rightEyeNo.distance.axis;

               DA.SetDistanceBothEye(n, Lsph, Lcyl, Laxis, Rsph, Rcyl, Raxis);
                // order

               string modelno = cus.products[i].modelNo;
               string co_frame_desc = cus.products[i].Desc;
               DA.SetcustomerOrder(modelno,co_frame_desc);

            }

            //Generate bill also

            for (int i = 0; i < StockList.Count; i++)
            {
                DA.product_stock_Update(StockList[i].Name,StockList[i].modelNo,StockList[i].qty);
            }
            /*
            
             * yahan pe Update ki querry chaalao
             * jo StockList se sare items ko update krde
             * taky jis ki qty kam hoi hy wo update hojae
             * inshort  -->> load pr stocklist me sara stock load hua tha 
             *          -->> ab wo stocklist wapis table me update hogi
             */
        }

        private void btwRemoveItem_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (DGCustProds.SelectedRows.Count > 0)
                {
                    //DataGridViewRow row = DGCustProds.SelectedRows[0];
                    CustomerClass.Products cProds = new CustomerClass.Products();

                    
                    foreach (DataGridViewRow row in DGCustProds.SelectedRows)
                    {
                        cProds = new CustomerClass.Products();
                        cProds.ProdName = row.Cells[0].ToString();
                        cProds.modelNo = row.Cells[1].ToString();
                        cProds.prescriptions.NamePresc = row.Cells[2].ToString();
                        cProds.Desc = row.Cells[3].ToString();
                        cus.products.RemoveAt(row.Index);
                        
                       // updateListBox();
                        DGCustProds.Rows.Remove(row);    
                    }
                    
                }
                else
                {
                    MessageBox.Show("Select Row(s) from Table");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
             
            }
        }

     

        private void txtAdvanceBill_TextChanged(object sender, EventArgs e)
        {
            double bal = Convert.ToDouble(txtBalanceBill.Text);
            double adv = 0;
            double tot = Convert.ToDouble(txtNetTotalBill.Text);
            if (txtAdvanceBill.Text != "")
            {
                adv = Convert.ToDouble(txtAdvanceBill.Text);
            }
            bal = tot - adv;
            if (bal < 0)
            {
                MessageBox.Show("Advance payment is full");
                txtNetTotalBill.Text = totalAmount.ToString();
                txtAdvanceBill.Text = txtNetTotalBill.Text;
                txtBalanceBill.Text = "0";
            }
            else
            {
                txtBalanceBill.Text = bal.ToString();            
            }   
        }

        private void txtDiscountBill_TextChanged(object sender, EventArgs e)
        {
            double discount = 0;
            if (txtDiscountBill.Text != "")
            {
                disAmount = totalAmount;
                discount = Convert.ToDouble(txtDiscountBill.Text);
                disAmount -= discount;
                txtNetTotalBill.Text = disAmount.ToString();
           //     txtAdvanceBill_TextChanged(sender, e);
            }
            else
            {
                disAmount = totalAmount;
                discount = 0;
                disAmount -= discount;
                txtNetTotalBill.Text = disAmount.ToString();
             ///   txtAdvanceBill_TextChanged(sender, e);
            }
        }

        private void txtBalanceBill_TextChanged(object sender, EventArgs e)
        {
            if (txtBalanceBill.Text!= "")
            {
                double bal = Convert.ToDouble(txtBalanceBill.Text);
                if (bal == 0)
                {
                    lblStatus.Text = "paid";
                }
                else if (bal == totalAmount)
                {
                    lblStatus.Text = "unpaid";
                }
                else
                {
                    lblStatus.Text = "partially paid";
                }
            }
        }
    }
}
