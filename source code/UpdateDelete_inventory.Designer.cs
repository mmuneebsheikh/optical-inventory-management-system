﻿namespace VisualProjectOpInventory
{
    partial class UpdateDelete_inventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.txtQuantityVendor = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboxCategoryVendor = new System.Windows.Forms.ComboBox();
            this.txtColorVendor = new System.Windows.Forms.TextBox();
            this.cboxSizeVendor = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtModelNoVendor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIProductNameVendor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtWholeSalePriceVendor = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButtonFemaleVendor = new System.Windows.Forms.RadioButton();
            this.radioButtonMaleVendor = new System.Windows.Forms.RadioButton();
            this.txtRetailPriceVendor = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.modelnotosearch = new System.Windows.Forms.TextBox();
            this.btweditprod = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(22, 315);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 29);
            this.label6.TabIndex = 71;
            this.label6.Text = "Size:";
            // 
            // txtQuantityVendor
            // 
            this.txtQuantityVendor.Location = new System.Drawing.Point(163, 288);
            this.txtQuantityVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtQuantityVendor.Name = "txtQuantityVendor";
            this.txtQuantityVendor.Size = new System.Drawing.Size(208, 20);
            this.txtQuantityVendor.TabIndex = 79;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label13.Location = new System.Drawing.Point(22, 281);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(92, 29);
            this.label13.TabIndex = 78;
            this.label13.Text = "Quantity";
            // 
            // cboxCategoryVendor
            // 
            this.cboxCategoryVendor.FormattingEnabled = true;
            this.cboxCategoryVendor.Items.AddRange(new object[] {
            "Frames",
            "Sunglass"});
            this.cboxCategoryVendor.Location = new System.Drawing.Point(163, 363);
            this.cboxCategoryVendor.Margin = new System.Windows.Forms.Padding(2);
            this.cboxCategoryVendor.Name = "cboxCategoryVendor";
            this.cboxCategoryVendor.Size = new System.Drawing.Size(208, 21);
            this.cboxCategoryVendor.TabIndex = 77;
            this.cboxCategoryVendor.Text = "Select";
            // 
            // txtColorVendor
            // 
            this.txtColorVendor.Location = new System.Drawing.Point(163, 257);
            this.txtColorVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtColorVendor.Name = "txtColorVendor";
            this.txtColorVendor.Size = new System.Drawing.Size(208, 20);
            this.txtColorVendor.TabIndex = 76;
            // 
            // cboxSizeVendor
            // 
            this.cboxSizeVendor.FormattingEnabled = true;
            this.cboxSizeVendor.Items.AddRange(new object[] {
            "48",
            "50",
            "52",
            "53",
            "54",
            "58"});
            this.cboxSizeVendor.Location = new System.Drawing.Point(163, 323);
            this.cboxSizeVendor.Margin = new System.Windows.Forms.Padding(2);
            this.cboxSizeVendor.Name = "cboxSizeVendor";
            this.cboxSizeVendor.Size = new System.Drawing.Size(208, 21);
            this.cboxSizeVendor.TabIndex = 75;
            this.cboxSizeVendor.Text = "Select";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(11, 188);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 29);
            this.label9.TabIndex = 68;
            this.label9.Text = "BrandName";
            // 
            // txtModelNoVendor
            // 
            this.txtModelNoVendor.Location = new System.Drawing.Point(163, 229);
            this.txtModelNoVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtModelNoVendor.Name = "txtModelNoVendor";
            this.txtModelNoVendor.Size = new System.Drawing.Size(208, 20);
            this.txtModelNoVendor.TabIndex = 74;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(22, 222);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 29);
            this.label8.TabIndex = 69;
            this.label8.Text = "Model No:";
            // 
            // txtIProductNameVendor
            // 
            this.txtIProductNameVendor.Location = new System.Drawing.Point(163, 195);
            this.txtIProductNameVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtIProductNameVendor.Name = "txtIProductNameVendor";
            this.txtIProductNameVendor.Size = new System.Drawing.Size(208, 20);
            this.txtIProductNameVendor.TabIndex = 73;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(22, 250);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 29);
            this.label5.TabIndex = 70;
            this.label5.Text = "Color:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(22, 356);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 29);
            this.label7.TabIndex = 72;
            this.label7.Text = "Category:";
            // 
            // txtWholeSalePriceVendor
            // 
            this.txtWholeSalePriceVendor.Location = new System.Drawing.Point(163, 475);
            this.txtWholeSalePriceVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtWholeSalePriceVendor.Name = "txtWholeSalePriceVendor";
            this.txtWholeSalePriceVendor.Size = new System.Drawing.Size(204, 20);
            this.txtWholeSalePriceVendor.TabIndex = 86;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(4, 467);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 29);
            this.label10.TabIndex = 85;
            this.label10.Text = "Whole Sale Price:";
            // 
            // radioButtonFemaleVendor
            // 
            this.radioButtonFemaleVendor.AutoSize = true;
            this.radioButtonFemaleVendor.Location = new System.Drawing.Point(294, 416);
            this.radioButtonFemaleVendor.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonFemaleVendor.Name = "radioButtonFemaleVendor";
            this.radioButtonFemaleVendor.Size = new System.Drawing.Size(59, 17);
            this.radioButtonFemaleVendor.TabIndex = 84;
            this.radioButtonFemaleVendor.TabStop = true;
            this.radioButtonFemaleVendor.Text = "Female";
            this.radioButtonFemaleVendor.UseVisualStyleBackColor = true;
            // 
            // radioButtonMaleVendor
            // 
            this.radioButtonMaleVendor.AutoSize = true;
            this.radioButtonMaleVendor.Location = new System.Drawing.Point(163, 416);
            this.radioButtonMaleVendor.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonMaleVendor.Name = "radioButtonMaleVendor";
            this.radioButtonMaleVendor.Size = new System.Drawing.Size(48, 17);
            this.radioButtonMaleVendor.TabIndex = 83;
            this.radioButtonMaleVendor.TabStop = true;
            this.radioButtonMaleVendor.Text = "Male";
            this.radioButtonMaleVendor.UseVisualStyleBackColor = true;
            // 
            // txtRetailPriceVendor
            // 
            this.txtRetailPriceVendor.Location = new System.Drawing.Point(163, 439);
            this.txtRetailPriceVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtRetailPriceVendor.Name = "txtRetailPriceVendor";
            this.txtRetailPriceVendor.Size = new System.Drawing.Size(204, 20);
            this.txtRetailPriceVendor.TabIndex = 82;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label14.Location = new System.Drawing.Point(11, 436);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 29);
            this.label14.TabIndex = 81;
            this.label14.Text = "Retail Price:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label15.Location = new System.Drawing.Point(22, 408);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 29);
            this.label15.TabIndex = 80;
            this.label15.Text = "Gender:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(301, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 29);
            this.label1.TabIndex = 87;
            this.label1.Text = "Update/Delete Items";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(11, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(285, 29);
            this.label2.TabIndex = 88;
            this.label2.Text = "Enter ModelNo to Update/Delete";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Mistral", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(434, 65);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 29);
            this.label3.TabIndex = 89;
            this.label3.Text = "ModelNo:";
            // 
            // modelnotosearch
            // 
            this.modelnotosearch.Location = new System.Drawing.Point(558, 74);
            this.modelnotosearch.Margin = new System.Windows.Forms.Padding(2);
            this.modelnotosearch.Name = "modelnotosearch";
            this.modelnotosearch.Size = new System.Drawing.Size(175, 20);
            this.modelnotosearch.TabIndex = 90;
            // 
            // btweditprod
            // 
            this.btweditprod.Font = new System.Drawing.Font("Mistral", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btweditprod.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btweditprod.Location = new System.Drawing.Point(453, 146);
            this.btweditprod.Margin = new System.Windows.Forms.Padding(2);
            this.btweditprod.Name = "btweditprod";
            this.btweditprod.Size = new System.Drawing.Size(106, 45);
            this.btweditprod.TabIndex = 91;
            this.btweditprod.Text = "Update";
            this.btweditprod.UseVisualStyleBackColor = true;
            this.btweditprod.Click += new System.EventHandler(this.btweditprod_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Mistral", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.button1.Location = new System.Drawing.Point(593, 146);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 45);
            this.button1.TabIndex = 91;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // UpdateDelete_inventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 529);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btweditprod);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.modelnotosearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWholeSalePriceVendor);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.radioButtonFemaleVendor);
            this.Controls.Add(this.radioButtonMaleVendor);
            this.Controls.Add(this.txtRetailPriceVendor);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtQuantityVendor);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboxCategoryVendor);
            this.Controls.Add(this.txtColorVendor);
            this.Controls.Add(this.cboxSizeVendor);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtModelNoVendor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtIProductNameVendor);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Name = "UpdateDelete_inventory";
            this.Text = "UpdateDelete_inventory";
            this.Load += new System.EventHandler(this.UpdateDelete_inventory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtQuantityVendor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboxCategoryVendor;
        private System.Windows.Forms.TextBox txtColorVendor;
        private System.Windows.Forms.ComboBox cboxSizeVendor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtModelNoVendor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIProductNameVendor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtWholeSalePriceVendor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton radioButtonFemaleVendor;
        private System.Windows.Forms.RadioButton radioButtonMaleVendor;
        private System.Windows.Forms.TextBox txtRetailPriceVendor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox modelnotosearch;
        private System.Windows.Forms.Button btweditprod;
        private System.Windows.Forms.Button button1;
    }
}