﻿namespace VisualProjectOpInventory
{
    partial class New_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(New_Order));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPhoneNomaskedNewOrder = new System.Windows.Forms.MaskedTextBox();
            this.txtNameNewOrder = new System.Windows.Forms.TextBox();
            this.txtAddressNewOrder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtOrderNoNewOrder = new System.Windows.Forms.TextBox();
            this.txtOrderDateNewOrder = new System.Windows.Forms.DateTimePicker();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.labelStatus2NewOrder = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.labelStatus1NewOrder = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btwBackNewOrder = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.labelPayedNewOrder = new System.Windows.Forms.Label();
            this.btwSaveNewOrder = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.labelFullidNewOrder = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.DGNewOrder = new System.Windows.Forms.DataGridView();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtPaidmaskedNewOrder = new System.Windows.Forms.MaskedTextBox();
            this.btwSubmitNewOrder = new System.Windows.Forms.Button();
            this.labelBalanceNewOrder = new System.Windows.Forms.Label();
            this.labelDiscountNewOrder = new System.Windows.Forms.Label();
            this.labelTotalNewOrder = new System.Windows.Forms.Label();
            this.labelSubTotalNewOrder = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicalDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckVendorsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clickStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsStatusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.goToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGNewOrder)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.MainmenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPhoneNomaskedNewOrder);
            this.panel1.Controls.Add(this.txtNameNewOrder);
            this.panel1.Controls.Add(this.txtAddressNewOrder);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(429, 184);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtPhoneNomaskedNewOrder
            // 
            this.txtPhoneNomaskedNewOrder.Location = new System.Drawing.Point(154, 47);
            this.txtPhoneNomaskedNewOrder.Mask = "0000-0000000";
            this.txtPhoneNomaskedNewOrder.Name = "txtPhoneNomaskedNewOrder";
            this.txtPhoneNomaskedNewOrder.Size = new System.Drawing.Size(263, 22);
            this.txtPhoneNomaskedNewOrder.TabIndex = 2;
            this.txtPhoneNomaskedNewOrder.Text = "0000000";
            // 
            // txtNameNewOrder
            // 
            this.txtNameNewOrder.Location = new System.Drawing.Point(94, 11);
            this.txtNameNewOrder.Name = "txtNameNewOrder";
            this.txtNameNewOrder.Size = new System.Drawing.Size(323, 22);
            this.txtNameNewOrder.TabIndex = 3;
            this.txtNameNewOrder.Text = "Enter Name";
            this.txtNameNewOrder.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // txtAddressNewOrder
            // 
            this.txtAddressNewOrder.Location = new System.Drawing.Point(115, 79);
            this.txtAddressNewOrder.Multiline = true;
            this.txtAddressNewOrder.Name = "txtAddressNewOrder";
            this.txtAddressNewOrder.Size = new System.Drawing.Size(302, 89);
            this.txtAddressNewOrder.TabIndex = 2;
            this.txtAddressNewOrder.Text = "Enter Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(12, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Phone No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtOrderNoNewOrder);
            this.panel2.Controls.Add(this.txtOrderDateNewOrder);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(491, 52);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(433, 184);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // txtOrderNoNewOrder
            // 
            this.txtOrderNoNewOrder.Location = new System.Drawing.Point(140, 10);
            this.txtOrderNoNewOrder.Name = "txtOrderNoNewOrder";
            this.txtOrderNoNewOrder.Size = new System.Drawing.Size(282, 22);
            this.txtOrderNoNewOrder.TabIndex = 8;
            // 
            // txtOrderDateNewOrder
            // 
            this.txtOrderDateNewOrder.Location = new System.Drawing.Point(140, 44);
            this.txtOrderDateNewOrder.Name = "txtOrderDateNewOrder";
            this.txtOrderDateNewOrder.Size = new System.Drawing.Size(282, 22);
            this.txtOrderDateNewOrder.TabIndex = 8;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Location = new System.Drawing.Point(251, 79);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(15, 43);
            this.panel8.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.labelStatus2NewOrder);
            this.panel7.Location = new System.Drawing.Point(255, 79);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(167, 43);
            this.panel7.TabIndex = 9;
            // 
            // labelStatus2NewOrder
            // 
            this.labelStatus2NewOrder.AutoSize = true;
            this.labelStatus2NewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus2NewOrder.Location = new System.Drawing.Point(45, 8);
            this.labelStatus2NewOrder.Name = "labelStatus2NewOrder";
            this.labelStatus2NewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelStatus2NewOrder.TabIndex = 7;
            this.labelStatus2NewOrder.Text = ",";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.labelStatus1NewOrder);
            this.panel6.Location = new System.Drawing.Point(90, 79);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(172, 43);
            this.panel6.TabIndex = 8;
            // 
            // labelStatus1NewOrder
            // 
            this.labelStatus1NewOrder.AutoSize = true;
            this.labelStatus1NewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus1NewOrder.Location = new System.Drawing.Point(43, 8);
            this.labelStatus1NewOrder.Name = "labelStatus1NewOrder";
            this.labelStatus1NewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelStatus1NewOrder.TabIndex = 7;
            this.labelStatus1NewOrder.Text = ",";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(3, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Status:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 25);
            this.label7.TabIndex = 4;
            this.label7.Text = "Order No:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Modern No. 20", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label6.Location = new System.Drawing.Point(3, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Order Date:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Location = new System.Drawing.Point(436, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(49, 173);
            this.panel3.TabIndex = 1;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btwBackNewOrder);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.btwSaveNewOrder);
            this.panel4.Controls.Add(this.panel12);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(1, 242);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(923, 529);
            this.panel4.TabIndex = 2;
            // 
            // btwBackNewOrder
            // 
            this.btwBackNewOrder.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBackNewOrder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBackNewOrder.Location = new System.Drawing.Point(788, 489);
            this.btwBackNewOrder.Name = "btwBackNewOrder";
            this.btwBackNewOrder.Size = new System.Drawing.Size(124, 32);
            this.btwBackNewOrder.TabIndex = 21;
            this.btwBackNewOrder.Text = "Back";
            this.btwBackNewOrder.UseVisualStyleBackColor = true;
            this.btwBackNewOrder.Click += new System.EventHandler(this.btwBackNewOrder_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Location = new System.Drawing.Point(414, 464);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(15, 43);
            this.panel10.TabIndex = 10;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.labelPayedNewOrder);
            this.panel11.Location = new System.Drawing.Point(418, 464);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(167, 43);
            this.panel11.TabIndex = 12;
            this.panel11.Paint += new System.Windows.Forms.PaintEventHandler(this.panel11_Paint);
            // 
            // labelPayedNewOrder
            // 
            this.labelPayedNewOrder.AutoSize = true;
            this.labelPayedNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPayedNewOrder.Location = new System.Drawing.Point(45, 8);
            this.labelPayedNewOrder.Name = "labelPayedNewOrder";
            this.labelPayedNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelPayedNewOrder.TabIndex = 7;
            this.labelPayedNewOrder.Text = ",";
            // 
            // btwSaveNewOrder
            // 
            this.btwSaveNewOrder.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSaveNewOrder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwSaveNewOrder.Location = new System.Drawing.Point(301, 426);
            this.btwSaveNewOrder.Name = "btwSaveNewOrder";
            this.btwSaveNewOrder.Size = new System.Drawing.Size(240, 32);
            this.btwSaveNewOrder.TabIndex = 21;
            this.btwSaveNewOrder.Text = "Save";
            this.btwSaveNewOrder.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.labelFullidNewOrder);
            this.panel12.Location = new System.Drawing.Point(253, 464);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(172, 43);
            this.panel12.TabIndex = 11;
            this.panel12.Paint += new System.Windows.Forms.PaintEventHandler(this.panel12_Paint);
            // 
            // labelFullidNewOrder
            // 
            this.labelFullidNewOrder.AutoSize = true;
            this.labelFullidNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFullidNewOrder.Location = new System.Drawing.Point(43, 8);
            this.labelFullidNewOrder.Name = "labelFullidNewOrder";
            this.labelFullidNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelFullidNewOrder.TabIndex = 7;
            this.labelFullidNewOrder.Text = ",";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.DGNewOrder);
            this.panel5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(646, 417);
            this.panel5.TabIndex = 0;
            // 
            // DGNewOrder
            // 
            this.DGNewOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGNewOrder.Location = new System.Drawing.Point(0, 0);
            this.DGNewOrder.Name = "DGNewOrder";
            this.DGNewOrder.RowTemplate.Height = 24;
            this.DGNewOrder.Size = new System.Drawing.Size(643, 414);
            this.DGNewOrder.TabIndex = 0;
            this.DGNewOrder.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGNewOrder_CellContentClick);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtPaidmaskedNewOrder);
            this.panel9.Controls.Add(this.btwSubmitNewOrder);
            this.panel9.Controls.Add(this.labelBalanceNewOrder);
            this.panel9.Controls.Add(this.labelDiscountNewOrder);
            this.panel9.Controls.Add(this.labelTotalNewOrder);
            this.panel9.Controls.Add(this.labelSubTotalNewOrder);
            this.panel9.Controls.Add(this.label12);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.label8);
            this.panel9.Location = new System.Drawing.Point(656, 242);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(265, 420);
            this.panel9.TabIndex = 1;
            this.panel9.Paint += new System.Windows.Forms.PaintEventHandler(this.panel9_Paint);
            // 
            // txtPaidmaskedNewOrder
            // 
            this.txtPaidmaskedNewOrder.Location = new System.Drawing.Point(79, 185);
            this.txtPaidmaskedNewOrder.Mask = "Rs.";
            this.txtPaidmaskedNewOrder.Name = "txtPaidmaskedNewOrder";
            this.txtPaidmaskedNewOrder.Size = new System.Drawing.Size(178, 22);
            this.txtPaidmaskedNewOrder.TabIndex = 20;
            // 
            // btwSubmitNewOrder
            // 
            this.btwSubmitNewOrder.Font = new System.Drawing.Font("Monotype Corsiva", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSubmitNewOrder.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwSubmitNewOrder.Location = new System.Drawing.Point(17, 277);
            this.btwSubmitNewOrder.Name = "btwSubmitNewOrder";
            this.btwSubmitNewOrder.Size = new System.Drawing.Size(240, 32);
            this.btwSubmitNewOrder.TabIndex = 19;
            this.btwSubmitNewOrder.Text = "Submit";
            this.btwSubmitNewOrder.UseVisualStyleBackColor = true;
            // 
            // labelBalanceNewOrder
            // 
            this.labelBalanceNewOrder.AutoSize = true;
            this.labelBalanceNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBalanceNewOrder.Location = new System.Drawing.Point(113, 227);
            this.labelBalanceNewOrder.Name = "labelBalanceNewOrder";
            this.labelBalanceNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelBalanceNewOrder.TabIndex = 18;
            this.labelBalanceNewOrder.Text = ",";
            // 
            // labelDiscountNewOrder
            // 
            this.labelDiscountNewOrder.AutoSize = true;
            this.labelDiscountNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDiscountNewOrder.Location = new System.Drawing.Point(113, 111);
            this.labelDiscountNewOrder.Name = "labelDiscountNewOrder";
            this.labelDiscountNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelDiscountNewOrder.TabIndex = 17;
            this.labelDiscountNewOrder.Text = ",";
            // 
            // labelTotalNewOrder
            // 
            this.labelTotalNewOrder.AutoSize = true;
            this.labelTotalNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalNewOrder.Location = new System.Drawing.Point(78, 78);
            this.labelTotalNewOrder.Name = "labelTotalNewOrder";
            this.labelTotalNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelTotalNewOrder.TabIndex = 16;
            this.labelTotalNewOrder.Text = ",";
            // 
            // labelSubTotalNewOrder
            // 
            this.labelSubTotalNewOrder.AutoSize = true;
            this.labelSubTotalNewOrder.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSubTotalNewOrder.Location = new System.Drawing.Point(113, 42);
            this.labelSubTotalNewOrder.Name = "labelSubTotalNewOrder";
            this.labelSubTotalNewOrder.Size = new System.Drawing.Size(16, 22);
            this.labelSubTotalNewOrder.TabIndex = 8;
            this.labelSubTotalNewOrder.Text = ",";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Modern No. 20", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(19, 228);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 21);
            this.label12.TabIndex = 14;
            this.label12.Text = "Balance:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Modern No. 20", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label11.Location = new System.Drawing.Point(19, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 21);
            this.label11.TabIndex = 13;
            this.label11.Text = "Paid:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Modern No. 20", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(13, 112);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 21);
            this.label10.TabIndex = 12;
            this.label10.Text = "Discount:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Modern No. 20", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label9.Location = new System.Drawing.Point(13, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 21);
            this.label9.TabIndex = 11;
            this.label9.Text = "Total:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Modern No. 20", 10.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(13, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 21);
            this.label8.TabIndex = 10;
            this.label8.Text = "Sub Total:";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.MainmenuStrip1);
            this.panel13.Location = new System.Drawing.Point(4, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(920, 35);
            this.panel13.TabIndex = 8;
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Segoe WP", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.goToToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Size = new System.Drawing.Size(920, 31);
            this.MainmenuStrip1.TabIndex = 1;
            this.MainmenuStrip1.Text = "menuStrip1";
            this.MainmenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MainmenuStrip1_ItemClicked);
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsTypeToolStripMenuItem,
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1,
            this.vendorsStatusToolStripMenuItem,
            this.paymentsStatusToolStripMenuItem});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(86, 27);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsTypeToolStripMenuItem
            // 
            this.vendorsTypeToolStripMenuItem.Name = "vendorsTypeToolStripMenuItem";
            this.vendorsTypeToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsTypeToolStripMenuItem.Text = "VendorsPage";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.ordersToolStripMenuItem.Text = "VendorsType";
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(277, 28);
            this.vendorsNameToolStripMenuItem1.Text = "VendorsName";
            // 
            // vendorsStatusToolStripMenuItem
            // 
            this.vendorsStatusToolStripMenuItem.Name = "vendorsStatusToolStripMenuItem";
            this.vendorsStatusToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.vendorsStatusToolStripMenuItem.Text = "VendorsStatus";
            // 
            // paymentsStatusToolStripMenuItem
            // 
            this.paymentsStatusToolStripMenuItem.Name = "paymentsStatusToolStripMenuItem";
            this.paymentsStatusToolStripMenuItem.Size = new System.Drawing.Size(277, 28);
            this.paymentsStatusToolStripMenuItem.Text = "VendorsPaymentsStatus";
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem2,
            this.ordersToolStripMenuItem1,
            this.customerNameToolStripMenuItem,
            this.customerAddToolStripMenuItem,
            this.customerRemoveToolStripMenuItem,
            this.customerUpdateToolStripMenuItem,
            this.customerStatusToolStripMenuItem,
            this.customerPaymentStatusToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(99, 27);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // customerToolStripMenuItem2
            // 
            this.customerToolStripMenuItem2.Name = "customerToolStripMenuItem2";
            this.customerToolStripMenuItem2.Size = new System.Drawing.Size(283, 28);
            this.customerToolStripMenuItem2.Text = "CustomerPage";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(283, 28);
            this.ordersToolStripMenuItem1.Text = "Orders";
            // 
            // customerNameToolStripMenuItem
            // 
            this.customerNameToolStripMenuItem.Name = "customerNameToolStripMenuItem";
            this.customerNameToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerNameToolStripMenuItem.Text = "CustomerName";
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerAddToolStripMenuItem.Text = "CustomerAdd";
            // 
            // customerRemoveToolStripMenuItem
            // 
            this.customerRemoveToolStripMenuItem.Name = "customerRemoveToolStripMenuItem";
            this.customerRemoveToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerRemoveToolStripMenuItem.Text = "CustomerRemove";
            // 
            // customerUpdateToolStripMenuItem
            // 
            this.customerUpdateToolStripMenuItem.Name = "customerUpdateToolStripMenuItem";
            this.customerUpdateToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerUpdateToolStripMenuItem.Text = "CustomerUpdate";
            // 
            // customerStatusToolStripMenuItem
            // 
            this.customerStatusToolStripMenuItem.Name = "customerStatusToolStripMenuItem";
            this.customerStatusToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerStatusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // customerPaymentStatusToolStripMenuItem
            // 
            this.customerPaymentStatusToolStripMenuItem.Name = "customerPaymentStatusToolStripMenuItem";
            this.customerPaymentStatusToolStripMenuItem.Size = new System.Drawing.Size(283, 28);
            this.customerPaymentStatusToolStripMenuItem.Text = "CustomerPaymentStatus";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monthlySalesToolStripMenuItem,
            this.yearlySalesToolStripMenuItem,
            this.profitsToolStripMenuItem,
            this.lossToolStripMenuItem,
            this.graphicalDataToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(62, 27);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // monthlySalesToolStripMenuItem
            // 
            this.monthlySalesToolStripMenuItem.Name = "monthlySalesToolStripMenuItem";
            this.monthlySalesToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.monthlySalesToolStripMenuItem.Text = "MonthlySales";
            // 
            // yearlySalesToolStripMenuItem
            // 
            this.yearlySalesToolStripMenuItem.Name = "yearlySalesToolStripMenuItem";
            this.yearlySalesToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.yearlySalesToolStripMenuItem.Text = "YearlySales";
            // 
            // profitsToolStripMenuItem
            // 
            this.profitsToolStripMenuItem.Name = "profitsToolStripMenuItem";
            this.profitsToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.profitsToolStripMenuItem.Text = "Profits";
            // 
            // lossToolStripMenuItem
            // 
            this.lossToolStripMenuItem.Name = "lossToolStripMenuItem";
            this.lossToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.lossToolStripMenuItem.Text = "Loss";
            // 
            // graphicalDataToolStripMenuItem
            // 
            this.graphicalDataToolStripMenuItem.Name = "graphicalDataToolStripMenuItem";
            this.graphicalDataToolStripMenuItem.Size = new System.Drawing.Size(200, 28);
            this.graphicalDataToolStripMenuItem.Text = "GraphicalData";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toAddCustomerToolStripMenuItem,
            this.toCheckCustomerStatusToolStripMenuItem,
            this.toAddVendorsToolStripMenuItem,
            this.toCheckVendorsStatusToolStripMenuItem,
            this.paymentsToolStripMenuItem,
            this.ordersToolStripMenuItem2});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(60, 27);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // toAddCustomerToolStripMenuItem
            // 
            this.toAddCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem3});
            this.toAddCustomerToolStripMenuItem.Name = "toAddCustomerToolStripMenuItem";
            this.toAddCustomerToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toAddCustomerToolStripMenuItem.Text = "ToAddCustomer";
            // 
            // customerToolStripMenuItem3
            // 
            this.customerToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
            this.customerToolStripMenuItem3.Name = "customerToolStripMenuItem3";
            this.customerToolStripMenuItem3.Size = new System.Drawing.Size(202, 28);
            this.customerToolStripMenuItem3.Text = "ClickCustomer";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(236, 28);
            this.addToolStripMenuItem.Text = "ClickCustomerAdd";
            // 
            // toCheckCustomerStatusToolStripMenuItem
            // 
            this.toCheckCustomerStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerToolStripMenuItem});
            this.toCheckCustomerStatusToolStripMenuItem.Name = "toCheckCustomerStatusToolStripMenuItem";
            this.toCheckCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toCheckCustomerStatusToolStripMenuItem.Text = "ToCheckCustomerStatus";
            // 
            // clickCustomerToolStripMenuItem
            // 
            this.clickCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerStatusToolStripMenuItem});
            this.clickCustomerToolStripMenuItem.Name = "clickCustomerToolStripMenuItem";
            this.clickCustomerToolStripMenuItem.Size = new System.Drawing.Size(202, 28);
            this.clickCustomerToolStripMenuItem.Text = "ClickCustomer";
            // 
            // clickCustomerStatusToolStripMenuItem
            // 
            this.clickCustomerStatusToolStripMenuItem.Name = "clickCustomerStatusToolStripMenuItem";
            this.clickCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 28);
            this.clickCustomerStatusToolStripMenuItem.Text = "ClickCustomerStatus";
            // 
            // toAddVendorsToolStripMenuItem
            // 
            this.toAddVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem});
            this.toAddVendorsToolStripMenuItem.Name = "toAddVendorsToolStripMenuItem";
            this.toAddVendorsToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toAddVendorsToolStripMenuItem.Text = "ToAddVendors";
            // 
            // clickVendorsToolStripMenuItem
            // 
            this.clickVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorAddToolStripMenuItem});
            this.clickVendorsToolStripMenuItem.Name = "clickVendorsToolStripMenuItem";
            this.clickVendorsToolStripMenuItem.Size = new System.Drawing.Size(189, 28);
            this.clickVendorsToolStripMenuItem.Text = "ClickVendors";
            // 
            // clickVendorAddToolStripMenuItem
            // 
            this.clickVendorAddToolStripMenuItem.Name = "clickVendorAddToolStripMenuItem";
            this.clickVendorAddToolStripMenuItem.Size = new System.Drawing.Size(216, 28);
            this.clickVendorAddToolStripMenuItem.Text = "ClickVendorAdd";
            // 
            // toCheckVendorsStatusToolStripMenuItem
            // 
            this.toCheckVendorsStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem1});
            this.toCheckVendorsStatusToolStripMenuItem.Name = "toCheckVendorsStatusToolStripMenuItem";
            this.toCheckVendorsStatusToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.toCheckVendorsStatusToolStripMenuItem.Text = "ToCheckVendorsStatus";
            // 
            // clickVendorsToolStripMenuItem1
            // 
            this.clickVendorsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickStatusToolStripMenuItem});
            this.clickVendorsToolStripMenuItem1.Name = "clickVendorsToolStripMenuItem1";
            this.clickVendorsToolStripMenuItem1.Size = new System.Drawing.Size(189, 28);
            this.clickVendorsToolStripMenuItem1.Text = "ClickVendors";
            // 
            // clickStatusToolStripMenuItem
            // 
            this.clickStatusToolStripMenuItem.Name = "clickStatusToolStripMenuItem";
            this.clickStatusToolStripMenuItem.Size = new System.Drawing.Size(175, 28);
            this.clickStatusToolStripMenuItem.Text = "ClickStatus";
            // 
            // paymentsToolStripMenuItem
            // 
            this.paymentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem4,
            this.vendorsToolStripMenuItem});
            this.paymentsToolStripMenuItem.Name = "paymentsToolStripMenuItem";
            this.paymentsToolStripMenuItem.Size = new System.Drawing.Size(279, 28);
            this.paymentsToolStripMenuItem.Text = "Payments";
            // 
            // customerToolStripMenuItem4
            // 
            this.customerToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripMenuItem,
            this.vendorsStatusToolStripMenuItem1});
            this.customerToolStripMenuItem4.Name = "customerToolStripMenuItem4";
            this.customerToolStripMenuItem4.Size = new System.Drawing.Size(163, 28);
            this.customerToolStripMenuItem4.Text = "Customer";
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(213, 28);
            this.statusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // vendorsStatusToolStripMenuItem1
            // 
            this.vendorsStatusToolStripMenuItem1.Name = "vendorsStatusToolStripMenuItem1";
            this.vendorsStatusToolStripMenuItem1.Size = new System.Drawing.Size(213, 28);
            this.vendorsStatusToolStripMenuItem1.Text = "VendorsStatus";
            // 
            // vendorsToolStripMenuItem
            // 
            this.vendorsToolStripMenuItem.Name = "vendorsToolStripMenuItem";
            this.vendorsToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.vendorsToolStripMenuItem.Text = "Vendors";
            // 
            // ordersToolStripMenuItem2
            // 
            this.ordersToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem5,
            this.vendorsToolStripMenuItem1});
            this.ordersToolStripMenuItem2.Name = "ordersToolStripMenuItem2";
            this.ordersToolStripMenuItem2.Size = new System.Drawing.Size(279, 28);
            this.ordersToolStripMenuItem2.Text = "Orders";
            // 
            // customerToolStripMenuItem5
            // 
            this.customerToolStripMenuItem5.Name = "customerToolStripMenuItem5";
            this.customerToolStripMenuItem5.Size = new System.Drawing.Size(163, 28);
            this.customerToolStripMenuItem5.Text = "Customer";
            // 
            // vendorsToolStripMenuItem1
            // 
            this.vendorsToolStripMenuItem1.Name = "vendorsToolStripMenuItem1";
            this.vendorsToolStripMenuItem1.Size = new System.Drawing.Size(163, 28);
            this.vendorsToolStripMenuItem1.Text = "Vendors";
            // 
            // goToToolStripMenuItem
            // 
            this.goToToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem6,
            this.vendorsToolStripMenuItem2,
            this.salesToolStripMenuItem1,
            this.ordersToolStripMenuItem3,
            this.paymentsToolStripMenuItem1,
            this.statusToolStripMenuItem1,
            this.loginToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.goToToolStripMenuItem.Name = "goToToolStripMenuItem";
            this.goToToolStripMenuItem.Size = new System.Drawing.Size(62, 27);
            this.goToToolStripMenuItem.Text = "GoTo";
            // 
            // customerToolStripMenuItem6
            // 
            this.customerToolStripMenuItem6.Name = "customerToolStripMenuItem6";
            this.customerToolStripMenuItem6.Size = new System.Drawing.Size(163, 28);
            this.customerToolStripMenuItem6.Text = "Customer";
            // 
            // vendorsToolStripMenuItem2
            // 
            this.vendorsToolStripMenuItem2.Name = "vendorsToolStripMenuItem2";
            this.vendorsToolStripMenuItem2.Size = new System.Drawing.Size(163, 28);
            this.vendorsToolStripMenuItem2.Text = "Vendors";
            // 
            // salesToolStripMenuItem1
            // 
            this.salesToolStripMenuItem1.Name = "salesToolStripMenuItem1";
            this.salesToolStripMenuItem1.Size = new System.Drawing.Size(163, 28);
            this.salesToolStripMenuItem1.Text = "Sales";
            // 
            // ordersToolStripMenuItem3
            // 
            this.ordersToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerOrdersToolStripMenuItem,
            this.vendorsOrdersToolStripMenuItem});
            this.ordersToolStripMenuItem3.Name = "ordersToolStripMenuItem3";
            this.ordersToolStripMenuItem3.Size = new System.Drawing.Size(163, 28);
            this.ordersToolStripMenuItem3.Text = "Orders";
            // 
            // customerOrdersToolStripMenuItem
            // 
            this.customerOrdersToolStripMenuItem.Name = "customerOrdersToolStripMenuItem";
            this.customerOrdersToolStripMenuItem.Size = new System.Drawing.Size(217, 28);
            this.customerOrdersToolStripMenuItem.Text = "CustomerOrders";
            // 
            // vendorsOrdersToolStripMenuItem
            // 
            this.vendorsOrdersToolStripMenuItem.Name = "vendorsOrdersToolStripMenuItem";
            this.vendorsOrdersToolStripMenuItem.Size = new System.Drawing.Size(217, 28);
            this.vendorsOrdersToolStripMenuItem.Text = "VendorsOrders";
            // 
            // paymentsToolStripMenuItem1
            // 
            this.paymentsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem7,
            this.vendorsToolStripMenuItem3});
            this.paymentsToolStripMenuItem1.Name = "paymentsToolStripMenuItem1";
            this.paymentsToolStripMenuItem1.Size = new System.Drawing.Size(163, 28);
            this.paymentsToolStripMenuItem1.Text = "Payments";
            // 
            // customerToolStripMenuItem7
            // 
            this.customerToolStripMenuItem7.Name = "customerToolStripMenuItem7";
            this.customerToolStripMenuItem7.Size = new System.Drawing.Size(170, 28);
            this.customerToolStripMenuItem7.Text = "Customers";
            // 
            // vendorsToolStripMenuItem3
            // 
            this.vendorsToolStripMenuItem3.Name = "vendorsToolStripMenuItem3";
            this.vendorsToolStripMenuItem3.Size = new System.Drawing.Size(170, 28);
            this.vendorsToolStripMenuItem3.Text = "Vendors";
            // 
            // statusToolStripMenuItem1
            // 
            this.statusToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem8,
            this.vendorsToolStripMenuItem4});
            this.statusToolStripMenuItem1.Name = "statusToolStripMenuItem1";
            this.statusToolStripMenuItem1.Size = new System.Drawing.Size(163, 28);
            this.statusToolStripMenuItem1.Text = "Status";
            // 
            // customerToolStripMenuItem8
            // 
            this.customerToolStripMenuItem8.Name = "customerToolStripMenuItem8";
            this.customerToolStripMenuItem8.Size = new System.Drawing.Size(170, 28);
            this.customerToolStripMenuItem8.Text = "Customers";
            // 
            // vendorsToolStripMenuItem4
            // 
            this.vendorsToolStripMenuItem4.Name = "vendorsToolStripMenuItem4";
            this.vendorsToolStripMenuItem4.Size = new System.Drawing.Size(170, 28);
            this.vendorsToolStripMenuItem4.Text = "Vendors";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(163, 28);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // New_Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(925, 775);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "New_Order";
            this.Text = "New_Order";
            this.Load += new System.EventHandler(this.New_Order_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGNewOrder)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.MaskedTextBox txtPhoneNomaskedNewOrder;
        private System.Windows.Forms.TextBox txtNameNewOrder;
        private System.Windows.Forms.TextBox txtAddressNewOrder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOrderNoNewOrder;
        private System.Windows.Forms.DateTimePicker txtOrderDateNewOrder;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label labelStatus2NewOrder;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label labelStatus1NewOrder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btwBackNewOrder;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label labelPayedNewOrder;
        private System.Windows.Forms.Button btwSaveNewOrder;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label labelFullidNewOrder;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.MaskedTextBox txtPaidmaskedNewOrder;
        private System.Windows.Forms.Button btwSubmitNewOrder;
        private System.Windows.Forms.Label labelBalanceNewOrder;
        private System.Windows.Forms.Label labelDiscountNewOrder;
        private System.Windows.Forms.Label labelTotalNewOrder;
        private System.Windows.Forms.Label labelSubTotalNewOrder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicalDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckVendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clickStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem goToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem customerOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.DataGridView DGNewOrder;
    }
}