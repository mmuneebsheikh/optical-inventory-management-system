﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class AdminPanel : Form
    {
        DataAccessClass DA = new DataAccessClass();
        //AdminPanel AP = new AdminPanel();
        public AdminPanel()
        {
            InitializeComponent();
        }


        void OnLoad()
        {
            try
            {
                //load all ther users in a combobox
                cmbUsers.Items.Clear();
                int rows = DA.GetAllUserNames().Tables["user"].Rows.Count;
                for (int i = 0; i < rows; i++)
                {
                    cmbUsers.Items.Add(DA.GetAllUserNames().Tables["user"].Rows[i].ItemArray[0]);
                }
                if (rows > 0)
                {
                    cmbUsers.SelectedIndex = 0;
                }
                txtPasswordLogin.Text = "";
                txtUsernameLogin.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void AdminPanel_Load(object sender, EventArgs e)
        {
            OnLoad();
        }

        private void insert_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtUsernameLogin.Text) && !string.IsNullOrWhiteSpace(txtPasswordLogin.Text))
                {
                    string u = txtUsernameLogin.Text;
                    string p = txtUsernameLogin.Text;
                    DA.user_insert(u, p);

                }
                else
                {
                    MessageBox.Show("Please Fill The Required Fields");

                }
                OnLoad();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void Update_Click_1(object sender, EventArgs e)
        {
            if (cmbUsers.SelectedIndex != -1)
            {
                string toUpdate = cmbUsers.SelectedItem.ToString();
                if (!string.IsNullOrWhiteSpace(txtUsernameLogin.Text) && !string.IsNullOrWhiteSpace(txtPasswordLogin.Text))
                {
                    string u = txtUsernameLogin.Text;
                    string p = txtUsernameLogin.Text;
                    DA.user_update(toUpdate, u, p);
                    
                }
                else
                {
                    MessageBox.Show("Please Fill The Required Fields");

                }
            }
            else
            {
                MessageBox.Show("Please Select A User");
            }
            OnLoad();
        }

        private void Delete_Click_1(object sender, EventArgs e)
        {
            if (cmbUsers.SelectedIndex != -1)
            {
                
                string toDelete = cmbUsers.SelectedItem.ToString();
                DialogResult dr = MessageBox.Show("Are You sure you want to delete " + toDelete + " user?", "Confirm Delete", MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {     
                    if(toDelete != "admin")
                      DA.user_delete(toDelete);
                    else
                    {
                        MessageBox.Show("'admin' cannot be deleted");
                    }
                }
                OnLoad();
            }
        }

        private void btwBackInventory_Click(object sender, EventArgs e)
        {
            OpeningTheme OT = new OpeningTheme();
            this.Hide();
            OT.Show();
        }
    }
}
