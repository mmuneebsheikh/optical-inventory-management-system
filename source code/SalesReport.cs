﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class SalesReport : Form
    {

        DataAccessClass DA = new DataAccessClass();
        
        public SalesReport()
        {
            InitializeComponent();
        }

        private int GetMonthNum(string month)
        {
            if (month == "January")
            {
                return 1;
            }
            else if (month == "Feburary")
            {
                return 2;
            }
            else if (month == "March")
            {
                return 3;
            }
            else if (month == "April")
            {
                return 4;
            }
            else if (month == "May")
            {
                return 5;
            }
            else if (month == "June")
            {
                return 6;
            }
            else if (month == "July")
            {
                return 7;
            }
            else if (month == "August")
            {
                return 8;
            }
            else if (month == "September")
            {
                return 9;
            }
            else if (month == "Octuber")
            {
                return 10;
            }
            else if (month == "November")
            {
                return 11;
            }
            else if (month == "December")
            {
                return 12;
            }
            else return 0;
        }


        private void btwSearch_Click(object sender, EventArgs e)
        {
            if (cmbMonth.SelectedIndex != -1)
            {
                string m = cmbMonth.SelectedItem.ToString();
                int monthNum = GetMonthNum(m);
                if (monthNum != 0)
                {
                    MessageBox.Show(monthNum.ToString());
                    //DataTable monthlyBills =  DA.GetBillsByMonth(monthNum).Tables["monthly_sale"];
                    DGMonthlyBills.DataSource = DA.GetBillsByMonth(monthNum).Tables["monthly_sale"];
                }
                else
                {
                    MessageBox.Show("Please Enter A valid Month");
                }
            }
        }

        private void btwBack_Click(object sender, EventArgs e)
        {
            OpeningTheme OT = new OpeningTheme();
            this.Hide();
            OT.Show();
        }

        private void btwGenerateReport_Click(object sender, EventArgs e)
        {
            if (cmbMonth.SelectedIndex != -1)
            {
                string m = cmbMonth.SelectedItem.ToString();
                int monthNum = GetMonthNum(m);
                if (monthNum != 0)
                {
                    //MessageBox.Show(monthNum.ToString());
                    //DataTable monthlyBills =  DA.GetBillsByMonth(monthNum).Tables["monthly_sale"];

                    DA.SetmonthlySales(monthNum);

                    DGMonthlyBills.DataSource = DA.GetBillsByMonth(monthNum).Tables["monthly_sale"];
                }
                else
                {
                    MessageBox.Show("Please Enter A valid Month");
                }
            }
        }

        private void btwShowReports_Click(object sender, EventArgs e)
        {
            DGMonthlyBills.DataSource = DA.GetAllReports().Tables["monthly_sale"];
        }

    }
}
