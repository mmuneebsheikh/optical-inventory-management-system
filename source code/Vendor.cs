﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Vendor : Form
    {
        string vendorName;
        VendorClass vd = new VendorClass();
        DataAccessClass DA = new DataAccessClass();
        
        int f;
        public Vendor()
        {
            InitializeComponent();
            f = 0;
            FormConfig(f);
        }
        public Vendor(string vendorName)
        {
            f = 1;
            InitializeComponent();
            this.vendorName = vendorName;
            FormConfig(f);
        }
        //public Vendor(VendorClass vendor)
        //{
        //    f = 1;
        //    InitializeComponent();
        //    vd = vendor;
        //    SetVendor();
        //    FormConfig(f);
        //}
        void FormConfig(int flag)
        {
            if (string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                btwVendorBillVendor.Enabled = false;
            }
            else
            {
                btwVendorBillVendor.Enabled = true;
            }
            if (flag == 0) // // When Form is loaded for New Vendor
            {
                btwAddVendor.Enabled = true;
                btwVendorBillVendor.Enabled = false;
            
            }
            else if (flag == 1)
            {
                SetVendor();
                txtNameVendor.ReadOnly = true ;
                txtCreditVendor.ReadOnly = true;
                txtBalanceVendor.ReadOnly = true;
                txtAddressVendor.ReadOnly = true;
                txtPhoneNomaskedVendor.ReadOnly = true;
                btwAddVendor.Enabled = false;
               
            }
            DGBills.DataSource = DA.GetVendorBillsByName(vendorName).Tables["vendor_bill"];
        }


        void SetVendor() 
        {
            //DGBills.DataSource = DA.vendor_orders(vendorName).Tables["vendor_bill"];
            
            try
            {
                //var vendorDetail = Tuple.Create(DA.vendor_detail_display(vendorName));

                DataTable vendDetail = DA.GetVendorDetailsByName(vendorName).Tables["vendor_detail"];

                vd.Name = vendDetail.Rows[0].ItemArray[1].ToString();
                
                //vd.Name = vendorDetail.Item1.Item1.ToString();
                vd.Address = vendDetail.Rows[0].ItemArray[2].ToString();
                vd.contactNo = vendDetail.Rows[0].ItemArray[3].ToString();
                vd.credit = Convert.ToDouble(vendDetail.Rows[0].ItemArray[4]);
                vd.balance = Convert.ToDouble(vendDetail.Rows[0].ItemArray[5]);

                txtNameVendor.Text = vd.Name;
                txtPhoneNomaskedVendor.Text = vd.contactNo;
                txtAddressVendor.Text = vd.Address;
                txtCreditVendor.Text = vd.credit.ToString();
                txtBalanceVendor.Text = vd.balance.ToString();

             

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btwBack_Click(object sender, EventArgs e)
        {
            MainPage obj2 = new MainPage();
            //this.Close();
            this.Hide();
            obj2.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       

        private void btwAddVendor_Click(object sender, EventArgs e)
        {

        }
        private void btwSaveVendor_Click(object sender, EventArgs e)
        {
            //vd.Name = txtNameVendor.Text;
            //vd.Address = txtAddressVendor.Text;
            //vd.contactNo = txtPhoneNomaskedVendor.Text;
            //vd.credit = Convert.ToDouble(txtCreditVendor.Text);
            //vd.balance = Convert.ToDouble(txtBalanceVendor.Text);

            //// Vendor Detail ki querry
            //DA.vendor_detail_insert(vd.Name, vd.Address, vd.contactNo, vd.credit, vd.balance);
            
        
        }

        private void btwVendorBillVendor_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login("vendorBill",vd);
            login.Show();
            //VendorBill vendorBill = new VendorBill(vd);
            //vendorBill.Show();

        }

        private void btwVendorList_Click(object sender, EventArgs e)
        {
            this.Hide();
            VendorList VL = new VendorList();
            VL.Show();
        }

        private void txtNameVendor_TextChanged(object sender, EventArgs e)
        {
            FormConfig(f);
        }

        private void btwAddVendor_Click_1(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                vd.Name = txtNameVendor.Text;
                vd.Address = txtAddressVendor.Text;
                vd.contactNo = txtPhoneNomaskedVendor.Text;
                vd.credit = 0;
                vd.balance = 0;
                DA.vendor_detail_insert(vd.Name, vd.Address, vd.contactNo, vd.credit, vd.balance);
                btwVendorBillVendor.Enabled = true;
                vendorName = vd.Name;
                FormConfig(1);                
            }
            else
            {
                MessageBox.Show("PLease Enter Name");
                txtNameVendor.Focus();
            }

        }

        private void MenuStripVendorOrders_clicked(object sender, EventArgs e)
        {
            this.Hide();
            VendorsOrder VO = new VendorsOrder();
            VO.Show();
        }

        private void MenuStripNewVendor_clicked(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vendor = new Vendor();
            vendor.Show();
        }

        private void MenuStripVendorList(object sender, EventArgs e)
        {
            this.Hide();
            VendorList VL = new VendorList();
            VL.Show();
        }

        private void MenuStripMainMenu(object sender, EventArgs e)
        {
            this.Hide();
            OpeningTheme OT = new OpeningTheme();
            OT.Show();
        }

        private void btwBack_Click_1(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
            OpeningTheme OT = new OpeningTheme();
            OT.Show();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Vendor_Load(object sender, EventArgs e)
        {

        }

        private void ordersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Orders O = new Orders();
            this.Hide();
            O.Show();
        }

        private void customerAddToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            CustomerMainPage CM = new CustomerMainPage();
            this.Hide();
            CM.Show();
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerList CL = new CustomerList();
            this.Hide();
            CL.Show();
        }

        private void salesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login L = new Login("Sales", null);
            this.Hide();
            L.Show();
        }
    }
}
