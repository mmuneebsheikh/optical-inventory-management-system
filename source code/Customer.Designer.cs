﻿namespace VisualProjectOpInventory
{
    partial class Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Customer
            // 
            this.ClientSize = new System.Drawing.Size(1037, 517);
            this.Name = "Customer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicalDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckVendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clickStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem goToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem customerOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtCusSearch;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btwSearch;
        private System.Windows.Forms.Button btwBack;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}