﻿namespace VisualProjectOpInventory
{
    partial class Opening
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Opening));
            this.btwWelcomeToOpticanWelcome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btwWelcomeToOpticanWelcome
            // 
            this.btwWelcomeToOpticanWelcome.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btwWelcomeToOpticanWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btwWelcomeToOpticanWelcome.Font = new System.Drawing.Font("Modern No. 20", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwWelcomeToOpticanWelcome.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btwWelcomeToOpticanWelcome.Location = new System.Drawing.Point(0, 0);
            this.btwWelcomeToOpticanWelcome.Margin = new System.Windows.Forms.Padding(2);
            this.btwWelcomeToOpticanWelcome.Name = "btwWelcomeToOpticanWelcome";
            this.btwWelcomeToOpticanWelcome.Size = new System.Drawing.Size(863, 425);
            this.btwWelcomeToOpticanWelcome.TabIndex = 0;
            this.btwWelcomeToOpticanWelcome.Text = "Welcome To Optican";
            this.btwWelcomeToOpticanWelcome.UseVisualStyleBackColor = false;
            this.btwWelcomeToOpticanWelcome.Click += new System.EventHandler(this.openprojectbtw_Click);
            // 
            // Opening
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 425);
            this.Controls.Add(this.btwWelcomeToOpticanWelcome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Opening";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Opening_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btwWelcomeToOpticanWelcome;
    }
}