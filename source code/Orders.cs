﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class Orders : Form
    {

        DataAccessClass DA = new DataAccessClass();

        public Orders()
        {
            InitializeComponent();
        }





        private void btwBack_Click(object sender, EventArgs e)
        {
            CustomerList obj4 = new CustomerList();
            this.Hide();
            obj4.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Orders_Load(object sender, EventArgs e)
        {
            DataTable bills = DA.GetAllCustomerBills().Tables["customer_bill"];

            DGOrders.DataSource = bills;



        }

        private void btwSearchOrders_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtVendorSearch.Text))
                {
                    int billNo = Convert.ToInt32(txtVendorSearch.Text);
                    DataTable bill = DA.GetCustomerBillByID(billNo).Tables["customer_bill"];
                    DGOrders.DataSource = bill;
                }
                else
                {
                    Orders_Load(sender,e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
    }
}
