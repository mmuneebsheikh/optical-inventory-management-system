﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class UpdateDelete_inventory : Form
    {
        DataAccessClass DA = new DataAccessClass();
        VendorClass.Product prods = new VendorClass.Product();
        string modelNo;
        public UpdateDelete_inventory()
        {
            InitializeComponent();
        }

        public UpdateDelete_inventory(string modelNo)
        {
            InitializeComponent();
            this.modelNo = modelNo;
        }


        void SetForm()
        {
            prods.prodName = DA.GetProductStockByModelno(modelNo).Tables["product_stock"].Rows[0].ItemArray[0].ToString();
            prods.modelNo = DA.GetProductStockByModelno(modelNo).Tables["product_stock"].Rows[0].ItemArray[1].ToString();
            prods.color = DA.GetProductStockByModelno(modelNo).Tables["product_stock"].Rows[0].ItemArray[2].ToString();
        }


        private void UpdateDelete_inventory_Load(object sender, EventArgs e)
        {

        }

        private void btweditprod_Click(object sender, EventArgs e)
        {
            string name = modelnotosearch.Text;
            string prodName = txtIProductNameVendor.Text;
            string modelNo = txtModelNoVendor.Text;
            string color = txtColorVendor.Text;
            int qty = Convert.ToInt32(txtQuantityVendor.Text);
            //string size = cboxSizeVendor.GetItemText(this.cboxSizeVendor.SelectedItem);
            string size = cboxSizeVendor.SelectedItem.ToString();
            //cate = cboxCategoryVendor.GetItemText(this.cboxCategoryVendor.SelectedItem);
            string cate = cboxCategoryVendor.SelectedItem.ToString();
            string gender = "";
            if (radioButtonMaleVendor.Checked)
            {
                gender = "Male";
            }
            else { gender = "Female"; }
            double retPrice = Convert.ToDouble(txtRetailPriceVendor.Text);
            double wholePrice = Convert.ToDouble(txtWholeSalePriceVendor.Text);
            DA.product_stock_Update(name,prodName,modelNo,color,qty,int.Parse(size),retPrice,cate,gender,wholePrice);
            MainPage mp = new MainPage();
            this.Hide();
            mp.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = modelnotosearch.Text;
            DA.product_stock_Delete(name);
            MainPage mp = new MainPage();
            this.Hide();
            mp.Show();
        }
    }
}
