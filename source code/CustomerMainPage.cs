﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class CustomerMainPage : Form
    {
        DataAccessClass DA = new DataAccessClass();
        CustomerClass cus = new CustomerClass();
        int f;
        string CustomerName;
        public CustomerMainPage()
        {
            InitializeComponent();
            f = 0;
            FormConfig(f);
        }
        public CustomerMainPage(string CustomerName)
        {
            f = 1;
            InitializeComponent();
            this.CustomerName = CustomerName;
            FormConfig(f);
        }
        void FormConfig(int flag)
        {
            if (string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                btwCustomerBillCustomer.Enabled = false;
            }
            else
            {
                btwCustomerBillCustomer.Enabled = true;
            }
            if (flag == 0) // // When Form is loaded for New Vendor
            {
                btwAddCustomer.Enabled = true;
                btwCustomerBillCustomer.Enabled = false;

            }
            else if (flag == 1)
            {
                SetCustomer();
                txtNameVendor.ReadOnly = true;
                txtAddressVendor.ReadOnly = true;
                txtPhoneNomaskedVendor.ReadOnly = true;
                btwAddCustomer.Enabled = false;

            }
            DGBills.DataSource = DA.GetCustomerBillsByName(CustomerName).Tables["customer_bill"];
        }


        void SetCustomer()
        {
            //DGBills.DataSource = DA.vendor_orders(vendorName).Tables["vendor_bill"];

            try
            {
                //var vendorDetail = Tuple.Create(DA.vendor_detail_display(vendorName));

                DataTable custDetail = DA.GetCustomerDetailsByName(CustomerName).Tables["customer_detail"];

                cus.Name = custDetail.Rows[0].ItemArray[0].ToString();

                //vd.Name = vendorDetail.Item1.Item1.ToString();
                cus.Address = custDetail.Rows[0].ItemArray[1].ToString();
                cus.ContactNo = custDetail.Rows[0].ItemArray[2].ToString();//error


                txtNameVendor.Text = cus.Name;
                txtPhoneNomaskedVendor.Text = cus.ContactNo;
                txtAddressVendor.Text = cus.Address;
                



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btwBack_Click(object sender, EventArgs e)
        {
            OpeningTheme obj2 = new OpeningTheme();
            //this.Close();
            this.Hide();
            obj2.Show();
        }

        private void btwCustomerBillCustomer_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login login = new Login("CustomerBill", cus);
            login.Show();
        }

        private void btwCustomerList_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerList CL = new CustomerList();
            CL.Show();
        }

        private void txtNameVendor_TextChanged(object sender, EventArgs e)
        {
            FormConfig(f);
        }

        private void btwAddCustomer_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                cus.Name = txtNameVendor.Text;
                cus.Address = txtAddressVendor.Text;
                cus.ContactNo = txtPhoneNomaskedVendor.Text;
                //long contact = Convert.ToInt32(cus.ContactNo);
                DA.customer_detail_insert(cus.Name, cus.Address, cus.ContactNo);
                btwCustomerBillCustomer.Enabled = true;
                CustomerName = cus.Name;
                FormConfig(1);
            }
            else
            {
                MessageBox.Show("PLease Enter Name");
                txtNameVendor.Focus();
            }
        }

        private void ordersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //this.Hide();
            //CustomerOrder VO = new CustomerOrder();
            //VO.Show();
        }

        private void customerAddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerMainPage cust = new CustomerMainPage();
            cust.Show();
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerList VL = new CustomerList();
            VL.Show();
        }

        private void btwDeleteCustomer_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerUpdate_Delete cud = new CustomerUpdate_Delete();
            cud.Show();
        }

        private void mAINMENUToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            OpeningTheme OT = new OpeningTheme();
            OT.Show();
        }

        private void vendorsNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VendorsOrder VO = new VendorsOrder();
            VO.Show();
        }

        private void ordersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Vendor vendor = new Vendor();
            vendor.Show();
        }

        private void vendorsNameToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            VendorList VL = new VendorList();
            VL.Show();
        }

        

        
    }
}
