﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class CustomerList : Form
    {
        DataAccessClass DA = new DataAccessClass();
        public CustomerList()
        {
            InitializeComponent();
        }
        private void CustomerList_Load(object sender, EventArgs e)
        {
            try
            {
                DGCustomerList.DataSource = DA.customer_detail_display().Tables["customer_detail"];
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        private void btwBackCustomerList_Click(object sender, EventArgs e)
        {
            //MainPage obj = new MainPage();
            //this.Close();

            this.Hide();
            CustomerMainPage obj = new CustomerMainPage();
            obj.Show();
        }

        private void btwAddNewBill_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearchCustomerList.Text))
            {
                string name = txtSearchCustomerList.Text;
                this.Hide();
                CustomerMainPage CM = new CustomerMainPage(name);
                CM.Show();
            }
            
            
            //this.Hide();
            //Bill bill = new Bill();
            //bill.Show();
        }

        private void btwSearchCustomerList_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtSearchCustomerList.Text))
            {
                string custName = txtSearchCustomerList.Text;
                DGCustomerList.DataSource = DA.GetCustomerDetailsByName(custName).Tables["customer_detail"];

            }
            else
            {
                DGCustomerList.DataSource = DA.customer_detail_display().Tables["customer_detail"];
            }
        }

        private void customerListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerList CL = new CustomerList();
            CL.Show();
        }

        private void mAINMENUToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            OpeningTheme OT = new OpeningTheme();
            OT.Show();
        }

        private void btwOrders_Click(object sender, EventArgs e)
        {
            this.Hide();
            Orders O = new Orders();
            O.Show();
        }

        
    }
}
