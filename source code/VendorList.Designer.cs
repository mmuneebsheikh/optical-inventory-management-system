﻿namespace VisualProjectOpInventory
{
    partial class VendorList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btwBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btwShowVendor = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtVendorSearch = new System.Windows.Forms.TextBox();
            this.btwSearchVendorList = new System.Windows.Forms.Button();
            this.DGVendorList = new System.Windows.Forms.DataGridView();
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerRemoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerPaymentStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monthlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yearlySalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicalDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickCustomerStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toAddVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toCheckVendorsStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clickVendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.clickStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsStatusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.goToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsOrdersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVendorList)).BeginInit();
            this.MainmenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btwShowVendor);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.DGVendorList);
            this.panel1.Location = new System.Drawing.Point(17, 74);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(934, 494);
            this.panel1.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btwBack);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(0, 28);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(932, 81);
            this.panel2.TabIndex = 6;
            // 
            // btwBack
            // 
            this.btwBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBack.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBack.Location = new System.Drawing.Point(813, 2);
            this.btwBack.Margin = new System.Windows.Forms.Padding(2);
            this.btwBack.Name = "btwBack";
            this.btwBack.Size = new System.Drawing.Size(117, 40);
            this.btwBack.TabIndex = 2;
            this.btwBack.Text = "Add Vendor";
            this.btwBack.UseVisualStyleBackColor = true;
            this.btwBack.Click += new System.EventHandler(this.btwBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(414, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vendor List";
            // 
            // btwShowVendor
            // 
            this.btwShowVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwShowVendor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwShowVendor.Location = new System.Drawing.Point(813, 113);
            this.btwShowVendor.Margin = new System.Windows.Forms.Padding(2);
            this.btwShowVendor.Name = "btwShowVendor";
            this.btwShowVendor.Size = new System.Drawing.Size(117, 64);
            this.btwShowVendor.TabIndex = 7;
            this.btwShowVendor.Text = "Show Vendor";
            this.btwShowVendor.UseVisualStyleBackColor = true;
            this.btwShowVendor.Click += new System.EventHandler(this.btwShowVendor_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.txtVendorSearch);
            this.panel4.Controls.Add(this.btwSearchVendorList);
            this.panel4.Location = new System.Drawing.Point(2, 114);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(806, 63);
            this.panel4.TabIndex = 4;
            // 
            // txtVendorSearch
            // 
            this.txtVendorSearch.Location = new System.Drawing.Point(0, 0);
            this.txtVendorSearch.Margin = new System.Windows.Forms.Padding(2);
            this.txtVendorSearch.Name = "txtVendorSearch";
            this.txtVendorSearch.Size = new System.Drawing.Size(753, 20);
            this.txtVendorSearch.TabIndex = 4;
            // 
            // btwSearchVendorList
            // 
            this.btwSearchVendorList.Font = new System.Drawing.Font("Lucida Sans Unicode", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwSearchVendorList.Location = new System.Drawing.Point(751, 0);
            this.btwSearchVendorList.Margin = new System.Windows.Forms.Padding(2);
            this.btwSearchVendorList.Name = "btwSearchVendorList";
            this.btwSearchVendorList.Size = new System.Drawing.Size(56, 22);
            this.btwSearchVendorList.TabIndex = 3;
            this.btwSearchVendorList.Text = "Go";
            this.btwSearchVendorList.UseVisualStyleBackColor = true;
            this.btwSearchVendorList.Click += new System.EventHandler(this.btwSearchVendorList_Click);
            // 
            // DGVendorList
            // 
            this.DGVendorList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVendorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVendorList.Location = new System.Drawing.Point(9, 182);
            this.DGVendorList.Margin = new System.Windows.Forms.Padding(2);
            this.DGVendorList.Name = "DGVendorList";
            this.DGVendorList.RowTemplate.Height = 24;
            this.DGVendorList.Size = new System.Drawing.Size(916, 312);
            this.DGVendorList.TabIndex = 9;
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.goToToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.MainmenuStrip1.Size = new System.Drawing.Size(962, 25);
            this.MainmenuStrip1.TabIndex = 4;
            this.MainmenuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            this.vendorsNameToolStripMenuItem.Click += new System.EventHandler(this.MenuStripVendorOrders_clicked);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ordersToolStripMenuItem.Text = "New Vendor";
            this.ordersToolStripMenuItem.Click += new System.EventHandler(this.MenuStripNewVendor_clicked);
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem1.Text = "Vendor List";
            this.vendorsNameToolStripMenuItem1.Click += new System.EventHandler(this.MenuStripVendorList);
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem2,
            this.ordersToolStripMenuItem1,
            this.customerNameToolStripMenuItem,
            this.customerAddToolStripMenuItem,
            this.customerRemoveToolStripMenuItem,
            this.customerUpdateToolStripMenuItem,
            this.customerStatusToolStripMenuItem,
            this.customerPaymentStatusToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(88, 21);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // customerToolStripMenuItem2
            // 
            this.customerToolStripMenuItem2.Name = "customerToolStripMenuItem2";
            this.customerToolStripMenuItem2.Size = new System.Drawing.Size(252, 22);
            this.customerToolStripMenuItem2.Text = "CustomerPage";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.ordersToolStripMenuItem1.Text = "Orders";
            // 
            // customerNameToolStripMenuItem
            // 
            this.customerNameToolStripMenuItem.Name = "customerNameToolStripMenuItem";
            this.customerNameToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerNameToolStripMenuItem.Text = "CustomerName";
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerAddToolStripMenuItem.Text = "CustomerAdd";
            // 
            // customerRemoveToolStripMenuItem
            // 
            this.customerRemoveToolStripMenuItem.Name = "customerRemoveToolStripMenuItem";
            this.customerRemoveToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerRemoveToolStripMenuItem.Text = "CustomerRemove";
            // 
            // customerUpdateToolStripMenuItem
            // 
            this.customerUpdateToolStripMenuItem.Name = "customerUpdateToolStripMenuItem";
            this.customerUpdateToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerUpdateToolStripMenuItem.Text = "CustomerUpdate";
            // 
            // customerStatusToolStripMenuItem
            // 
            this.customerStatusToolStripMenuItem.Name = "customerStatusToolStripMenuItem";
            this.customerStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerStatusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // customerPaymentStatusToolStripMenuItem
            // 
            this.customerPaymentStatusToolStripMenuItem.Name = "customerPaymentStatusToolStripMenuItem";
            this.customerPaymentStatusToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.customerPaymentStatusToolStripMenuItem.Text = "CustomerPaymentStatus";
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monthlySalesToolStripMenuItem,
            this.yearlySalesToolStripMenuItem,
            this.profitsToolStripMenuItem,
            this.lossToolStripMenuItem,
            this.graphicalDataToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // monthlySalesToolStripMenuItem
            // 
            this.monthlySalesToolStripMenuItem.Name = "monthlySalesToolStripMenuItem";
            this.monthlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.monthlySalesToolStripMenuItem.Text = "MonthlySales";
            // 
            // yearlySalesToolStripMenuItem
            // 
            this.yearlySalesToolStripMenuItem.Name = "yearlySalesToolStripMenuItem";
            this.yearlySalesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.yearlySalesToolStripMenuItem.Text = "YearlySales";
            // 
            // profitsToolStripMenuItem
            // 
            this.profitsToolStripMenuItem.Name = "profitsToolStripMenuItem";
            this.profitsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.profitsToolStripMenuItem.Text = "Profits";
            // 
            // lossToolStripMenuItem
            // 
            this.lossToolStripMenuItem.Name = "lossToolStripMenuItem";
            this.lossToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lossToolStripMenuItem.Text = "Loss";
            // 
            // graphicalDataToolStripMenuItem
            // 
            this.graphicalDataToolStripMenuItem.Name = "graphicalDataToolStripMenuItem";
            this.graphicalDataToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.graphicalDataToolStripMenuItem.Text = "GraphicalData";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toAddCustomerToolStripMenuItem,
            this.toCheckCustomerStatusToolStripMenuItem,
            this.toAddVendorsToolStripMenuItem,
            this.toCheckVendorsStatusToolStripMenuItem,
            this.paymentsToolStripMenuItem,
            this.ordersToolStripMenuItem2});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 21);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // toAddCustomerToolStripMenuItem
            // 
            this.toAddCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem3});
            this.toAddCustomerToolStripMenuItem.Name = "toAddCustomerToolStripMenuItem";
            this.toAddCustomerToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.toAddCustomerToolStripMenuItem.Text = "ToAddCustomer";
            // 
            // customerToolStripMenuItem3
            // 
            this.customerToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem});
            this.customerToolStripMenuItem3.Name = "customerToolStripMenuItem3";
            this.customerToolStripMenuItem3.Size = new System.Drawing.Size(178, 22);
            this.customerToolStripMenuItem3.Text = "ClickCustomer";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.addToolStripMenuItem.Text = "ClickCustomerAdd";
            // 
            // toCheckCustomerStatusToolStripMenuItem
            // 
            this.toCheckCustomerStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerToolStripMenuItem});
            this.toCheckCustomerStatusToolStripMenuItem.Name = "toCheckCustomerStatusToolStripMenuItem";
            this.toCheckCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.toCheckCustomerStatusToolStripMenuItem.Text = "ToCheckCustomerStatus";
            // 
            // clickCustomerToolStripMenuItem
            // 
            this.clickCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickCustomerStatusToolStripMenuItem});
            this.clickCustomerToolStripMenuItem.Name = "clickCustomerToolStripMenuItem";
            this.clickCustomerToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.clickCustomerToolStripMenuItem.Text = "ClickCustomer";
            // 
            // clickCustomerStatusToolStripMenuItem
            // 
            this.clickCustomerStatusToolStripMenuItem.Name = "clickCustomerStatusToolStripMenuItem";
            this.clickCustomerStatusToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.clickCustomerStatusToolStripMenuItem.Text = "ClickCustomerStatus";
            // 
            // toAddVendorsToolStripMenuItem
            // 
            this.toAddVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem});
            this.toAddVendorsToolStripMenuItem.Name = "toAddVendorsToolStripMenuItem";
            this.toAddVendorsToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.toAddVendorsToolStripMenuItem.Text = "ToAddVendors";
            // 
            // clickVendorsToolStripMenuItem
            // 
            this.clickVendorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorAddToolStripMenuItem});
            this.clickVendorsToolStripMenuItem.Name = "clickVendorsToolStripMenuItem";
            this.clickVendorsToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.clickVendorsToolStripMenuItem.Text = "ClickVendors";
            // 
            // clickVendorAddToolStripMenuItem
            // 
            this.clickVendorAddToolStripMenuItem.Name = "clickVendorAddToolStripMenuItem";
            this.clickVendorAddToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.clickVendorAddToolStripMenuItem.Text = "ClickVendorAdd";
            // 
            // toCheckVendorsStatusToolStripMenuItem
            // 
            this.toCheckVendorsStatusToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickVendorsToolStripMenuItem1});
            this.toCheckVendorsStatusToolStripMenuItem.Name = "toCheckVendorsStatusToolStripMenuItem";
            this.toCheckVendorsStatusToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.toCheckVendorsStatusToolStripMenuItem.Text = "ToCheckVendorsStatus";
            // 
            // clickVendorsToolStripMenuItem1
            // 
            this.clickVendorsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clickStatusToolStripMenuItem});
            this.clickVendorsToolStripMenuItem1.Name = "clickVendorsToolStripMenuItem1";
            this.clickVendorsToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.clickVendorsToolStripMenuItem1.Text = "ClickVendors";
            // 
            // clickStatusToolStripMenuItem
            // 
            this.clickStatusToolStripMenuItem.Name = "clickStatusToolStripMenuItem";
            this.clickStatusToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.clickStatusToolStripMenuItem.Text = "ClickStatus";
            // 
            // paymentsToolStripMenuItem
            // 
            this.paymentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem4,
            this.vendorsToolStripMenuItem});
            this.paymentsToolStripMenuItem.Name = "paymentsToolStripMenuItem";
            this.paymentsToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.paymentsToolStripMenuItem.Text = "Payments";
            // 
            // customerToolStripMenuItem4
            // 
            this.customerToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusToolStripMenuItem,
            this.vendorsStatusToolStripMenuItem1});
            this.customerToolStripMenuItem4.Name = "customerToolStripMenuItem4";
            this.customerToolStripMenuItem4.Size = new System.Drawing.Size(144, 22);
            this.customerToolStripMenuItem4.Text = "Customer";
            // 
            // statusToolStripMenuItem
            // 
            this.statusToolStripMenuItem.Name = "statusToolStripMenuItem";
            this.statusToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.statusToolStripMenuItem.Text = "CustomerStatus";
            // 
            // vendorsStatusToolStripMenuItem1
            // 
            this.vendorsStatusToolStripMenuItem1.Name = "vendorsStatusToolStripMenuItem1";
            this.vendorsStatusToolStripMenuItem1.Size = new System.Drawing.Size(190, 22);
            this.vendorsStatusToolStripMenuItem1.Text = "VendorsStatus";
            // 
            // vendorsToolStripMenuItem
            // 
            this.vendorsToolStripMenuItem.Name = "vendorsToolStripMenuItem";
            this.vendorsToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.vendorsToolStripMenuItem.Text = "Vendors";
            // 
            // ordersToolStripMenuItem2
            // 
            this.ordersToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem5,
            this.vendorsToolStripMenuItem1});
            this.ordersToolStripMenuItem2.Name = "ordersToolStripMenuItem2";
            this.ordersToolStripMenuItem2.Size = new System.Drawing.Size(253, 22);
            this.ordersToolStripMenuItem2.Text = "Orders";
            // 
            // customerToolStripMenuItem5
            // 
            this.customerToolStripMenuItem5.Name = "customerToolStripMenuItem5";
            this.customerToolStripMenuItem5.Size = new System.Drawing.Size(144, 22);
            this.customerToolStripMenuItem5.Text = "Customer";
            // 
            // vendorsToolStripMenuItem1
            // 
            this.vendorsToolStripMenuItem1.Name = "vendorsToolStripMenuItem1";
            this.vendorsToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.vendorsToolStripMenuItem1.Text = "Vendors";
            // 
            // goToToolStripMenuItem
            // 
            this.goToToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem6,
            this.vendorsToolStripMenuItem2,
            this.salesToolStripMenuItem1,
            this.ordersToolStripMenuItem3,
            this.paymentsToolStripMenuItem1,
            this.statusToolStripMenuItem1,
            this.loginToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.goToToolStripMenuItem.Name = "goToToolStripMenuItem";
            this.goToToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.goToToolStripMenuItem.Text = "GoTo";
            // 
            // customerToolStripMenuItem6
            // 
            this.customerToolStripMenuItem6.Name = "customerToolStripMenuItem6";
            this.customerToolStripMenuItem6.Size = new System.Drawing.Size(146, 22);
            this.customerToolStripMenuItem6.Text = "Customer";
            // 
            // vendorsToolStripMenuItem2
            // 
            this.vendorsToolStripMenuItem2.Name = "vendorsToolStripMenuItem2";
            this.vendorsToolStripMenuItem2.Size = new System.Drawing.Size(146, 22);
            this.vendorsToolStripMenuItem2.Text = "Vendors";
            // 
            // salesToolStripMenuItem1
            // 
            this.salesToolStripMenuItem1.Name = "salesToolStripMenuItem1";
            this.salesToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.salesToolStripMenuItem1.Text = "Sales";
            // 
            // ordersToolStripMenuItem3
            // 
            this.ordersToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerOrdersToolStripMenuItem,
            this.vendorsOrdersToolStripMenuItem});
            this.ordersToolStripMenuItem3.Name = "ordersToolStripMenuItem3";
            this.ordersToolStripMenuItem3.Size = new System.Drawing.Size(146, 22);
            this.ordersToolStripMenuItem3.Text = "Orders";
            // 
            // customerOrdersToolStripMenuItem
            // 
            this.customerOrdersToolStripMenuItem.Name = "customerOrdersToolStripMenuItem";
            this.customerOrdersToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.customerOrdersToolStripMenuItem.Text = "CustomerOrders";
            // 
            // vendorsOrdersToolStripMenuItem
            // 
            this.vendorsOrdersToolStripMenuItem.Name = "vendorsOrdersToolStripMenuItem";
            this.vendorsOrdersToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.vendorsOrdersToolStripMenuItem.Text = "VendorsOrders";
            // 
            // paymentsToolStripMenuItem1
            // 
            this.paymentsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem7,
            this.vendorsToolStripMenuItem3});
            this.paymentsToolStripMenuItem1.Name = "paymentsToolStripMenuItem1";
            this.paymentsToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.paymentsToolStripMenuItem1.Text = "Payments";
            // 
            // customerToolStripMenuItem7
            // 
            this.customerToolStripMenuItem7.Name = "customerToolStripMenuItem7";
            this.customerToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.customerToolStripMenuItem7.Text = "Customers";
            // 
            // vendorsToolStripMenuItem3
            // 
            this.vendorsToolStripMenuItem3.Name = "vendorsToolStripMenuItem3";
            this.vendorsToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.vendorsToolStripMenuItem3.Text = "Vendors";
            // 
            // statusToolStripMenuItem1
            // 
            this.statusToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem8,
            this.vendorsToolStripMenuItem4});
            this.statusToolStripMenuItem1.Name = "statusToolStripMenuItem1";
            this.statusToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.statusToolStripMenuItem1.Text = "Status";
            // 
            // customerToolStripMenuItem8
            // 
            this.customerToolStripMenuItem8.Name = "customerToolStripMenuItem8";
            this.customerToolStripMenuItem8.Size = new System.Drawing.Size(152, 22);
            this.customerToolStripMenuItem8.Text = "Customers";
            // 
            // vendorsToolStripMenuItem4
            // 
            this.vendorsToolStripMenuItem4.Name = "vendorsToolStripMenuItem4";
            this.vendorsToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.vendorsToolStripMenuItem4.Text = "Vendors";
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.loginToolStripMenuItem.Text = "Login";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // VendorList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(962, 579);
            this.Controls.Add(this.MainmenuStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "VendorList";
            this.Text = "VendorList";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.VendorList_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVendorList)).EndInit();
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btwBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btwShowVendor;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtVendorSearch;
        private System.Windows.Forms.Button btwSearchVendorList;
        private System.Windows.Forms.DataGridView DGVendorList;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRemoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerPaymentStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monthlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yearlySalesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicalDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickCustomerStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toAddVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toCheckVendorsStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clickVendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem clickStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsStatusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem goToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem customerOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsOrdersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem statusToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem vendorsToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;

    }
}