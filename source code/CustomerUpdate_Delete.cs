﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    public partial class CustomerUpdate_Delete : Form
    {
        DataAccessClass DA = new DataAccessClass();
        CustomerClass cus = new CustomerClass();
        string CustomerName;
        public CustomerUpdate_Delete()
        {
            InitializeComponent();
        }

        private void btwDeleteCustomer_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                string name = txtNameVendor.Text;
                cus.Name = txtnewname.Text;
                cus.Address = txtAddressVendor.Text;
                cus.ContactNo = txtPhoneNomaskedVendor.Text;
                CustomerName = cus.Name;
                DA.Delete_CustomerDetailByName(name);


            }
            else
            {
                MessageBox.Show("PLease Enter Name");
                txtNameVendor.Focus();
            }
            this.Hide();
            MainPage mp = new MainPage();
            mp.Show();
            
        }

        private void btwUpdateCustomer_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtNameVendor.Text))
            {
                string name = txtNameVendor.Text;
                cus.Name = txtnewname.Text;
                cus.Address = txtAddressVendor.Text;
                cus.ContactNo = txtPhoneNomaskedVendor.Text;
                DA.Update_CustomerDetailByName(cus.Name, cus.Address, cus.ContactNo,name);

            }
            else
            {
                MessageBox.Show("PLease Enter Name");
                txtNameVendor.Focus();
            }

            this.Hide();
            MainPage mp = new MainPage();
            mp.Show();
        }

        private void btwBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerMainPage CM = new CustomerMainPage();
            CM.Show();
        }

        private void txtnewname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPhoneNomaskedVendor_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        
    }
}
