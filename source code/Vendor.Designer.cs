﻿namespace VisualProjectOpInventory
{
    partial class Vendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vendor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btwBack = new System.Windows.Forms.Button();
            this.btwVendorList = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btwAddVendor = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DGBills = new System.Windows.Forms.DataGridView();
            this.btwVendorBillVendor = new System.Windows.Forms.Button();
            this.txtBalanceVendor = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCreditVendor = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPhoneNomaskedVendor = new System.Windows.Forms.MaskedTextBox();
            this.txtNameVendor = new System.Windows.Forms.TextBox();
            this.txtAddressVendor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.MainmenuStrip1 = new System.Windows.Forms.MenuStrip();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorsNameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mAINMENUToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGBills)).BeginInit();
            this.panel2.SuspendLayout();
            this.MainmenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.btwBack);
            this.panel1.Controls.Add(this.btwVendorList);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(32, 36);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(860, 669);
            this.panel1.TabIndex = 0;
            // 
            // btwBack
            // 
            this.btwBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwBack.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwBack.Location = new System.Drawing.Point(4, 8);
            this.btwBack.Margin = new System.Windows.Forms.Padding(2);
            this.btwBack.Name = "btwBack";
            this.btwBack.Size = new System.Drawing.Size(79, 28);
            this.btwBack.TabIndex = 57;
            this.btwBack.Text = "<--";
            this.btwBack.UseVisualStyleBackColor = true;
            this.btwBack.Click += new System.EventHandler(this.btwBack_Click_1);
            // 
            // btwVendorList
            // 
            this.btwVendorList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwVendorList.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwVendorList.Location = new System.Drawing.Point(589, 10);
            this.btwVendorList.Margin = new System.Windows.Forms.Padding(2);
            this.btwVendorList.Name = "btwVendorList";
            this.btwVendorList.Size = new System.Drawing.Size(137, 28);
            this.btwVendorList.TabIndex = 56;
            this.btwVendorList.Text = "Vendor List";
            this.btwVendorList.UseVisualStyleBackColor = true;
            this.btwVendorList.Click += new System.EventHandler(this.btwVendorList_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btwAddVendor);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.btwVendorBillVendor);
            this.panel3.Controls.Add(this.txtBalanceVendor);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.txtCreditVendor);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.txtPhoneNomaskedVendor);
            this.panel3.Controls.Add(this.txtNameVendor);
            this.panel3.Controls.Add(this.txtAddressVendor);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.Location = new System.Drawing.Point(4, 43);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(874, 631);
            this.panel3.TabIndex = 1;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // btwAddVendor
            // 
            this.btwAddVendor.Enabled = false;
            this.btwAddVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwAddVendor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwAddVendor.Location = new System.Drawing.Point(733, 184);
            this.btwAddVendor.Margin = new System.Windows.Forms.Padding(2);
            this.btwAddVendor.Name = "btwAddVendor";
            this.btwAddVendor.Size = new System.Drawing.Size(106, 50);
            this.btwAddVendor.TabIndex = 61;
            this.btwAddVendor.Text = "AddVendor";
            this.btwAddVendor.UseVisualStyleBackColor = true;
            this.btwAddVendor.Click += new System.EventHandler(this.btwAddVendor_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DGBills);
            this.groupBox1.Location = new System.Drawing.Point(20, 206);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(836, 415);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bills";
            // 
            // DGBills
            // 
            this.DGBills.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGBills.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGBills.Location = new System.Drawing.Point(4, 36);
            this.DGBills.Margin = new System.Windows.Forms.Padding(2);
            this.DGBills.Name = "DGBills";
            this.DGBills.RowTemplate.Height = 24;
            this.DGBills.Size = new System.Drawing.Size(828, 417);
            this.DGBills.TabIndex = 0;
            // 
            // btwVendorBillVendor
            // 
            this.btwVendorBillVendor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btwVendorBillVendor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btwVendorBillVendor.Location = new System.Drawing.Point(735, 130);
            this.btwVendorBillVendor.Margin = new System.Windows.Forms.Padding(2);
            this.btwVendorBillVendor.Name = "btwVendorBillVendor";
            this.btwVendorBillVendor.Size = new System.Drawing.Size(106, 50);
            this.btwVendorBillVendor.TabIndex = 59;
            this.btwVendorBillVendor.Text = "New Bill";
            this.btwVendorBillVendor.UseVisualStyleBackColor = true;
            this.btwVendorBillVendor.Click += new System.EventHandler(this.btwVendorBillVendor_Click);
            // 
            // txtBalanceVendor
            // 
            this.txtBalanceVendor.Location = new System.Drawing.Point(627, 96);
            this.txtBalanceVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtBalanceVendor.Name = "txtBalanceVendor";
            this.txtBalanceVendor.Size = new System.Drawing.Size(216, 23);
            this.txtBalanceVendor.TabIndex = 58;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label16.Location = new System.Drawing.Point(514, 89);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 29);
            this.label16.TabIndex = 57;
            this.label16.Text = "Balance:";
            // 
            // txtCreditVendor
            // 
            this.txtCreditVendor.Location = new System.Drawing.Point(627, 59);
            this.txtCreditVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreditVendor.Name = "txtCreditVendor";
            this.txtCreditVendor.Size = new System.Drawing.Size(216, 23);
            this.txtCreditVendor.TabIndex = 56;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label17.Location = new System.Drawing.Point(514, 59);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 29);
            this.label17.TabIndex = 55;
            this.label17.Text = "Credit:";
            // 
            // txtPhoneNomaskedVendor
            // 
            this.txtPhoneNomaskedVendor.Location = new System.Drawing.Point(157, 65);
            this.txtPhoneNomaskedVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNomaskedVendor.Name = "txtPhoneNomaskedVendor";
            this.txtPhoneNomaskedVendor.Size = new System.Drawing.Size(243, 23);
            this.txtPhoneNomaskedVendor.TabIndex = 6;
            // 
            // txtNameVendor
            // 
            this.txtNameVendor.Location = new System.Drawing.Point(157, 31);
            this.txtNameVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtNameVendor.Name = "txtNameVendor";
            this.txtNameVendor.Size = new System.Drawing.Size(243, 23);
            this.txtNameVendor.TabIndex = 9;
            this.txtNameVendor.TextChanged += new System.EventHandler(this.txtNameVendor_TextChanged);
            // 
            // txtAddressVendor
            // 
            this.txtAddressVendor.Location = new System.Drawing.Point(157, 103);
            this.txtAddressVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddressVendor.Multiline = true;
            this.txtAddressVendor.Name = "txtAddressVendor";
            this.txtAddressVendor.Size = new System.Drawing.Size(243, 73);
            this.txtAddressVendor.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(20, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 29);
            this.label3.TabIndex = 8;
            this.label3.Text = "Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(20, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Phone No:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(20, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Name:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(302, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 36);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(90, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vendor";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainmenuStrip1
            // 
            this.MainmenuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.MainmenuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainmenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainmenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem,
            this.customerToolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.mAINMENUToolStripMenuItem});
            this.MainmenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.MainmenuStrip1.Name = "MainmenuStrip1";
            this.MainmenuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.MainmenuStrip1.Size = new System.Drawing.Size(921, 25);
            this.MainmenuStrip1.TabIndex = 3;
            this.MainmenuStrip1.Text = "menuStrip1";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vendorsNameToolStripMenuItem,
            this.ordersToolStripMenuItem,
            this.vendorsNameToolStripMenuItem1});
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.customerToolStripMenuItem.Text = "Vendors";
            // 
            // vendorsNameToolStripMenuItem
            // 
            this.vendorsNameToolStripMenuItem.Name = "vendorsNameToolStripMenuItem";
            this.vendorsNameToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem.Text = "Orders";
            this.vendorsNameToolStripMenuItem.Click += new System.EventHandler(this.MenuStripVendorOrders_clicked);
            // 
            // ordersToolStripMenuItem
            // 
            this.ordersToolStripMenuItem.Name = "ordersToolStripMenuItem";
            this.ordersToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ordersToolStripMenuItem.Text = "New Vendor";
            this.ordersToolStripMenuItem.Click += new System.EventHandler(this.MenuStripNewVendor_clicked);
            // 
            // vendorsNameToolStripMenuItem1
            // 
            this.vendorsNameToolStripMenuItem1.Name = "vendorsNameToolStripMenuItem1";
            this.vendorsNameToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.vendorsNameToolStripMenuItem1.Text = "Vendor List";
            this.vendorsNameToolStripMenuItem1.Click += new System.EventHandler(this.MenuStripVendorList);
            // 
            // customerToolStripMenuItem1
            // 
            this.customerToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordersToolStripMenuItem1,
            this.customerAddToolStripMenuItem,
            this.customerListToolStripMenuItem});
            this.customerToolStripMenuItem1.Name = "customerToolStripMenuItem1";
            this.customerToolStripMenuItem1.Size = new System.Drawing.Size(88, 21);
            this.customerToolStripMenuItem1.Text = "Customer";
            // 
            // ordersToolStripMenuItem1
            // 
            this.ordersToolStripMenuItem1.Name = "ordersToolStripMenuItem1";
            this.ordersToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.ordersToolStripMenuItem1.Text = "Orders";
            this.ordersToolStripMenuItem1.Click += new System.EventHandler(this.ordersToolStripMenuItem1_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.salesToolStripMenuItem.Text = "Sales";
            this.salesToolStripMenuItem.Click += new System.EventHandler(this.salesToolStripMenuItem_Click);
            // 
            // mAINMENUToolStripMenuItem
            // 
            this.mAINMENUToolStripMenuItem.Name = "mAINMENUToolStripMenuItem";
            this.mAINMENUToolStripMenuItem.Size = new System.Drawing.Size(106, 21);
            this.mAINMENUToolStripMenuItem.Text = "MAIN MENU";
            this.mAINMENUToolStripMenuItem.Click += new System.EventHandler(this.MenuStripMainMenu);
            // 
            // customerAddToolStripMenuItem
            // 
            this.customerAddToolStripMenuItem.Name = "customerAddToolStripMenuItem";
            this.customerAddToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.customerAddToolStripMenuItem.Text = "Customer Add";
            this.customerAddToolStripMenuItem.Click += new System.EventHandler(this.customerAddToolStripMenuItem_Click_1);
            // 
            // customerListToolStripMenuItem
            // 
            this.customerListToolStripMenuItem.Name = "customerListToolStripMenuItem";
            this.customerListToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.customerListToolStripMenuItem.Text = "Customer  List";
            this.customerListToolStripMenuItem.Click += new System.EventHandler(this.customerListToolStripMenuItem_Click);
            // 
            // Vendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(921, 628);
            this.Controls.Add(this.MainmenuStrip1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Vendor";
            this.Text = "Vendor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Vendor_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGBills)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.MainmenuStrip1.ResumeLayout(false);
            this.MainmenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox txtPhoneNomaskedVendor;
        private System.Windows.Forms.TextBox txtNameVendor;
        private System.Windows.Forms.TextBox txtAddressVendor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btwVendorBillVendor;
        private System.Windows.Forms.TextBox txtBalanceVendor;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCreditVendor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DGBills;
        private System.Windows.Forms.Button btwVendorList;
        private System.Windows.Forms.Button btwAddVendor;
        private System.Windows.Forms.MenuStrip MainmenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorsNameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ordersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mAINMENUToolStripMenuItem;
        private System.Windows.Forms.Button btwBack;
        private System.Windows.Forms.ToolStripMenuItem customerAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerListToolStripMenuItem;

    }
}