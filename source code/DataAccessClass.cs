﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace VisualProjectOpInventory
{
    class DataAccessClass
    {


        SqlConnection con = new SqlConnection(@"Data Source=MUNEEB-PC;Initial Catalog=inventorySystemDB;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        SqlDataAdapter adp;
        DataSet ds;
        SqlDataReader dr;
       
        //
        //
        //Users
        //
        //

        public DataSet GetAllUserNames()
        {
            try
            {
                ds = new DataSet();
               if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@" SELECT u.u_userName From Users u ", con))
                    adp.Fill(ds, "user");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public bool VarifyUser(string userName, string Pass)
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@" SELECT COUNT (u.u_userName) From Users u where u.u_userName = '"+userName+"' AND u.u_password = '"+Pass+"'", con))
                    adp.Fill(ds, "user");

                con.Close();
                if (ds.Tables["user"].Rows[0].ItemArray[0].ToString() == "0")
                {
                    return false;
                }
                else return true;
                //return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }


        public string GetUserName(string userName, string pass)
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@" SELECT u.u_userName From Users u where u.u_userName = '" + userName + "' AND u.u_password = '" + pass + "'", con))
                    adp.Fill(ds, "user");

                con.Close();

                string un = ds.Tables["user"].Rows[0].ItemArray[0].ToString();
                return un;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public void user_insert(string u_userName, string u_password)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(u_userName))
                {
                    MessageBox.Show("enter Username please");
                }
                else
                {
                    String query = "INSERT INTO Users(u_userName,u_password)VALUES(@u_userName,@u_password)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@u_userName", u_userName);
                        cmd.Parameters.AddWithValue("@u_password", u_password);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void user_update(string name,string u_userName, string u_password)
        {
            try
            {
                if (name==null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "Update Users(u_userName = @u_userName,u_password = @u_password where u_userName = @user";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@user", name);
                        cmd.Parameters.AddWithValue("@u_userName", u_userName);
                        cmd.Parameters.AddWithValue("@u_password", u_password);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not updated");
                        else
                            MessageBox.Show("Record updated!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void user_delete(string name)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "delete from Users where u_userName = @user";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.Add("@user", name);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not delete");
                        else
                            MessageBox.Show("Record delete!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        //
        //
        //Vendor
        //
        //
        public void vendor_detail_insert(string name, string address, long phno, double credit, double blnc)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "INSERT INTO vendor_detail(vd_name,vd_address,vd_phoneno,vd_credit,vd_balance)VALUES(@name,@address,@phno,@credit,@blnc)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@address", address);
                        cmd.Parameters.AddWithValue("@phno", phno);
                        cmd.Parameters.AddWithValue("@credit", credit);
                        cmd.Parameters.AddWithValue("@blnc", blnc);

                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();                            
                        }

                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void vendor_detail_update(string con_name, string name, string address, int phno, double credit, double blnc)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "UPDATE vendor_detail SET vd_name = @name, vd_address = @address, vd_phoneno = @phno, vd_credit = @credit, vd_balance = @blnc where vd_name = '" + con_name + "')";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@address", address);
                        cmd.Parameters.AddWithValue("@phno", phno);
                        cmd.Parameters.AddWithValue("@credit", credit);
                        cmd.Parameters.AddWithValue("@blnc", blnc);


                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void vendor_bill_insert(string u_name, string vd_name, DateTime orderdate, DateTime duedate, string status, double total_amount, double amount_paid, double blnc)
        {
            try
            {
                if (vd_name == null)
                {
                    MessageBox.Show("enter Name please");
                }
                else//bill status ambiguity
                {
                    int vendorID, userID;
                    //string q1 ="select vd_id from vendor_detail where vd_name = '" + vd_name + "'";
                    cmd = new SqlCommand("select vd_id from vendor_detail where vd_name = '" + vd_name + "'", con);
                    con.Open();

                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    using (dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        vendorID = dr.GetInt32(0);
                        //textBox3.Text = dr.GetString(2);


                    }
                    con.Close();
                    cmd = new SqlCommand("select u_id from Users where u_userName = '" + u_name + "'", con);
                    con.Open();

                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    using (dr = cmd.ExecuteReader())
                    {
                        dr.Read();

                        userID = dr.GetInt32(0);
                        //textBox3.Text = dr.GetString(2);



                    }
                    con.Close();


                    //insert block
                    string query = "INSERT INTO vendor_bill(vb_orderDate,vb_duedate,vb_status,vb_totalamount,vb_amountpaid,vb_balance,vd_id,u_id)VALUES(@vb_orderDate,@vb_duedate,@vb_status,@vb_totalamount,@vb_amountpaid,@vb_balance, @vd_id,@u_id)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@vb_orderDate", orderdate);
                        cmd.Parameters.AddWithValue("@vb_duedate", duedate);
                        cmd.Parameters.AddWithValue("@vb_status", status);
                        cmd.Parameters.AddWithValue("@vb_totalamount", total_amount);
                        cmd.Parameters.AddWithValue("@vb_amountpaid", amount_paid);
                        cmd.Parameters.AddWithValue("@vb_balance", blnc);
                        cmd.Parameters.AddWithValue("@vd_id", vendorID);
                        cmd.Parameters.AddWithValue("@u_id", userID);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }
        }


        public void UpdateBill(double amountPaid, string status, double balc, int billNo)
        {
            try
            { 
                if (Double.IsNaN(amountPaid))
                {
                    MessageBox.Show("enter Amount please");
                }
                else//bill status ambiguity
                {
                    //con.Close();


                    //insert block
                    string query = "UPDATE vendor_bill SET vb_amountpaid = @paid, vb_balance = @balance, vb_status = @status Where vb_billno = @billNo";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@paid", amountPaid);
                        cmd.Parameters.AddWithValue("@balance", balc);
                        cmd.Parameters.AddWithValue("@status", status);
                        cmd.Parameters.AddWithValue("@billNo",billNo);       
                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Updated");
                        else
                            MessageBox.Show("Record Updated!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }

            }
        }


        public DataSet GetVendorBillsByName(string vdName)
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT b.vb_billno, b.vb_orderDate, b.vb_duedate,b.vb_status, b.vb_totalamount,b.vb_amountpaid,b.vb_balance,(SElECT u.u_username FROM Users u Where u.u_id = b.u_id)UserName From vendor_bill b left Join vendor_detail vd on b.vd_id = vd.vd_id where vd.vd_name = '" + vdName + "'", con))
                    adp.Fill(ds, "vendor_bill");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }


        public DataSet GetVendorDetailsByName(string Name)
        {
            try
            {
                ds = new DataSet();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"Select * From vendor_detail Where vendor_detail.vd_name = '" + Name + "'", con))
                    adp.Fill(ds, "vendor_detail");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }

        
        }


        public DataSet AllVendors()
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"select distinct a.vd_name,a.vd_address,a.vd_phoneno,a.vd_credit,a.vd_balance from vendor_detail a", con))
                    adp.Fill(ds, "vendor_detail");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        //public DataSet AllVendors(string vdName)
        //{
        //    try
        //    {
        //        ds = new DataSet();
        //        //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
        //        //{
        //        if (con.State == ConnectionState.Closed)
        //            con.Open();
        //        using (adp = new SqlDataAdapter(@"select distinct a.vd_name,a.vd_address,a.vd_phoneno,a.vd_credit,a.vd_balance from vendor_detail a where a.vd_name ='"+vdName+"'", con))
        //            adp.Fill(ds, "vendor_detail");

        //        con.Close();
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        throw;
        //    }
        //}

        public DataSet vendor_detail_display()
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"select distinct a.vd_name,a.vd_address,a.vd_phoneno,a.vd_credit,a.vd_balance from vendor_detail a inner join vendor_bill b on a.vd_id = b.vd_id order by vd_name", con))
                    adp.Fill(ds, "vendor_detail");
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }


        public DataSet vendor_billPrint(int billNo)
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT bill.vb_totalamount, bill.vb_amountpaid, bill.vb_balance, bill.vb_duedate,bill.vb_orderDate,bill.vb_status, det.vd_name From vendor_bill bill inner Join vendor_detail det on bill.vd_id = det.vd_id Where bill.vb_billno = " + billNo + "", con))
                    adp.Fill(ds, "vendor_billPrint");
                using (adp = new SqlDataAdapter(@"Select prd.p_brandname, prd.p_modelno, prd.p_color, prd.p_quantity, prd.p_size, prd.p_category, prd.p_gender, prd.p_retailPrice, prd.p_wholePrice From product_stock prd inner Join shipment sh on sh.p_no = prd.p_no Where vb_billno = " + billNo + "", con))
                    adp.Fill(ds, "vendor_billProdsPrint");
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public DataSet vendor_orders(string vdName)
        {
            //SELECT b.vb_billno, b.vb_orderDate, b.vb_dueDate, b.vb_status, b.vb_totalamount, b.vb_amountpaid, b.vb_balance From vendor_bill b inner join vendor_detail v on v.vd_id = b.vd_id Where v.vd_name = 'hello'
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT b.vb_billno, b.vb_orderDate, b.vb_dueDate, b.vb_status, b.vb_totalamount, b.vb_amountpaid, b.vb_balance From vendor_bill b inner join vendor_detail v on v.vd_id = b.vd_id Where v.vd_name = '" + vdName + "'", con))
                    adp.Fill(ds, "vendor_bill");
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        
        }


        public DataSet vendor_detail_displayDS(string vdName)
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"select a.vd_name,a.vd_address,a.vd_phoneno,a.vd_credit,a.vd_balance from vendor_detail a inner join vendor_bill b on a.vd_id = b.vd_id and a.vd_name = '" + vdName + "'", con))
                    adp.Fill(ds, "vendor_detail");
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
        
        public List<int> vendorNameToget_vbillno(string vd_name)
        {
            var list = new List<int>();
            try
            {
                if (vd_name != null && vd_name != "")
                {
                    string query = "select b.vb_billno from vendor_detail a inner join vendor_bill b on a.vd_id = b.vd_id where a.vd_name =  '" + vd_name + "'";
                    using (cmd = new SqlCommand(query, con))
                    {
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        dr = cmd.ExecuteReader();
                        //dt.Load(dr);
                        while (dr.Read())
                        {
                            list.Add(dr.GetInt32(0));
                        }
                    }
                }

                return list;
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                    //   return -1;
                }
            }



        }

        
        public void vendor_detail_insert(string name, string address, string phno, double credit, double blnc)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "INSERT INTO vendor_detail(vd_name,vd_address,vd_phoneno,vd_credit,vd_balance)VALUES(@name,@address,@phno,@credit,@blnc)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@name", name);
                        cmd.Parameters.AddWithValue("@address", address);
                        cmd.Parameters.AddWithValue("@phno", phno);
                        cmd.Parameters.AddWithValue("@credit", credit);
                        cmd.Parameters.AddWithValue("@blnc", blnc);


                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
        
        public int getVendorID(string VendorName) 
        {
            try
            {
                if (VendorName != null && VendorName != "")
                {
                    string query = "SELECT v.vd_id FROM vendor_detail v WHERE v.vd_name = '"+VendorName+"'";
                    using (cmd = new SqlCommand(query, con))
                    {
                       // cmd.Parameters.AddWithValue("@Name", VendorName);
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                return dr.GetInt32(0);
                            }
                            else 
                            {
                                MessageBox.Show("No Vendor Found");
                                //             con.Close();
                                return -1;
                            }
                        } 
                        
                      }
                }
                else
                {
                    MessageBox.Show("Please Provide Vendor Name");
                    return -1;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return -1;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                 //   return -1;
                }
            }
            


        }
        
        //
        //
        //Customer
        //
        //

        public DataSet GetAllCustomerBills()
        {
            try
            {
                ds = new DataSet();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT * From customer_bill", con))
                    adp.Fill(ds, "customer_bill");

                con.Close();
                return ds;

            }
            catch (Exception ex )
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            finally
            {
                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }

        public DataSet GetCustomerBillsByName(string Name)
        {
            try
            {
                ds = new DataSet();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT b.cb_id as billID, b.cb_orderDate as OrderDate, b.cb_deliveryDate as DeliveryDate, b.cb_balance as Balance, b.cb_advance as Advance, b.cb_billStatus as billStatus FROM customer_bill b inner join customer_detail cd on b.c_id = cd.c_id where cd.c_name = '" + Name + "'", con))
                    adp.Fill(ds, "customer_bill");

                con.Close();
                return ds;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            finally
            {
                if (con.State != ConnectionState.Closed)
                {
                    con.Close();
                }
            }
        }




        public void SetcustomerOrder(string modelno = "", string co_frame_desc = "")
        {
            try
            {
                if (modelno=="")
                {
                    MessageBox.Show("Cant find product");
                    return;
                }
                else
                {
                    int cb_id, pre_no;

                    using (cmd = new SqlCommand(@" SELECT * From product_stock where p_modelno = @modelno", con))
                        //{
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                    adp.SelectCommand = cmd;

                    cmd.Parameters.Add("@modelno", SqlDbType.VarChar, 50).Value = modelno;
                    DataSet ds = new DataSet();
                    adp.Fill(ds, "product_stock");
                    MessageBox.Show(ds.Tables["product_stock"].Rows[0].ItemArray[0].ToString());
                    int p = Convert.ToInt32(ds.Tables["product_stock"].Rows[0].ItemArray[0].ToString());
                    
                    con.Close();

                    using (cmd = new SqlCommand(@"SELECT cb_id FROM customer_bill WHERE cb_id = (SELECT MAX(cb_id)  FROM customer_bill)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            cb_id = dr.GetInt32(0);

                        }

                        con.Close();
                    }
                    using (cmd = new SqlCommand("SELECT pre_no FROM Prescription WHERE pre_no = (SELECT MAX(pre_no)  FROM Prescription)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            pre_no = dr.GetInt32(0);

                        }

                        con.Close();
                        //distance PK fetch
                    }

                    String query00;

                    if (co_frame_desc == "")
                    {
                        query00 = "insert into customer_order (cb_id,p_no,pre_no) values (@cb_id,@p_no,@pre_no)";
                    }
                    else
                    {
                        query00 = "insert into customer_order (co_frame_desc,cb_id,p_no,pre_no) values (@co_frame_desc,@cb_id,@p_no,@pre_no)";
                    }
                    using (cmd = new SqlCommand(query00, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@co_frame_desc", co_frame_desc);
                        cmd.Parameters.AddWithValue("@cb_id", cb_id);
                        cmd.Parameters.AddWithValue("@p_no", p);
                        cmd.Parameters.AddWithValue("@pre_no", pre_no);


                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record order Not Insert");
                        else
                            MessageBox.Show("Record order Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }


        public int InsertPresDistance(double sph = 0 , double cyl = 0, double axis = 0) 
        {
            // Return the Id of inserted Record
            return 0;
        }
        public int InsertPresNear(double sph = 0, double cyl = 0, double axis = 0)
        {
            // Return the Id of inserted Record
            return 0;
        }



        public void customer_detail_insert(string c_name, string c_address, string c_contact)
        {
            try
            {
                if (c_name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "INSERT INTO customer_detail(c_name,c_address,c_contact)VALUES(@c_name,@c_address,@c_contact)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@c_name", c_name);
                        cmd.Parameters.AddWithValue("@c_address", c_address);
                        cmd.Parameters.AddWithValue("@c_contact", c_contact);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void customer_bill_insert(DateTime cb_orderDate, DateTime cb_deliveryDate, double cb_balance, double cb_advance, string cb_billStatus,string name, double cb_discount = 0, string cb_description = "")
        {
            /*
             * Parameter me username aur customer name dena hy
             * name se id nikalni hy user aur customer ki
             * 
             * CustomerNewBill me Generate k button k piche cus.Name se cus ka name milega
             * 
             */
            try
            {
                if (cb_advance == null)
                {
                    MessageBox.Show("enter advance please");
                }
                else
                {
                    int c_id;
                    ds = new DataSet();

                    using (cmd = new SqlCommand(@" select c_id from customer_detail where c_name = @name", con))
                        //{
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                    adp = new SqlDataAdapter(" select c_id from customer_detail where c_name = @name", con);
                    adp.SelectCommand = cmd;
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                    
                    adp.Fill(ds, "customer_detail");
                    c_id = Convert.ToInt32(ds.Tables["customer_detail"].Rows[0].ItemArray[0]);

                    con.Close();



                    String query = "INSERT INTO customer_bill(cb_orderDate,cb_deliveryDate,cb_discount,cb_description,cb_balance,cb_advance,cb_billStatus,u_id,c_id)VALUES(@cb_orderDate,@cb_deliveryDate,@cb_discount,@cb_description,@cb_balance,@cb_advance,@cb_billStatus,@u_id,@c_id)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@cb_orderDate", cb_orderDate);
                        cmd.Parameters.AddWithValue("@cb_deliveryDate", cb_deliveryDate);
                        cmd.Parameters.AddWithValue("@cb_discount", cb_discount);
                        cmd.Parameters.AddWithValue("@cb_description", cb_description);
                        cmd.Parameters.AddWithValue("@cb_balance", cb_balance);
                        cmd.Parameters.AddWithValue("@cb_advance", cb_advance);
                        cmd.Parameters.AddWithValue("@cb_billStatus", cb_billStatus);
                        cmd.Parameters.AddWithValue("@u_id", "1");
                        cmd.Parameters.AddWithValue("@c_id", c_id);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record customer bill Not Insert");
                        else
                            MessageBox.Show("Record customer bill Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public DataTable GetLastCustumerBillID()
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT TOP 1 cb.cb_id FROM customer_bill cb ORDER BY cb.cb_id DESC", con))
                    adp.Fill(ds, "CustomerBill");

                con.Close();
                DataTable dt = ds.Tables["CustomerBill"];
                return dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public DataSet customer_detail_display()
        {
            try
            {
                if (false)
                {
                    MessageBox.Show("enter name to search");
                    return null;
                }
                else
                {
                    ds = new DataSet();
                    //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                    //{
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    using (adp = new SqlDataAdapter(@"select c_name as name,c_address as address,c_contact as contact from customer_detail", con))
                        adp.Fill(ds, "customer_detail");

                    //}
                }
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }


        public DataSet GetCustomerBillByID(int billNo)
        {
            try
            {
                ds = new DataSet(); 
                using (cmd = new SqlCommand(@" SELECT * From customer_bill  where cb_id = @id", con))
                    //{
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                adp.SelectCommand = cmd;

                cmd.Parameters.Add("@id", SqlDbType.Int, 50).Value = billNo;
                adp.Fill(ds, "customer_bill");

                con.Close();

                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public DataSet GetCustomerDetailsByName(string Name)
        {
            try
            {
                ds = new DataSet();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"Select c_name as name,c_address as address,c_contact as contact From customer_detail Where c_name = '" + Name + "'", con))
                    adp.Fill(ds, "customer_detail");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }


        }

        public void Update_CustomerDetailByName(string c_name, string c_address, string c_contact,string name)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "Update customer_detail SET c_name = @c_name, c_address = @c_address, c_contact = @c_contact where c_name = '"+name+"'";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@c_name", c_name);
                        cmd.Parameters.AddWithValue("@c_address", c_address);
                        cmd.Parameters.AddWithValue("@c_contact", c_contact);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Updated");
                        else
                            MessageBox.Show("Record Updated!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void Delete_CustomerDetailByName(string c_name)
        {
            try
            {
                if (c_name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else
                {
                    String query = "delete from customer_detail where c_name = @c_name";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@c_name", c_name);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not deleted");
                        else
                            MessageBox.Show("Record deleted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }



        ///
        //
        //Prescription
        //
        ///

        //
        //Distance
        //
        public void SetDistanceRightEye(string pres_name, double sph = 0, double cyl = 0, double axis = 0)
        {
            try
            {
                if (pres_name == null)
                {
                    MessageBox.Show("enter Prescription name please");
                    return;
                }
                else
                {
                    String query = "INSERT INTO Distance(sph,cyl,axis)VALUES(@sph,@cyl,@axis)";
                    int dis_no, Re_no;
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@sph", sph);
                        cmd.Parameters.AddWithValue("@cyl", cyl);
                        cmd.Parameters.AddWithValue("@axis", axis);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();

                        if (result < 0)
                            MessageBox.Show("Record Not distance Insert");
                        else
                            MessageBox.Show("Record distance Inserted!");
                        con.Close();
                        //distance insert

                    }
                    using (cmd = new SqlCommand("SELECT dis_no FROM Distance WHERE dis_no = (SELECT MAX(dis_no)  FROM Distance)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            dis_no = dr.GetInt32(0);

                        }
                        MessageBox.Show("distance PK fetch");
                        con.Close();
                        //distance PK fetch
                    }

                    String insert_RightEye = "insert into Right_eye (dis_no) values (@dis_no)";
                    using (cmd = new SqlCommand(insert_RightEye, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@dis_no", dis_no);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Right_eye Insert");
                        else
                            MessageBox.Show("Record Right_eye Inserted!");
                        con.Close();
                        //Right distance pk insert as fk

                    }
                    using (cmd = new SqlCommand("SELECT Re_no FROM Right_eye WHERE Re_no = (SELECT MAX(Re_no)  FROM Right_eye)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            Re_no = dr.GetInt32(0);

                        }
                        MessageBox.Show("Right_Eye PK fetch");
                        con.Close();
                        //Right_Eye PK fetch
                    }
                    using (cmd = new SqlCommand("insert into Prescription (pre_name,Re_no) values (@pre_name,@Re_no)", con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@pre_name", pres_name);
                        cmd.Parameters.AddWithValue("@Re_no", Re_no);
                        //cmd.Parameters.AddWithValue("@null", null);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        con.Close();
                        //Right_Eye pk insert as fk into prescription


                        if (result < 0)
                            MessageBox.Show("Record Not prescription Insert");
                        else
                            MessageBox.Show("Record prescription Inserted!");

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void SetDistanceLeftEye(string pres_name, double sph = 0, double cyl = 0, double axis = 0)
        {
            try
            {
                if (pres_name == null)
                {
                    MessageBox.Show("enter Prescription name please");
                }
                else
                {
                    String query = "INSERT INTO Distance(sph,cyl,axis)VALUES(@sph,@cyl,@axis)";
                    int dis_no, Le_no;
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@sph", sph);
                        cmd.Parameters.AddWithValue("@cyl", cyl);
                        cmd.Parameters.AddWithValue("@axis", axis);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //distance insert

                    }
                    using (cmd = new SqlCommand("SELECT dis_no FROM Distance WHERE dis_no = (SELECT MAX(dis_no)  FROM Distance)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            dis_no = dr.GetInt32(0);

                        }
                        con.Close();
                        //distance PK fetch
                    }

                    using (cmd = new SqlCommand("insert into Left_eye (dis_no) values @dis_no", con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@dis_no", dis_no);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //Left distance pk insert as fk

                    }
                    using (cmd = new SqlCommand("SELECT Le_no FROM Left_eye WHERE Le_no = (SELECT MAX(Le_no)  FROM Left_eye)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            Le_no = dr.GetInt32(0);

                        }
                        con.Close();
                        //Left_Eye PK fetch
                    }
                    using (cmd = new SqlCommand("insert into Prescription (pre_name,Le_no) values (@pre_name,@Le_no)", con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@pre_name", pres_name);
                        cmd.Parameters.AddWithValue("@Le_no", Le_no);
                        //cmd.Parameters.AddWithValue("@null", null);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        con.Close();
                        //Left_Eye pk insert as fk into prescription


                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void SetDistanceBothEye(string pres_name, double Lsph = 0, double Lcyl = 0, double Laxis = 0, double Rsph = 0, double Rcyl = 0, double Raxis = 0)
        {
            try
            {
                if (pres_name == null)
                {
                    MessageBox.Show("enter Prescription name please");
                }
                else
                {
                    String query = "INSERT INTO Distance(sph,cyl,axis)VALUES(@sph,@cyl,@axis)";
                    int dis_no, Re_no, Le_no;
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@sph", Lsph);
                        cmd.Parameters.AddWithValue("@cyl", Lcyl);
                        cmd.Parameters.AddWithValue("@axis", Laxis);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //distance insert
                    }
                    using (cmd = new SqlCommand("SELECT dis_no FROM Distance WHERE dis_no = (SELECT MAX(dis_no)  FROM Distance)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            dis_no = dr.GetInt32(0);
                        }
                        con.Close();
                        //distance PK fetch
                    }

                    using (cmd = new SqlCommand("insert into Left_eye(dis_no) VALUES (@dis_nol)", con))
                    {

                        cmd.Parameters.AddWithValue("@dis_nol", dis_no);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //Left distance pk insert as fk

                    }

                    string query1 = "INSERT INTO Distance(sph,cyl,axis)VALUES(@sph,@cyl,@axis)";
                    using (cmd = new SqlCommand(query1, con))
                    {

                        cmd.Parameters.AddWithValue("@sph", Rsph);
                        cmd.Parameters.AddWithValue("@cyl", Rcyl);
                        cmd.Parameters.AddWithValue("@axis", Raxis);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //distance insert

                    }
                    using (cmd = new SqlCommand("SELECT dis_no FROM Distance WHERE dis_no = (SELECT MAX(dis_no)  FROM Distance)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            dis_no = dr.GetInt32(0);

                        }
                        con.Close();
                        //distance PK fetch
                    }




                    using (cmd = new SqlCommand("insert into Right_eye (dis_no) values (@dis_nor)", con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@dis_nor", dis_no);

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        //Right distance pk insert as fk

                    }
                    using (cmd = new SqlCommand("SELECT Le_no FROM Left_eye WHERE Le_no = (SELECT MAX(Le_no)  FROM Left_eye)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            Le_no = dr.GetInt32(0);
                        }
                        con.Close();
                        //Left_Eye PK fetch
                    }
                    using (cmd = new SqlCommand("SELECT Re_no FROM Right_eye WHERE Re_no = (SELECT MAX(Re_no)  FROM Right_eye)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            Re_no = dr.GetInt32(0);
                        }
                        con.Close();
                        //Left_Eye PK fetch
                    }
                    using (cmd = new SqlCommand("insert into Prescription (pre_name,Re_no,Le_no) values (@pre_name,@Re_no,@Le_no)", con))
                    {

                        cmd.Parameters.AddWithValue("@Re_no", Re_no);
                        cmd.Parameters.AddWithValue("@Le_no", Le_no);
                        cmd.Parameters.AddWithValue("@pre_name", pres_name);

                        //cmd.Parameters.AddWithValue("@null", null);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        con.Close();
                        //Left_Eye pk insert as fk into prescription


                        if (result < 0)
                            MessageBox.Show("Record prescribtion Not Insert");
                        else
                            MessageBox.Show("Record prescribtion Inserted!");

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }



        //
        //
        //monthly_Sale
        //
        //

        public void SetmonthlySales()
        {
            try
            {
                if (false)
                {
                    MessageBox.Show("enter Name please");
                }
                else//bill status ambiguity
                {
                    decimal vb_amt, cb_amt;

                    cmd = new SqlCommand("select sum(vb_totalamount) , sum(cb_balance)  from  vendor_bill,customer_bill", con);
                    con.Open();

                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    using (dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        vb_amt = dr.GetDecimal(0);
                        cb_amt = dr.GetDecimal(1);

                    }
                    con.Close();
                    //sum amount

                    using (cmd = new SqlCommand("insert into monthly_sale(cb_totalPrice , vb_totalProce, ms_date) values (@c , @v, CONVERT (date, CURRENT_TIMESTAMP))", con))
                    {

                        cmd.Parameters.AddWithValue("@c", cb_amt);
                        cmd.Parameters.AddWithValue("@v", vb_amt);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Record Not Insert");
                        else
                            MessageBox.Show("Record Inserted!");
                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }
        }


        public DataSet GetBillsByMonth(int month)
        {
            try
            {
                ds = new DataSet();
                using (cmd = new SqlCommand(@" select Distinct cb_totalPrice as customer_amount,vb_totalProce as vendor_amount from monthly_sale where month(ms_date) = @month", con))
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                adp = new SqlDataAdapter(" select Distinct cb_totalPrice as customer_amount,vb_totalProce as vendor_amount from monthly_sale where month(ms_date) = @month", con);

                adp.SelectCommand = cmd;
                cmd.Parameters.Add("@month", SqlDbType.Int, 5).Value = month;
                adp.Fill(ds, "monthly_sale");
                con.Close();


                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        } //select parametrize

        public void SetmonthlySales(int month)
        {
            try
            {
                if (false)
                {
                    MessageBox.Show("enter Name please");
                }
                else//bill status ambiguity
                {
                    decimal vb_amt, cb_amt;

                    cmd = new SqlCommand("select sum(vb_totalamount) , sum(cb_balance)  from  vendor_bill,customer_bill WHERE month(cb_orderDate) = '" + month + "'", con);
                    con.Open();

                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    using (dr = cmd.ExecuteReader())
                    {
                        dr.Read();
                        vb_amt = dr.GetDecimal(0);
                        cb_amt = dr.GetDecimal(1);

                    }
                    con.Close();
                    //sum amount

                    using (cmd = new SqlCommand("insert into monthly_sale(cb_totalPrice , vb_totalProce, ms_date) values (@c , @v, CONVERT (date, CURRENT_TIMESTAMP))", con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        cmd.Parameters.AddWithValue("@c", cb_amt);
                        cmd.Parameters.AddWithValue("@v", vb_amt);
                        //cmd.Parameters.AddWithValue("@axis", axis);

                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Unable to Generate Report");
                        else
                            MessageBox.Show("Report Generated!");
                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }
        }
        public DataSet GetAllReports()
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"SELECT * FROM monthly_sale", con))
                    adp.Fill(ds, "monthly_sale");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }





        public DataSet GetBillSUMbyMonth(int month)
        {
            try
            {
                ds = new DataSet();

                using (cmd = new SqlCommand(@" select sum(cb_totalPrice) as customer_amount,sum(vb_totalProce) as vendor_amount from monthly_sale where month(ms_date) = @month", con))
                    //{
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                adp = new SqlDataAdapter("select sum(cb_totalPrice) as customer_amount,sum(vb_totalProce) as vendor_amount from monthly_sale where month(ms_date) = @month", con);
                adp.SelectCommand = cmd;
                cmd.Parameters.Add("@month", SqlDbType.Int, 2).Value = month;
                //cmd.Parameters.Add("@modelno", SqlDbType.VarChar, 50).Value = Modelno;
                //DataSet custDS = new DataSet();
                adp.Fill(ds, "monthly_sale");

                con.Close();


                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        } //select parametrize











        //
        //
        //Product Stock
        //
        //

        public void product_stock_insert(string p_brandname, string p_modelno, string p_color, int p_quantity, int p_size, double p_retailPrice, string p_category, string p_gender, double p_wholePrice, byte[] p_image = null)
        {
            try
            {
                if (p_retailPrice == null)
                {
                    MessageBox.Show("enter retailPrice please");
                }
                else//bill status ambiguity
                {
                    String query = "INSERT INTO product_stock(p_brandname,p_modelno,p_color,p_quantity,p_size,p_retailPrice,p_category,p_gender,p_wholePrice)VALUES(@p_brandname,@p_modelno,@p_color,@p_quantity,@p_size,@p_retailPrice,@p_category,@p_gender,@p_wholePrice)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        //cmd.Parameters.AddWithValue("@p_image", p_image);
                        cmd.Parameters.AddWithValue("@p_brandname", p_brandname);
                        cmd.Parameters.AddWithValue("@p_modelno", p_modelno);
                        cmd.Parameters.AddWithValue("@p_color", p_color);
                        cmd.Parameters.AddWithValue("@p_quantity", p_quantity);
                        cmd.Parameters.AddWithValue("@p_size", p_size);
                        cmd.Parameters.AddWithValue("@p_retailPrice", p_retailPrice);
                        cmd.Parameters.AddWithValue("@p_category", p_category);
                        cmd.Parameters.AddWithValue("@p_gender", p_gender);
                        cmd.Parameters.AddWithValue("@p_wholePrice", p_wholePrice);
                        if(con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Product Record Not Insert");
                        else
                        {
                            MessageBox.Show("Product Record Inserted!");
                            shipment_insert(p_modelno);
                            stock_detail_insert(p_modelno);
                        }
                            
                            
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public DataSet AllProduct_display()
        {
            try
            {
                ds = new DataSet();
                //using (cmd = new SqlCommand("select c_name,c_address,c_contact from customer_detail", con))
                //{
                if (con.State == ConnectionState.Closed)
                    con.Open();
                using (adp = new SqlDataAdapter(@"select distinct p_brandname,p_modelno,p_color,p_quantity,p_size,p_retailPrice,p_category,p_gender,p_wholePrice from product_stock", con))
                    adp.Fill(ds, "product_stock");

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        public DataSet GetProductsByBrandNameANDModelNo(string Brandname, string Modelno)
        {
            try
            {
                ds = new DataSet();
                using (cmd = new SqlCommand(@" SELECT * From product_stock  where p_brandname = @brandname AND p_modelno = @modelno", con))
                    //{
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                adp.SelectCommand = cmd;
                //using (adp = new SqlDataAdapter(@" SELECT * From product_stock  where p_brandname = @brandname AND p_modelno = @modelno", con))
                //    adp.SelectCommand.Parameters.AddWithValue("@brandname", Brandname);
                //adp.SelectCommand.Parameters.AddWithValue("@modelno",Modelno);
                //    adp.Fill(ds, "product_stock");



                cmd.Parameters.Add("@brandname", SqlDbType.VarChar, 50).Value = Brandname;
                cmd.Parameters.Add("@modelno", SqlDbType.VarChar, 50).Value = Modelno;
                //DataSet custDS = new DataSet();
                adp.Fill(ds, "product_stock");

                con.Close();


                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
       


        public DataSet GetProductStockByModelno(string Name)
        {
            try
            {
                ds = new DataSet();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                //cmd = new SqlCommand(@"select distinct p_brandname,p_modelno,p_color,p_quantity,p_size,p_retailPrice,p_category,p_gender,p_wholePrice from product_stock where p_modelno = @p_modelno", con);
                using (adp = new SqlDataAdapter(@"select distinct p_brandname,p_modelno,p_color,p_quantity,p_size,p_retailPrice,p_category,p_gender,p_wholePrice from product_stock where p_modelno = '" + Name + "'", con))
                    //cmd.Parameters.AddWithValue("@p_modelno", Name);
                    adp.Fill(ds, "product_stock");
                // cmd.ExecuteNonQuery();
                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }


        }


        public void product_stock_Update(string name,string p_brandname, string p_modelno, string p_color, int p_quantity, int p_size, double p_retailPrice, string p_category, string p_gender, double p_wholePrice, byte[] p_image = null)
        {
            try
            {
                if (name == null)
                {
                    MessageBox.Show("enter name please");
                }
                else//bill status ambiguity
                {
                    String query = "Update product_stock set p_brandname=@p_brandname,p_modelno=@p_modelno,p_color=@p_color,p_quantity=@p_quantity,p_size=@p_size,p_retailPrice=@p_retailPrice,p_category=@p_category,p_gender=@p_gender,p_wholePrice=@p_wholePrice where p_modelno = @p";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        //cmd.Parameters.AddWithValue("@p_image", p_image);
                        cmd.Parameters.AddWithValue("@p_brandname", p_brandname);
                        cmd.Parameters.AddWithValue("@p", name);
                        if (p_modelno!=null)
                        {
                            cmd.Parameters.AddWithValue("@p_modelno", p_modelno);
                            
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@p_modelno", name);
                        }
                        
                        cmd.Parameters.AddWithValue("@p_color", p_color);
                        cmd.Parameters.AddWithValue("@p_quantity", p_quantity);
                        cmd.Parameters.AddWithValue("@p_size", p_size);
                        cmd.Parameters.AddWithValue("@p_retailPrice", p_retailPrice);
                        cmd.Parameters.AddWithValue("@p_category", p_category);
                        cmd.Parameters.AddWithValue("@p_gender", p_gender);
                        cmd.Parameters.AddWithValue("@p_wholePrice", p_wholePrice);
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Product Record Not Updated");
                        else
                        {
                            MessageBox.Show("Product Record Updated!");
                            //shipment_insert(p_modelno);
                            //stock_detail_insert(p_modelno);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void product_stock_Update(string p_brandname, string p_modelno, int p_quantity)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(p_brandname) || string.IsNullOrWhiteSpace(p_modelno))
                {
                    MessageBox.Show("enter name please");
                }
                else//bill status ambiguity
                {
      //Update product_stock set p_quantity= @qty where product_stock.p_brandname = '@brand' AND product_stock.p_modelno = @model

                    String query = "Update product_stock set p_quantity= @qty where product_stock.p_brandname = @brand AND product_stock.p_modelno = @model";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        //cmd.Parameters.AddWithValue("@p_image", p_image);
                        cmd.Parameters.AddWithValue("@brand", p_brandname);
                        cmd.Parameters.AddWithValue("@model", p_modelno);
                        cmd.Parameters.AddWithValue("@qty", p_quantity);
                        
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Product Record Not Updated");
                        else
                        {
                            MessageBox.Show("Product Record Updated!");
                            //shipment_insert(p_modelno);
                            //stock_detail_insert(p_modelno);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public void product_stock_Delete(string modelNo)
        {
            try
            {
                if (modelNo == null)
                {
                    MessageBox.Show("enter name please");
                }
                else//bill status ambiguity
                {
                    String query = "Delete from product_stock where p_modelno = @p_modelno";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);
                        //cmd.Parameters.AddWithValue("@p_image", p_image);
                        cmd.Parameters.AddWithValue("@p_modelno", modelNo);
                        
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Unable to Delete Product");
                        else
                        {
                            MessageBox.Show("Product Record Deleted!");
                            //shipment_insert(p_modelno);
                            //stock_detail_insert(p_modelno);
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
        
        //
        //
        //shiplment
        //
        //
        public void shipment_insert(string pname)
        {
            try
            {
                if (pname == null)
                {
                    MessageBox.Show("enter product name to search");
                    //return null;
                }
                else
                {
                    int billno;
                    int prod_no;


                    using (cmd = new SqlCommand("SELECT vb_billno FROM  vendor_bill WHERE vb_billno = (SELECT MAX(vb_billno)  FROM vendor_bill)", con))
                    {
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            billno = dr.GetInt32(0);

                        }
                    }
                    con.Close();
                    using (cmd = new SqlCommand("SELECT p_no FROM  product_stock WHERE p_no = (SELECT MAX(p_no)  FROM product_stock)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            prod_no = dr.GetInt32(0);

                        }
                    }
                    con.Close();

                    String query = "INSERT INTO shipment(vb_billno,p_no)VALUES(@vb_billno,@p_no)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);

                        cmd.Parameters.AddWithValue("@vb_billno", billno);
                        cmd.Parameters.AddWithValue("@p_no", prod_no);
                        if(con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("ShipmentRecord Not Insert");
                        else
                            MessageBox.Show("ShipmentRecord Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        //
        //
        //stock_detail
        //
        //

        public void stock_detail_insert(string pname)
        {
            try
            {
                if (pname == null)
                {
                    MessageBox.Show("enter product name to search");
                    //return null;
                }
                else
                {
                    int billno;
                    int prod_no;


                    using (cmd = new SqlCommand("SELECT vd_id FROM  vendor_detail WHERE vd_id = (SELECT MAX(vd_id)  FROM vendor_detail)", con))
                    {
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            billno = dr.GetInt32(0);

                        }
                    }
                    con.Close();
                    using (cmd = new SqlCommand("SELECT p_no FROM  product_stock WHERE p_no = (SELECT MAX(p_no)  FROM product_stock)", con))
                    {

                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        using (dr = cmd.ExecuteReader())
                        {

                            dr.Read();
                            prod_no = dr.GetInt32(0);

                        }
                    }
                    con.Close();

                    String query = "INSERT INTO stock_detail1(vd_id,p_no)VALUES(@vd_id,@p_no)";
                    using (cmd = new SqlCommand(query, con))
                    {

                        //cmd.Parameters.AddWithValue("@id", ID);

                        cmd.Parameters.AddWithValue("@vd_id", billno);
                        cmd.Parameters.AddWithValue("@p_no", prod_no);
                        if (con.State == ConnectionState.Closed)
                            con.Open();
                        int result = cmd.ExecuteNonQuery();
                        if (result < 0)
                            MessageBox.Show("Stock_detail Record Not Insert");
                        else
                            MessageBox.Show("Stock_detail Record Inserted!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);


            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
    }
}
