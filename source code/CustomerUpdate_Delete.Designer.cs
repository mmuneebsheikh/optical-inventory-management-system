﻿namespace VisualProjectOpInventory
{
    partial class CustomerUpdate_Delete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPhoneNomaskedVendor = new System.Windows.Forms.MaskedTextBox();
            this.txtNameVendor = new System.Windows.Forms.TextBox();
            this.txtAddressVendor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btwDeleteCustomer = new System.Windows.Forms.Button();
            this.btwUpdateCustomer = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnewname = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btwBack = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPhoneNomaskedVendor
            // 
            this.txtPhoneNomaskedVendor.Location = new System.Drawing.Point(228, 188);
            this.txtPhoneNomaskedVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNomaskedVendor.Name = "txtPhoneNomaskedVendor";
            this.txtPhoneNomaskedVendor.Size = new System.Drawing.Size(240, 20);
            this.txtPhoneNomaskedVendor.TabIndex = 12;
            this.txtPhoneNomaskedVendor.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtPhoneNomaskedVendor_MaskInputRejected);
            // 
            // txtNameVendor
            // 
            this.txtNameVendor.Location = new System.Drawing.Point(180, 43);
            this.txtNameVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtNameVendor.Name = "txtNameVendor";
            this.txtNameVendor.Size = new System.Drawing.Size(243, 20);
            this.txtNameVendor.TabIndex = 15;
            // 
            // txtAddressVendor
            // 
            this.txtAddressVendor.Location = new System.Drawing.Point(224, 226);
            this.txtAddressVendor.Margin = new System.Windows.Forms.Padding(2);
            this.txtAddressVendor.Multiline = true;
            this.txtAddressVendor.Name = "txtAddressVendor";
            this.txtAddressVendor.Size = new System.Drawing.Size(243, 73);
            this.txtAddressVendor.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(82, 221);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 29);
            this.label3.TabIndex = 14;
            this.label3.Text = "Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(82, 186);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 29);
            this.label2.TabIndex = 11;
            this.label2.Text = "Phone No:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(82, 36);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 29);
            this.label4.TabIndex = 10;
            this.label4.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(82, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(489, 29);
            this.label1.TabIndex = 10;
            this.label1.Text = "Enter Customer Name to Update / Delete";
            // 
            // btwDeleteCustomer
            // 
            this.btwDeleteCustomer.Location = new System.Drawing.Point(118, 312);
            this.btwDeleteCustomer.Name = "btwDeleteCustomer";
            this.btwDeleteCustomer.Size = new System.Drawing.Size(106, 44);
            this.btwDeleteCustomer.TabIndex = 65;
            this.btwDeleteCustomer.Text = "Delete";
            this.btwDeleteCustomer.UseVisualStyleBackColor = true;
            this.btwDeleteCustomer.Click += new System.EventHandler(this.btwDeleteCustomer_Click);
            // 
            // btwUpdateCustomer
            // 
            this.btwUpdateCustomer.Location = new System.Drawing.Point(317, 311);
            this.btwUpdateCustomer.Name = "btwUpdateCustomer";
            this.btwUpdateCustomer.Size = new System.Drawing.Size(106, 45);
            this.btwUpdateCustomer.TabIndex = 64;
            this.btwUpdateCustomer.Text = "Update";
            this.btwUpdateCustomer.UseVisualStyleBackColor = true;
            this.btwUpdateCustomer.Click += new System.EventHandler(this.btwUpdateCustomer_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(82, 142);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 29);
            this.label5.TabIndex = 10;
            this.label5.Text = "New Name";
            // 
            // txtnewname
            // 
            this.txtnewname.Location = new System.Drawing.Point(228, 151);
            this.txtnewname.Margin = new System.Windows.Forms.Padding(2);
            this.txtnewname.Name = "txtnewname";
            this.txtnewname.Size = new System.Drawing.Size(243, 20);
            this.txtnewname.TabIndex = 15;
            this.txtnewname.TextChanged += new System.EventHandler(this.txtnewname_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.btwBack);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btwDeleteCustomer);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btwUpdateCustomer);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtPhoneNomaskedVendor);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtnewname);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtNameVendor);
            this.panel1.Controls.Add(this.txtAddressVendor);
            this.panel1.Location = new System.Drawing.Point(252, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(613, 401);
            this.panel1.TabIndex = 66;
            // 
            // btwBack
            // 
            this.btwBack.Location = new System.Drawing.Point(3, 3);
            this.btwBack.Name = "btwBack";
            this.btwBack.Size = new System.Drawing.Size(67, 26);
            this.btwBack.TabIndex = 66;
            this.btwBack.Text = "<--";
            this.btwBack.UseVisualStyleBackColor = true;
            this.btwBack.Click += new System.EventHandler(this.btwBack_Click);
            // 
            // CustomerUpdate_Delete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(1169, 488);
            this.Controls.Add(this.panel1);
            this.Name = "CustomerUpdate_Delete";
            this.Text = "CustomerUpdate_Delete";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txtPhoneNomaskedVendor;
        private System.Windows.Forms.TextBox txtNameVendor;
        private System.Windows.Forms.TextBox txtAddressVendor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btwDeleteCustomer;
        private System.Windows.Forms.Button btwUpdateCustomer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnewname;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btwBack;
    }
}