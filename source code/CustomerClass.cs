﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualProjectOpInventory
{
    public class CustomerClass
    {
        
        //string Name, contactNo;
        //Reciept[] Reciepts; // multiple reciepts from database and single reciept from gui 
        public CustomerClass()
        {
            reciepts = new List<Reciept>();
            products = new List<Products>();
            prescriptions = new List<Prescription>();
        }


        public string Name { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public List<Reciept> reciepts { get; set; }
        // public Reciept[] reciept = new Reciept[10000000];
        public Bill bill = new Bill();
        public List<Prescription> prescriptions { get; set; }
        public List<Products> products { get; set; }
        //public Prescription[] prescription = new Prescription[10];


        public class Reciept
        {
            public int RecieptNo { get; set; }
            public int billPrice { get; set; }
            public DateTime date { get; set; }
            public string status { get; set; }

        }
        public class Products
        {
            public Products()
            {
                //prescriptions = new ListPrescription();
            }
            public Prescription prescriptions = new Prescription();
            public string ProdName { get; set; }
            //public string modelNo { get; set; }
            public string modelNo { get; set; }

            public string Desc { get; set; }
        }


        public class Bill
        {

            public Bill()
            {
                products = new List<Products>();
            }
            public class Amount
            {
                double Advance { get; set; }
                double Balance { get; set; }
            }
            

            public string billNo { get; set; }
            public DateTime OrderDate { get; set; }
            public DateTime deliveryDate { get; set; }
            public Amount amount { get; set; }
            public List<Products> products { get; set; }
           // public Products[] prods = new Products[1000000];

        }


        public class Prescription
        {

            public Prescription()
            {
                //leftEyeNo = new leftEye();
                //rightEyeNo = new rightEye();
            }

            public string NamePresc { get; set; } // Name of Priscription person
            public leftEye leftEyeNo = new leftEye();
            public rightEye rightEyeNo = new rightEye();



            public class leftEye
            {
                
                public class D
                {
                    public double sph { get; set; }
                    public double cyl { get; set; }
                    public double axis { get; set; }              
                }
                public class N
                {
                    public double sph { get; set; }
                    public double cyl { get; set; }
                    public double axis { get; set; }
                }

                public D distance = new D();
                public N near = new N();

            }

            public class rightEye
            {
                public class D // // // Distance sight
                {
                    public double sph { get; set; }
                    public double cyl { get; set; }
                    public double axis { get; set; }

                }
                public class N // // // Near Sight
                {
                    public double sph { get; set; }
                    public double cyl { get; set; }
                    public double axis { get; set; }
                }

                public D distance = new D();
                public N near = new N();

            }
        }

    }
}
